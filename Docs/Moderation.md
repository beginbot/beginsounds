# Moderation

## Global Modes

```
!chaos
```
Chaos Mode:
  - ALL OBS commands are available, cost Mana

Non-Chaos Mode:
  - Only Streamlords/gods/Streawm Jester have access to the OBS commands

## User Level

```
!cage @viewer
```

- Block users from playing sounds/triggering OBS effects
  are available, cost Mana

## Sound / Source Level

```
!disable INSERT_SOUND

!enable INSERT_SOUND
```
