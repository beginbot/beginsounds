package main

import (
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

func main() {
	fmt.Println("Populate the Meme info")
	db := database.CreateDBConn("beginsounds4")
	images, _ := media_request.ApprovedImages(db)
	for _, m := range images {
		_, err := memes.Find(db, m.Name)
		if err == nil {
			//
			// We need to update some stats on the Meme
		}
	}
}
