module gitlab.com/beginbot/beginsounds

go 1.15

// So we need start using our own
// replace github.com/christopher-dG/go-obs-websocket => ../opensource/go-obs-websocket
// replace github.com/davidbegin/go-obs-websocket => ../opensource/go-obs-websocket

// obsws "github.com/davidbegin/go-obs-websocket"

require (
	github.com/agnivade/levenshtein v1.1.0
	github.com/aws/aws-sdk-go v1.34.2
	github.com/buildpacks/pack v0.15.1
	// github.com/christopher-dG/go-obs-websocket v0.0.0-20200720193653-c4fed10356a5
	github.com/d4l3k/go-pry v0.0.0-20181122210047-3e3af674fe57
	github.com/davidbegin/go-obs-websocket v0.0.0-20210216055217-ae23f0435168
	github.com/davidbegin/helix v1.7.1-0.20210329150404-e4fbb82c1fc3 // indirect
	github.com/desertbit/timer v0.0.0-20180107155436-c41aec40b27f
	github.com/docker/docker v1.4.2-0.20200221181110-62bd5a33f707
	github.com/fatih/color v1.9.0
	github.com/go-pg/migrations v6.7.3+incompatible
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-pg/pg/v10 v10.3.2
	github.com/golang/mock v1.4.4
	github.com/google/uuid v1.1.2
	github.com/gookit/color v1.3.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/go-getter v1.4.0 // indirect
	github.com/hpcloud/tail v1.0.0 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jackc/pgx/v4 v4.9.0
	github.com/lib/pq v1.8.0
	github.com/linode/linodego v0.23.1
	github.com/linode/terraform-provider-linode v1.13.3
	github.com/lucasb-eyer/go-colorful v1.0.3 // indirect
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/mitchellh/mapstructure v1.4.1
	github.com/nxadm/tail v1.4.8 // indirect
	github.com/prometheus/common v0.20.0 // indirect
	github.com/veandco/go-sdl2 v0.4.4
	golang.org/x/oauth2 v0.0.0-20210323180902-22b0adad7558
	google.golang.org/appengine v1.6.6
	gopkg.in/go-playground/colors.v1 v1.2.0
	gorm.io/driver/postgres v1.0.2
	gorm.io/gorm v1.20.2
	sigs.k8s.io/structured-merge-diff/v3 v3.0.0 // indirect
)
