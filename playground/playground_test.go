package fun_funcs

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_parser"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

var pokemonChoices = [151]string{"bulbasaur", "ivysaur", "venusaur", "charmander", "charmeleon", "charizard", "squirtle", "wartortle", "blastoise", "caterpie", "metapod", "butterfree", "weedle", "kakuna", "beedrill", "pidgey", "pidgeotto", "pidgeot", "rattata", "raticate", "spearow", "fearow", "ekans", "arbok", "pikachu", "raichu", "sandshrew", "sandslash", "nidoran-f", "nidorina", "nidoqueen", "nidoran-m", "nidorino", "nidoking", "clefairy", "clefable", "vulpix", "ninetales", "jigglypuff", "wigglytuff", "zubat", "golbat", "oddish", "gloom", "vileplume", "paras", "parasect", "venonat", "venomoth", "diglett", "dugtrio", "meowth", "persian", "psyduck", "golduck", "mankey", "primeape", "growlithe", "arcanine", "poliwag", "poliwhirl", "poliwrath", "abra", "kadabra", "alakazam", "machop", "machoke", "machamp", "bellsprout", "weepinbell", "victreebel", "tentacool", "tentacruel", "geodude", "graveler", "golem", "ponyta", "rapidash", "slowpoke", "slowbro", "magnemite", "magneton", "farfetchd", "doduo", "dodrio", "seel", "dewgong", "grimer", "muk", "shellder", "cloyster", "gastly", "haunter", "gengar", "onix", "drowzee", "hypno", "krabby", "kingler", "voltorb", "electrode", "exeggcute", "exeggutor", "cubone", "marowak", "hitmonlee", "hitmonchan", "lickitung", "koffing", "weezing", "rhyhorn", "rhydon", "chansey", "tangela", "kangaskhan", "horsea", "seadra", "goldeen", "seaking", "staryu", "starmie", "mr.mime", "scyther", "jynx", "electabuzz", "magmar", "pinsir", "tauros", "magikarp", "gyarados", "lapras", "ditto", "eevee", "vaporeon", "jolteon", "flareon", "porygon", "omanyte", "omastar", "kabuto", "kabutops", "aerodactyl", "snorlax", "articuno", "zapdos", "moltres", "dratini", "dragonair", "dragonite", "mewtwo", "mew"}

func TestCreateMeme(t *testing.T) {
	client := testingClient()
	defer client.Client.Disconnect()

	// scene := PokemonScene

	baseURL := "https://www.pkparaiso.com/imagenes/xy/sprites/animados/"

	for _, pokemon := range pokemonChoices {
		meme, _ := memes.FindDefault(client.DB, pokemon)

		if meme.Name == "" {
			fmt.Printf("meme = %+v\n", pokemon)
			url := fmt.Sprintf("%s/%s.gif", baseURL, pokemon)

			fakeMsg := chat.ChatMessage{
				Streamgod: true,
				Message:   fmt.Sprintf("!gif %s %s", url, pokemon),
			}
			fmt.Sprintf("!gif %s %s", url, pokemon)

			media, err := media_parser.Parse("video", fakeMsg)
			if err != nil {
				fmt.Printf("Error Parsing = %+v\n", err)
			}

			fmt.Printf("media = %+v\n", media)
			// fmt.Printf("media = %+v\n", media)
			// tx := db.Create(&media)
		}

		// fmt.Printf("meme = %+v\n", meme.Name)
		// NewCreateImageOrVideo(client, scene, meme)
		// meme.Verified = true
		// meme.Scene = scene
		// client.DB.Save(&meme)
	}

}
