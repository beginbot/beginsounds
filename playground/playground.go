package fun_funcs

import (
	"log"

	obsws "github.com/davidbegin/go-obs-websocket"
)

func main() {
	c := obsws.Client{Host: "localhost", Port: 4444}
	// var mutex = &sync.Mutex{}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	defer c.Disconnect()
}
