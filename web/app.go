package main

import (
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gorm.io/gorm"
)

// This makes me uncomfortable
// Declaring outside of any scope
// then assigning within a scope
var db *gorm.DB
var audioRequestsTmpl *template.Template
var colorsTmpl *template.Template
var commandTmpl *template.Template
var commandsTmpl *template.Template
var cubeBetsTmpl *template.Template
var helpTmpl *template.Template
var indexTmpl *template.Template
var jesterTmpl *template.Template
var partiesTmpl *template.Template
var themeTmpl *template.Template
var userRequestsTmpl *template.Template
var userTmpl *template.Template
var usersTmpl *template.Template
var packsTmpl *template.Template
var memeTmpl *template.Template
var artMemeTmpl *template.Template
var mediaRequestsTmpl *template.Template

var metadata Metadata
var localMetadata Metadata

// Metadata This is supposed to hold information relavant to all Page structs
// We are not using this properly
// We need to learn about Metadata structs
type Metadata struct {
	Domain string
}

func init() {
	db = database.CreateDBConn("beginsounds4")

	// We could add the other file here, easily
	subTemplate := "templates/sub_templates.tmpl"

	audioRequestsTmpl = template.Must(template.ParseFiles("templates/audio_requests.html", subTemplate))
	colorsTmpl = template.Must(template.ParseFiles("templates/theme.html", subTemplate))
	commandTmpl = template.Must(template.ParseFiles("templates/command.html", subTemplate))
	commandsTmpl = template.Must(template.ParseFiles("templates/commands.html", subTemplate))
	cubeBetsTmpl = template.Must(template.ParseFiles("templates/cube_bets.html", subTemplate))
	helpTmpl = template.Must(template.ParseFiles("templates/help.html", subTemplate))
	indexTmpl = template.Must(template.ParseFiles("templates/index.html", subTemplate))
	jesterTmpl = template.Must(template.ParseFiles("templates/jester.html", subTemplate))
	packsTmpl = template.Must(template.ParseFiles("templates/packs.html", subTemplate))
	partiesTmpl = template.Must(template.ParseFiles("templates/parties.html", subTemplate))
	themeTmpl = template.Must(template.ParseFiles("templates/theme.html", subTemplate))
	userRequestsTmpl = template.Must(template.ParseFiles("templates/user_requests.html", subTemplate))
	userTmpl = template.Must(template.ParseFiles("templates/user.html", subTemplate))
	usersTmpl = template.Must(template.ParseFiles("templates/users.html", subTemplate))
	memeTmpl = template.Must(template.ParseFiles("templates/memes.html", subTemplate))
	artMemeTmpl = template.Must(template.ParseFiles("templates/artmemes.html", subTemplate))
	mediaRequestsTmpl = template.Must(template.ParseFiles("templates/media_requests.html", subTemplate))
}

func main() {
	// ......wat
	metadata = Metadata{
		Domain: "http://localhost:1992",
	}
	localMetadata = Metadata{
		Domain: "http://localhost:1992",
	}

	r := mux.NewRouter()

	r.HandleFunc("/", HomeHandler)
	r.HandleFunc("/audio_requests", AudioRequestHandler)
	r.HandleFunc("/colors", ColorsHandler)
	r.HandleFunc("/commands", CommandsHandler)
	r.HandleFunc("/commands.json", CommandsJsonHandler)
	r.HandleFunc("/commands/{command}", CommandHandler)
	r.HandleFunc("/cube_bets", CubeBetsHandler)
	r.HandleFunc("/help", HelpHandler)
	r.HandleFunc("/memes", MemeHandler)
	r.HandleFunc("/memes/visible.json", VisibleMemeJSONHandler)
	r.HandleFunc("/memes/invisible.json", InvisibleMemeJSONHandler)
	r.HandleFunc("/artmemes", ArtMemeHandler)
	r.HandleFunc("/jester", JesterHandler)
	r.HandleFunc("/parties", PartiesHandler)
	r.HandleFunc("/theme/{theme}", ThemeHandler)
	r.HandleFunc("/user_requests", UserRequestsHandler)
	r.HandleFunc("/media_requests", MediaRequestsHandler)
	r.HandleFunc("/users", UsersHandler)
	r.HandleFunc("/packs", PacksHandler)
	r.HandleFunc("/users/{user}", UserHandler)

	http.ListenAndServe(":1992", r)
	// for { }
}
