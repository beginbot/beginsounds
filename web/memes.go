package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
)

func InvisibleMemeJSONHandler(w http.ResponseWriter, r *http.Request) {
	memes, _ := memes.Invisible(db)
	json.NewEncoder(w).Encode(memes)
}

func VisibleMemeJSONHandler(w http.ResponseWriter, r *http.Request) {
	memes, _ := memes.Visible(db)
	json.NewEncoder(w).Encode(memes)
}

func MemeHandler(w http.ResponseWriter, r *http.Request) {
	website_generator.CreateMemePage(db)
	website_generator.SyncPage("memes")

	memes, _ := memes.AllDefaults(db)

	// ms, _ := memes.All(db)
	page := website_generator.MemePage{
		Memes: memes,
		// Images: images,
		// Videos: videos,
		// Videos: media_request.AllMedia(db),
		// Videos: ms,
		Domain: "http://localhost:1992",
	}
	memeTmpl.Execute(w, page)
}

func ArtMemeHandler(w http.ResponseWriter, r *http.Request) {
	website_generator.CreateArtMemePage(db)
	website_generator.SyncPage("artmemes")
	images := media_request.Videos(db)
	videos := media_request.Images(db)

	files, err := ioutil.ReadDir("assets/static/.")
	if err != nil {
		log.Fatal("Error reading in the ")
	}
	var stylesheets []string
	for _, f := range files {
		stylesheets = append(stylesheets, f.Name())
	}
	page := website_generator.ArtMemesPage{
		Stylesheets: stylesheets,
		Images:      images,
		Videos:      videos,
		Domain:      "http://localhost:1992",
	}
	artMemeTmpl.Execute(w, page)
}
