package obs

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"gitlab.com/beginbot/beginsounds/obs/obs/filters"
)

type Effect struct {
	Command string
	Min     float64
	Max     float64
}

var threeDTransformFilterCommands = map[string]string{
	"!fov":    "Filter.Transform.Camera.FieldOfView",
	"!posx":   "Filter.Transform.Position.X",
	"!posy":   "Filter.Transform.Position.Y",
	"!posz":   "Filter.Transform.Position.Z",
	"!rotx":   "Filter.Transform.Rotation.X",
	"!roty":   "Filter.Transform.Rotation.Y",
	"!rotz":   "Filter.Transform.Rotation.Z",
	"!scalex": "Filter.Transform.Scale.X",
	"!scaley": "Filter.Transform.Scale.Y",
	"!sheerx": "Filter.Transform.Shear.X",
	"!sheery": "Filter.Transform.Shear.Y",
}

var effectLimits = []Effect{
	{
		Command: "!fov",
		Min:     -500,
		Max:     500,
	},
	{
		Command: "!posx",
		Min:     -500,
		Max:     500,
	},
	{
		Command: "!posy",
		Min:     -500,
		Max:     500,
	},
	{
		Command: "!rotx",
		Min:     -5035,
		Max:     5035,
	},
	{
		Command: "!roty",
		Min:     -5035,
		Max:     5035,
	},
	{
		Command: "!rotz",
		Min:     -5035,
		Max:     5035,
	},
	{
		Command: "!scalex",
		Min:     -1000,
		Max:     1000,
	},
	{
		Command: "!scaley",
		Min:     -1000,
		Max:     1000,
	},
	// These are just boring
	// {
	// 	Command: "!sheerx",
	// 	Min:     -1000,
	// 	Max:     1000,
	// },
	// {
	// 	Command: "!sheery",
	// 	Min:     -1000,
	// 	Max:     1000,
	// },
}

func (effect *Effect) RandomVal() float64 {
	rand.Seed(time.Now().UnixNano())
	return float64(rand.Intn(int(effect.Max)-int(effect.Min)) + int(effect.Min))
}

func buildComboFilter(msg string) map[string]interface{} {
	cmds := strings.Split(msg, "&")

	duration := 1000.0
	filterSettings := filters.DefaultTransformFilter
	filterSettings["duration"] = duration

	for _, subCommand := range cmds {
		parts := strings.Split(strings.TrimSpace(subCommand), " ")
		fmt.Printf("\tparts = %+v\n", parts)
		cmd := strings.TrimSpace(parts[0])

		fmt.Printf("\ncmd = %+v\n", cmd)

		for key, filterName := range threeDTransformFilterCommands {
			if cmd == key {
				// if cmd == key || cmd == "!norm" {
				defaultVal := filterSettings[filterName]

				var floatVal float64
				switch defaultVal.(type) {
				case int:
					floatVal = float64(defaultVal.(int))
				case float64:
					floatVal = defaultVal.(float64)
				}

				value := fetchFloat(parts, 2, floatVal)
				filterName, _ := threeDTransformFilterCommands[key]
				filterSettings[filterName] = value
			}
		}

	}
	return filterSettings
}

// This should take in full settings
func triggerFullViewer3DTransform(
	client *OBSClient,
	parts []string,
	settings map[string]interface{},
) {
	source := "gopher"
	if len(parts) > 1 {
		source = parts[1]
	}
	SetSourceFilterSettings(client, source, "viewer_move", settings)

	// This toggling is what I want wait on
	ToggleFilter(client, source, "viewer_move", true)
}

// This is for single settings
func triggerViewer3DTransform(client *OBSClient,
	parts []string,
	key string,
	defaultVal float64,
) {
	source := "gopher"
	if len(parts) > 1 {
		source = parts[1]
	}
	value := fetchFloat(parts, 2, defaultVal)
	filterName, ok := threeDTransformFilterCommands[key]
	if !ok {
		fmt.Printf("Couldn't find Filter for key: %s\n", key)
		return
	}

	secondsDuration := fetchInt(parts, 3, 1)
	duration := secondsDuration * 1000

	filterSettings := filters.DefaultTransformFilter
	filterSettings["duration"] = duration
	filterSettings[filterName] = value
	SetSourceFilterSettings(client, source, "viewer_move", filterSettings)
	ToggleFilter(client, source, "viewer_move", true)
}

// meme.Name,
// effect.Command, // Filter command
// randVal,        // val
// 0.0,            // default
// I don't like this how it references the key:
// which is a user command
func triggerViewer3DTransformSettings(
	client *OBSClient,
	memeName string,
	key string,
	value float64,
) map[string]interface{} {

	filterName, ok := threeDTransformFilterCommands[key]
	if !ok {
		fmt.Printf("Couldn't find Filter for key: %s\n", key)
		return map[string]interface{}{}
	}

	duration := 20000.0

	filterSettings := filters.DefaultTransformFilter
	filterSettings["duration"] = duration
	filterSettings[filterName] = value
	return filterSettings
}

func generateTransformFilterSettings(
	client *OBSClient,
	memeName string,
	filterName string,
	value float64,
) map[string]interface{} {

	duration := 20000.0

	filterSettings := filters.DefaultTransformFilter
	filterSettings["duration"] = duration
	filterSettings[filterName] = value
	return filterSettings
}
