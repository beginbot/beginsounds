package obs

import (
	"fmt"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gorm.io/gorm"
)

type OBSRequest struct {
	ID            int
	ChatMessageID int
	RawMessage    string
	TriggeredAt   *time.Time
	FailedAt      *time.Time
	CreatedAt     time.Time
}

func UnfufilledRequests(db *gorm.DB) []chat.ChatMessage {
	var requests []*OBSRequest
	db.Model(&OBSRequest{}).Where("failed_at IS NULL AND triggered_at IS NULL").Scan(&requests)

	var results []chat.ChatMessage

	for _, req := range requests {
		c := chat.FindByID(db, req.ChatMessageID)
		tx := db.Model(&req).Update("triggered_at", time.Now())
		if tx.Error != nil {
			fmt.Printf("Error Updating triggered_at = %+v\n", tx.Error)
		}
		results = append(results, *c)
	}

	return results
}
