package obs

import (
	"strconv"
	"time"
)

// BigBrain gives the source a bigger brain...duh
func BigBrain(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	filterName := ViewerTransformFilter
	ToggleFilter(client, sourceName, filterName, true)

	start := 0.00
	end := -50.5
	ticker := time.NewTicker(5 * time.Millisecond)

	for i := start; i > end; i -= 0.01 {
		_, _ = FetchFilterSettings(client, sourceName, ViewerTransformFilter)

		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":     1,
			"Filter.Transform.Position.Y": 0,
			"Filter.Transform.Position.Z": 0.00,
			"Filter.Transform.Rotation.X": i,
			"Filter.Transform.Rotation.Y": 0,
			"Filter.Transform.Rotation.Z": 0,
			"Filter.Transform.Scale.Y":    110,
		}
		SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C
	return nil
}

func ChadSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {

	filterName := ViewerTransformFilter
	ToggleFilter(client, sourceName, filterName, true)

	start := 0.00
	end := 259.26

	for i := start; i < end; i += 0.03 {
		_, _ = FetchFilterSettings(client, sourceName, filterName)

		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":     1,
			"Filter.Transform.Position.Y": -0.12999999999999998,
			"Filter.Transform.Rotation.X": 58.65,
			"Filter.Transform.Rotation.Y": 3.03,
			"Filter.Transform.Rotation.Z": 0,
			"Filter.Transform.Scale.X":    111.11,
			"Filter.Transform.Scale.Y":    i,
		}
		sourceName := sourceName
		SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}

	return nil
}

func BlackGlasses(client *OBSClient) {
	ToggleSource(client, PrimaryScene, "i_need_attention", true)
	ToggleFilter(client, "i_need_attention", ViewerTransformFilter, true)

	start := -50.00
	end := 32.61
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i < end; i += 0.01 {
		_, _ = FetchFilterSettings(client, "i_need_attention", ViewerTransformFilter)

		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":             1,
			"Filter.Transform.Camera.FieldOfView": i,
			"Filter.Transform.Position.X":         -i,
			"Filter.Transform.Position.Y":         10,
		}
		sourceName := "i_need_attention"
		filterName := ViewerTransformFilter
		SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C
}

func RiseSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	YModifier := 0.8

	startY := settings["start_y"].(float64)
	endY := settings["end_y"].(float64)
	x := settings["x"].(float64)

	for y := startY; y > endY; y -= YModifier {
		SetSourcePosition(client, sceneName, sourceName, x, y)
	}
	return nil
}

func TallSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {
	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * 20)

	go func() {
		y := 1

		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.Y": y,
				}
				SetSourceFilterSettings(client, source, filterName, filterSettings)
				y = y + 4
			}
		}
	}()

	return nil
}

func WideSource(
	client *OBSClient,
	scene string, // Scene
	source string, // Source
	settings map[string]interface{}, // Settings
) error {

	rawDuration, ok := settings["duration"]
	var duration time.Duration
	if ok {
		duration = rawDuration.(time.Duration)
	} else {
		duration = 7
	}

	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * duration)

	go func() {
		x := 1

		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X": x,
				}
				SetSourceFilterSettings(client, source, filterName, filterSettings)
				x = x + 4
			}
		}
	}()

	return nil

}

func WideBegin(
	client *OBSClient,
	sourceName string,
	duration time.Duration,
) {

	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * duration)

	go func() {
		x := 1

		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X": x,
				}
				SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
				x = x + 4
			}
		}
	}()

}

func TallBegin(client *OBSClient, sourceName string, delay time.Duration) {
	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * 20)

	go func() {
		y := 1

		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.Y": y,
				}
				SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
				y = y + 4
			}
		}
	}()

}

func TheZoom(client *OBSClient, sourceName string, delay time.Duration) {
	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * 7)

	go func() {
		x := 0
		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				// This scaling up is a zoom in a affect
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X": x,
					"Filter.Transform.Scale.Y": x,
				}

				SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
				x++
			}
		}
	}()
}

// This is a Ticker Pattern
// We increase some values according to a pattern
// and then stop based on a Timer
func DelaySpin(
	client *OBSClient,
	sceneName string, sourceName string,
	delay time.Duration,
	duration time.Duration,
) {

	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)

	go func() {
		delayStartTimer := time.NewTimer(time.Second * delay)
		<-delayStartTimer.C
		timer := time.NewTimer(time.Second * duration)

		x := 50
		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    x,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": x,
					"Filter.Transform.Rotation.Z": x,
				}
				SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
				x++
			}
		}
	}()
}

func ReactionZoom(client *OBSClient, source string) {
	ToggleFilter(client, source, "zoom", true)

	start := 100.0
	end := 20.0
	ticker := time.NewTicker(2 * time.Second)

	for i := start; i > end; i -= 20 {
		settings := map[string]interface{}{
			"level": i,
		}
		Zoom(client, MemeScene, source, settings)
		<-ticker.C
	}
}

func RiseFromTheGrave(client *OBSClient) {
	// ToggleSource(c, mutex, PrimaryScene, "i_need_attention", true)
	ToggleFilter(client, BeginCam, ViewerTransformFilter, true)

	start := 175.0
	end := 0.0
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i > end; i -= 0.005 {
		filterSettings := map[string]interface{}{
			"Filter.Transform.Position.Y": i,
		}
		sourceName := BeginCam
		filterName := ViewerTransformFilter
		SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C
}

// UnveilSource rises the source
func UnveilSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {

	filterName := ViewerTransformFilter
	ToggleFilter(client, sourceName, filterName, true)

	// start := 175.0
	// end := 0.0
	rawStartY := settings["start_y"]
	rawEndY := settings["end_y"]
	start := rawStartY.(float64)
	end := rawEndY.(float64)

	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i > end; i -= 0.010 {
		filterSettings := map[string]interface{}{
			"Filter.Transform.Position.Y": i,
		}
		SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C

	return nil
}

func PulsatingHighlight(
	client *OBSClient,
	sourceName string,
	delay time.Duration,
	duration time.Duration,
	toggleOff bool,
) {

	timer := time.NewTimer(duration * time.Second)

	go func() {
		for {
			select {
			case <-timer.C:
				ReturnToNormie(client)
				if toggleOff {
					ToggleSource(client, PrimaryScene, sourceName, false)
				}
				return
			default:
				for _, colorFloat := range colors {
					color, err := strconv.ParseFloat(colorFloat, 32)
					if err != nil {
						client.Logger.Printf("err = %+v\n", err)
					}
					// This might be overkill
					OutlineColor(client, color, sourceName)
					time.Sleep(time.Millisecond * delay)
				}
			}
		}
	}()

}

// =============================== BORING

func Hide(client *OBSClient) {
	ToggleFilter(client, BeginCam, ViewerTransformFilter, true)

	start := 100.0
	end := 150.0

	for i := start; i < end; i += 0.005 {

		filterSettings := map[string]interface{}{
			"Filter.Transform.Position.Y": i,
		}
		sourceName := BeginCam
		filterName := ViewerTransformFilter
		SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
}

// Spin spins the specified source for the delay passed in
func Spin(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},

) error {
	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)
	delay, _ := settings["delay"]
	d := time.Duration(delay.(int))
	timer := time.NewTimer(time.Second * d)

	toggleTransform(client, sourceName, true)
	x := 0
	for {
		select {
		case <-timer.C:
			return nil
		case _ = <-ticker.C:

			filterSettings := map[string]interface{}{
				"Filter.Transform.Scale.X":    x,
				"Filter.Transform.Scale.Y":    x,
				"Filter.Transform.Rotation.X": x,
				"Filter.Transform.Rotation.Y": x,
				"Filter.Transform.Rotation.Z": x,
			}

			SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
			x++
		}
	}
}

func AppearFromThinAir(client *OBSClient, sourceName string) {
	filterName := ViewerTransformFilter
	ToggleFilter(client, sourceName, ViewerTransformFilter, true)

	start := -90.0
	end := 0.
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i < end; i += 0.01 {
		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":     0,
			"Filter.Transform.Rotation.X": i,
		}
		SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C
}

func ZoomFieldOfView(client *OBSClient, sourceName string) {
	filterName := ViewerTransformFilter
	ToggleFilter(client, sourceName, ViewerTransformFilter, true)

	start := 179.0
	end := 75.0
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i > end; i -= 0.02 {
		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":             1,
			"Filter.Transform.Camera.FieldOfView": i,
		}
		SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C
}

// ReverseSlideSource is used to reverse a slide
func ReverseSlideSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) {

	rawStartX, _ := settings["start_x"]
	rawEndX, _ := settings["end_x"]
	rawY, _ := settings["y"]
	rawModifier, _ := settings["modifier"]

	startX := rawStartX.(float64)
	endX := rawEndX.(float64)
	y := rawY.(float64)
	modifier := rawModifier.(float64)

	for x := startX; x > endX; x += modifier {
		SetSourcePosition(client, sceneName, sourceName, x, y)
	}

}

func Fall(
	client *OBSClient,
	sceneName string,
	sourceName string,
	x float64,
	startY float64,
	endY float64,
	modifier float64,
) {
	for y := startY; y < endY; y += modifier {
		SetSourcePosition(client, sceneName, sourceName, x, y)
	}
}

// FallSource - moves source from current Y position to default Y position
func FallSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {

	ToggleSource(client, scene, source, true)

	rawModifier := settings["modifier"]
	rawX := settings["x"]
	rawStartY := settings["start_y"]
	rawEndY := settings["end_y"]

	modifier := rawModifier.(float64)
	x := rawX.(float64)
	startY := rawStartY.(float64)
	endY := rawEndY.(float64)

	Fall(client, scene, source, x, startY, endY, modifier)

	return nil
}
