package obs

import (
	"fmt"
	"log"
	"sync"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gorm.io/gorm"
)

type OBSClient struct {
	Client *obsws.Client
	Mutex  *sync.Mutex
	Logger *log.Logger
	DB     *gorm.DB
}

// ObsCommand represents an OBS command
type ObsCommand struct {
	Name  string `json:"name"`
	Type  string `json:"type"`
	Scene string `json:"scene"`
}

func (o *ObsCommand) String() string {
	return fmt.Sprintf(
		"ObsCommand<Name: %s Type: %s Scene: %s>",
		o.Name,
		o.Type,
		o.Scene,
	)
}
