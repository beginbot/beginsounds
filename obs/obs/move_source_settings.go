package obs

import (
	"math/rand"

	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

type moveSourceSettingsBuilder = func(
	client *OBSClient,
	meme *memes.Meme,
	duration int,
) MoveSourceSettings

func VortexSettings(client *OBSClient, meme *memes.Meme, duration int) MoveSourceSettings {
	if duration == 0 {
		duration = 3000
	}

	settings := defaultMoveSourceSetings(meme.Name)

	settings.Pos.X = meme.X
	// settings.Pos.Y = meme.Y - 100
	settings.Pos.Y = meme.Y - 300
	settings.Bounds.X = 0
	settings.Bounds.Y = 0
	settings.Scale.X = meme.Scale / 12
	settings.Scale.Y = meme.Scale / 12
	// settings.Rot = 900     // A Tasteful as 900
	// settings.Rot = 9000000 // A Tasteful as 900 --> this is wild

	// 5 is nice for spinning
	settings.EasingFunctionMatch = 5
	easeNum := rand.Intn(10)
	settings.EasingFunctionMatch = easeNum

	randRot := rand.Intn(6000)
	settings.Rot = float64(randRot)

	// This is a slow spin
	// settings.Duration = 10000 // Thats 10 seconds
	settings.Duration = 10000
	// settings.Duration = duration
	return settings
}

func VibeCatTakeOver(client *OBSClient, meme *memes.Meme, duration int) MoveSourceSettings {
	if duration == 0 {
		duration = 3000
	}

	settings := defaultMoveSourceSetings(meme.Name)

	var scale float64 = 10
	if meme.Width > 500 {
		scale = 5
	}

	settings.Pos.X = meme.X
	settings.Pos.Y = meme.Y
	settings.Bounds.X = 0
	settings.Bounds.Y = 0
	settings.Scale.X = scale
	settings.Scale.Y = scale

	easeNum := rand.Intn(10)
	settings.EasingFunctionMatch = easeNum
	settings.Duration = duration

	return settings
}

func ProtectBeginSettings(client *OBSClient, meme *memes.Meme, duration int) MoveSourceSettings {
	if duration == 0 {
		duration = 30000
	}

	settings := defaultMoveSourceSetings(meme.Name)
	settings.Scale.X = meme.Scale / 4
	settings.Scale.Y = meme.Scale / 4
	settings.Pos.X = meme.X - 200
	settings.Pos.Y = meme.Y - 100
	return settings
}

func ProtectBeginSettings2(client *OBSClient, meme *memes.Meme, duration int) MoveSourceSettings {
	if duration == 0 {
		duration = 30000
	}

	settings := defaultMoveSourceSetings(meme.Name)
	settings.Scale.X = meme.Scale / 5
	settings.Scale.Y = meme.Scale / 5
	settings.Pos.X = meme.X - 200
	settings.Pos.Y = meme.Y - 100
	return settings
}

func MoveUpSettings(client *OBSClient, meme *memes.Meme, duration int) MoveSourceSettings {
	if duration == 0 {
		duration = 10000
	}

	settings := newDefaultMoveSourceSetings(meme.Name)
	settings.Pos.X = meme.X
	settings.Pos.Y = meme.Y - 200
	return settings
}

func ScaleToCenterSettings(client *OBSClient, source string) MoveSourceSettings {
	settings := newScaleToCenter(source)
	return settings
}
