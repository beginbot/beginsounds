package obs

import obsws "github.com/davidbegin/go-obs-websocket"

// ==========================================================================
// Functions that are filters are settings
// ==========================================================================

func ResetColors(
	client *OBSClient,
	sourceName string,
) {
	settings := map[string]interface{}{
		"brightness": 0,
		"color":      Red,
		"contrast":   0,
		"gamma":      0,
		"hue_shift":  0,
		"opacity":    0,
		"saturation": 0,
	}

	filterName := "color_correction"
	SetSourceFilterSettings(client, sourceName, filterName, settings)
}

func SetColorFade(
	client *OBSClient,
	sourceName string,
	filterName string,
	color float64,
) {
	settings := map[string]interface{}{
		"brightness":     0,
		"color":          color,
		"contrast":       0,
		"duration":       10000,
		"filter":         "color_correction",
		"gamma":          0,
		"hue_shift":      0,
		"opacity":        32,
		"saturation":     0,
		"single_setting": false,
		"start_trigger":  5,
		"stop_trigger":   4,
		"value_type":     0,
	}
	SetSourceFilterSettings(client, sourceName, filterName, settings)
}

// TODO: This needs to take in Settings
// Turn it on
func OutlineColor(
	client *OBSClient,
	color float64,
	sourceName string,
) {
	filterName := "outline"
	filterSettings := map[string]interface{}{"Filter.SDFEffects.Outline.Color": color}
	req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
	client.execOBSCommand(&req)
}
