package obs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"sync"
	"testing"
	"time"

	obsws "github.com/davidbegin/go-obs-websocket"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gitlab.com/beginbot/beginsounds/pkg/player"
)

func NoTestUpdateMoveFilterTwice(t *testing.T) {
	client := TestingClient()
	defer client.Client.Disconnect()
	memeTriforce(client)
	return

	meme, _ := memes.FindDefault(client.DB, "vibecat")
	settings := defaultMoveSourceSetings("alex")
	meme, _ = memes.FindDefault(client.DB, "alex")
	settings.Pos.X = meme.X
	settings.Pos.Y = meme.Y
	// settings.Rot = 3240
	// settings.Rot = 10080
	settings.Rot = 36000
	// settings.Rot = 720
	// settings.EasingFunctionMatch = 7

	settings.EasingFunctionMatch = 5
	// settings.TransformText.Pos.X = 200
	// settings.TransformText.Pos.Y = -200
	filterSettings := settings.ToMap()
	fmt.Printf("sfilterSettings = %+v\n", filterSettings)
	SetSourceFilterSettings(client, "viewer_memes", "MoveSourceAlex", filterSettings)
}

func NoTestFilterSettingFetching(t *testing.T) {
	db := database.CreateDBConn("beginsounds4")
	c := obsws.Client{Host: "localhost", Port: 4444}
	var mutex = &sync.Mutex{}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	defer c.Disconnect()

	fmt.Println("HsElo")
	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	client := OBSClient{
		Client: &c,
		Mutex:  mutex,
		Logger: logger,
		DB:     db,
	}
	// meme, _ := memes.FindDefault(db, "primetime")
	meme, _ := memes.FindDefault(db, "melkeycam")
	riseFromHottube(&client, meme)
	return
	// settings := FetchFilterSettings(&client, PrimaryScene, "MoveBeginCam")
	// fmt.Printf("ssettings = %+v\n", settings)
}

func NoTestDynamicSourceMove(t *testing.T) {
	db := database.CreateDBConn("beginsounds4")
	c := obsws.Client{Host: "localhost", Port: 4444}
	var mutex = &sync.Mutex{}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	defer c.Disconnect()
	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	client := OBSClient{
		Client: &c,
		Mutex:  mutex,
		Logger: logger,
		DB:     db,
	}

	// defaultMoveBeginCamSettings.Pos.X = 0.0
	// defaultMoveBeginCamSettings.Pos.Y = 500.0
	// defaultMoveBeginCamSettings.Bounds.Y = 500.0
	// defaultMoveBeginCamSettings.Bounds.Y = 100.0
	// defaultMoveBeginCamSettings.Duration = 300.0
	filterSettings := defaultMoveBeginCamSettings.ToMap()
	fmt.Printf("filterSettings = %+v\n", filterSettings)
	SetSourceFilterSettings(&client, "Primary", "MoveBeginCam", filterSettings)

}

func NoTestSourceVisibility(t *testing.T) {
	db := database.CreateDBConn("beginsounds4")
	c := obsws.Client{Host: "localhost", Port: 4444}
	var mutex = &sync.Mutex{}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	defer c.Disconnect()
	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	client := OBSClient{
		Client: &c,
		Mutex:  mutex,
		Logger: logger,
		DB:     db,
	}

	rand.Seed(time.Now().UnixNano())
	allMemes, _ := memes.Visible(db)
	allReqs := []map[string]interface{}{}

	for _, meme := range allMemes {
		fmt.Printf("Memes21: %+v\n", meme)
		// RandomTransformRequest(&client, *meme, false)
		reqs := RandomTransformRequestRaw(&client, *meme, false)
		allReqs = append(allReqs, reqs...)
		// rand.Seed(time.Now().UnixNano())
		// reqs1 := RandomTransformRequestRaw(&client, *meme, false)
		// allReqs = append(allReqs, reqs1...)

		// flatten arrays
		// ToggleFilter(client, source, "normie_transform", true)
		// ToggleFilter(&client, meme.Name, "viewer_transform", true)
		// ToggleFilter(&client, meme.Name, "viewer_normie_move", true)
		// ToggleFilter(&client, meme.Name, "normie_transform", true)
	}

	BatchRequest(&client, allReqs)

	// We need Visible source
	// sources, _ := SourcesByVisibility(&client, MemeScene)
	// for _, source := range sources {
	// 	ToggleSource(&client, MemeScene, source, false)
	// }
}

func NoTestHotKeys(t *testing.T) {
	db := database.CreateDBConn("beginsounds4")
	c := obsws.Client{Host: "localhost", Port: 4444}
	var mutex = &sync.Mutex{}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	defer c.Disconnect()
	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	client := OBSClient{
		Client: &c,
		Mutex:  mutex,
		Logger: logger,
		DB:     db,
	}

	// {
	//     "alt": true,
	//     "command": true,
	//     "control": true,
	//     "key": "OBS_KEY_O",
	//     "shift": true
	// }

	// req := obsws.NewTriggerHotkeyBySequenceRequest(
	// 	"OBS_KEY_O",
	// 	map[string]interface{}{},
	// 	true,
	// 	true,
	// 	true,
	// 	true,
	// )
	// client.execOBSCommand(&req)
	// keyModifiers map[string]interface{},
	// keyModifiersShift bool,
	// keyModifiersAlt bool,
	// keyModifiersControl bool,
	// keyModifiersCommand bool,
	// This works

	// This does not work

	// This works
	req := obsws.NewTriggerHotkeyBySequenceRequest(
		"OBS_KEY_8",
		map[string]interface{}{
			"shift":   true,
			"control": true,
			"command": true,
			"alt":     true,
		},
		false, // shift
		true,  // alt
		false, // control
		false, // command
	)
	_, err := client.execOBSCommand(&req)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	// This works
	// req := obsws.NewTriggerHotkeyBySequenceRequest(
	// 	"OBS_KEY_B",
	// 	map[string]interface{}{},
	// 	false, // shift
	// 	false, // alt
	// 	false, // control
	// 	false, // command
	// )
	// _, err := client.execOBSCommand(&req)
	// if err != nil {
	// 	fmt.Printf("err = %+v\n", err)
	// }
	// res := resp.(obsws.TriggerHotkeyBySequenceResponse)
	// fmt.Printf("res.Status = %+v\n", res.Status())
}

func NoTestFilterChanging(t *testing.T) {

	// add visible on musadgaemes
	fmt.Println("xag dgsg, isdgfsddgdsdasegifsdffgfgaalsdkfja")
	rand.Seed(time.Now().UnixNano())
	db := database.CreateDBConn("beginsounds4")

	c := obsws.Client{Host: "localhost", Port: 4444}
	var mutex = &sync.Mutex{}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	defer c.Disconnect()
	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	client := OBSClient{
		Client: &c,
		Mutex:  mutex,
		Logger: logger,
		DB:     db,
	}

	// You can use log.Println with these flags `log.SetFlags(log.LstdFlags | log.Lshortfile` that way you can also have date attached to each log
	// logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	var count int

	for {
		if count > 3 {
			t.Error()
			panic("Aa,sdfssdfg,HH")
		}
		ms, _ := memes.AllDefaults(db)
		i := rand.Intn(len(ms))
		meme := ms[i]
		fmt.Printf("meme = %+v\n", meme)
		// meme, _ := memes.Find(db, "spatula")
		renderReq := obsws.NewSetSceneItemRenderRequest(MemeScene, meme.Name, 0, true)
		ymin := 0
		ymax := 600
		// ymax := 720
		yval := rand.Intn(ymax-ymin) + ymin

		xmin := 0
		xmax := 1000
		// xmax := 1280
		xval := rand.Intn(xmax-xmin) + xmin

		// This needs to be random
		genericEffectOps := make([]interface{}, len(effectLimits))
		// how do get the keys of a map
		//
		for i, v := range effectLimits {
			genericEffectOps[i] = v
		}
		shufEffects := shuffle(genericEffectOps)

		effect := shufEffects[0].(Effect)
		randVal := effect.RandomVal()

		fmt.Printf("\teffect = %+v\n", effect)
		settings := triggerViewer3DTransformSettings(
			&client,
			meme.Name,
			effect.Command, // Filter command
			randVal,        // val
		)
		fmt.Printf("\tsettings = %+v\n", settings)
		reqs := []map[string]interface{}{}

		tReq := obsws.NewSetSourceFilterSettingsRequest(meme.Name, "viewer_transform", settings)
		tReqMap := requestToMap(&tReq)

		ttReq := obsws.NewSetSourceFilterVisibilityRequest(meme.Name, "viewer_move", true)
		ttReqMap := requestToMap(&ttReq)

		req1 := obsws.NewSetSceneItemPositionRequest(MemeScene, meme.Name, float64(xval), float64(yval))
		reqMap := requestToMap(&req1)
		renderReqMap := requestToMap(&renderReq)
		reqs = append(reqs, reqMap)
		reqs = append(reqs, renderReqMap)
		reqs = append(reqs, tReqMap)
		reqs = append(reqs, ttReqMap)
		fmt.Println("Batch Request")
		BatchRequest(&client, reqs)
		count = count + 1
	}
}

func NoTestCombinedMessages(t *testing.T) {
	msg := "!rotz bmo 90 & !scaley bmo 180"
	res := buildComboFilter(msg)

	if res["Filter.Transform.Rotation.Z"] != 90.0 {
		t.Errorf("Incorrect Settings: %+v", res)
	}
	if res["Filter.Transform.Scale.Y"] != 180.0 {
		t.Errorf("Incorrect Settings: %+v", res)
	}
}

func NoTestSceneSaver(t *testing.T) {
	c := obsws.Client{Host: "localhost", Port: 4444}
	var mutex = &sync.Mutex{}

	// You can use log.Println with these flags `log.SetFlags(log.LstdFlags | log.Lshortfile` that way you can also have date attached to each log

	// logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	client := OBSClient{
		Client: &c,
		Mutex:  mutex,
		Logger: logger,
	}
	// Quick Dumb Theory
	// When you query a filters default settings
	// you get nothing back
	// s := FetchFilterSettings(&client, "tswift", "greenscreen")
	// s := FetchFilterSettings(&client, "922", "transform")
	// fmt.Printf("s = %+v\n", s)
	// <-time.NewTimer(time.Second * 2).C
	return
	sources, _ := SourcesByVisibility(&client, MemeScene)

	for _, source := range sources {
		fmt.Println("source: ", source)
		MemeInfo(&client, MemeScene, source)
	}
}

// We can test seomtjin

// !find sunbegin|!move sunbegin 881 167|!scale sunbegin 0.87|!rotate sunbegin 0
func TestChatExploder(t *testing.T) {
	fmt.Println("START chatExploder")
	name := "young.thug"
	msg := "!reset clippy | !slide clippy"
	cm := chat.ChatMessage{
		PlayerName: name,
		Message:    msg,
	}

	msgs := make(chan chat.ChatMessage, 1000)
	msgs <- cm

	results := ChatExploder(msgs)
	timer := time.NewTimer(time.Millisecond * 150)

	select {
	case <-timer.C:
		t.Errorf("Didn't get message")
	case res1 := <-results:
		if res1.Message != "!reset clippy" {
			t.Errorf("Wrong Message: %s", res1.Message)
		}
		res2 := <-results
		if res2.Message != "!slide clippy" {
			t.Errorf("Wrong Message: %s", res2.Message)
		}

	}

}

func TestChatExploderPlusCombos(t *testing.T) {
	name := "young.thug"
	msg := "!norm chadmander3 | !rotx chadmander3 180 & !roty chadmander3 360 & !fov chadmander3 -100 | !rotx chadmander3 180 & !roty chadmander3 180 & !fov chadmander3 -100"
	cm := chat.ChatMessage{
		PlayerName: name,
		Message:    msg,
	}

	msgs := make(chan chat.ChatMessage, 1000)
	msgs <- cm

	results := ChatExploder(msgs)
	timer := time.NewTimer(time.Millisecond * 150)

	select {
	case <-timer.C:
		t.Errorf("Didn't get message")
	case res1 := <-results:
		if res1.Message != "!norm chadmander3" {
			t.Errorf("Wrong Message: %s", res1.Message)
		}

		res2 := <-results
		if res2.Message != "!rotx chadmander3 180 & !roty chadmander3 360 & !fov chadmander3 -100" {
			t.Errorf("Wrong Message: %s", res2.Message)
		}

		res3 := <-results
		if res3.Message != "!rotx chadmander3 180 & !roty chadmander3 180 & !fov chadmander3 -100" {
			t.Errorf("Wrong Message: %s", res2.Message)
		}

	}

}

func NoTestDynamicSourceMove2(t *testing.T) {
	fmt.Println("Nice")
	db := database.CreateDBConn("beginsounds4")
	c := obsws.Client{Host: "localhost", Port: 4444}
	var mutex = &sync.Mutex{}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	defer c.Disconnect()
	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	client := OBSClient{
		Client: &c,
		Mutex:  mutex,
		Logger: logger,
		DB:     db,
	}

	filterSettings, _ := FetchFilterSettings(
		&client,
		"viewer_memes",
		"MoveSourceAlex",
	)

	var result MoveSourceSettings
	err := mapstructure.Decode(filterSettings, &result)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	transformText := ParseRawTransformText(result.RawTransformText)

	// transformText.Pos.Y = meme.Y - 100
	// transformText.Pos.X = meme.X - 100

	// transformText.Pos.Y = meme.Y - 100
	// transformText.Pos.X = meme.X - 100

	// 500 is not a lot
	// result.Pos.Y = 100

	result.Duration = 1000

	// Fall 200
	result.Pos.Y = result.Pos.Y - 200

	// Move Left 200
	result.Pos.X = result.Pos.X - 200

	bottomRightX := 1150.0
	bottomRightY := 600.0
	result.Pos.X = bottomRightX
	result.Pos.Y = bottomRightY

	topRightX := 1150.0
	topRightY := 0.0
	result.Pos.X = topRightX
	result.Pos.Y = topRightY

	topLeftX := 0.0
	topLeftY := 0.0
	result.Pos.X = topLeftX
	result.Pos.Y = topLeftY

	inrec, _ := json.Marshal(result)
	var inInterface map[string]interface{}
	json.Unmarshal(inrec, &inInterface)
	inInterface["transform_text"] = transformText.String()

	SetSourceFilterSettings(&client, "viewer_memes", "MoveSourceAlex", inInterface)
	ToggleFilter(&client, "viewer_memes", "MoveSourceAlex", true)
}

func NotTestSavingMeme(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	p := player.CreatePlayerFromName(db, "young.thug")
	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	c := obsws.Client{Host: "localhost", Port: 4444, Logger: logger}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	defer c.Disconnect()
	// var mutex = &sync.Mutex{}
	// source := "carlvan"

	// client := OBSClient{
	// 	Client: &c,
	// 	Mutex:  mutex,
	// 	Logger: logger,
	// }
	// obsMeme := MemeInfo(&client, source)

	msg := "!save carlvan"

	parsedCmd := chat.ParsedCommand{
		Name: "save",
		// TargetMedia     string
	}

	// we need a message that says save a meme
	// and we need to make sure it saves a Meme
	cm := chat.ChatMessage{
		PlayerName: p.Name,
		PlayerID:   p.ID,
		Message:    msg,
		Streamgod:  true,
		ParsedCmd:  parsedCmd,
		Parts:      []string{"!save", "carlvan"},
	}

	fmt.Printf("cm = %+v\n", cm)

	m, _ := memes.Find(db, "carlvan")
	if m.X != 700.0 {
		t.Errorf("Error Saving Meme: %+v", m)
	}
}
