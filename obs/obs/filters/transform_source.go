package filters

var ViewerTransformFilter = "viewer_transform"

var DefaultTransformFilter = map[string]interface{}{
	"Filter.Transform.Camera.FieldOfView": 90,
	"Filter.Transform.Position.X":         0,
	"Filter.Transform.Position.Y":         0,
	"Filter.Transform.Position.Z":         0,
	"Filter.Transform.Rotation.X":         0,
	"Filter.Transform.Rotation.Y":         0,
	"Filter.Transform.Rotation.Z":         0,
	"Filter.Transform.Scale.X":            100,
	"Filter.Transform.Scale.Y":            100,
	"Filter.Transform.Shear.X":            0,
	"Filter.Transform.Shear.Y":            0,
	"duration":                            1000,
	"filter":                              ViewerTransformFilter,
	"setting_float":                       0,
	"single_setting":                      false,
	"start_trigger":                       0,
	"value_type":                          0,
}

// To Map
// To JSON
// yesnojh
