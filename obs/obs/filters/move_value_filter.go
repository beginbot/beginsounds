package filters

var ColorCorrectionFilter = "color_correction"
var ColorCorrectionFilterType = "color_correction"
var Green = "4.278255445e+09"

var MoveColorFilterSettings = map[string]interface{}{
	"brightness":     0,
	"color":          Green,
	"contrast":       0,
	"duration":       10000,
	"filter":         ColorCorrectionFilter,
	"gamma":          0,
	"hue_shift":      0,
	"opacity":        64,
	"saturation":     0,
	"single_setting": false,
	"start_trigger":  5,
	"stop_trigger":   4,
	"value_type":     0,
}

var ColorCorrectionSettings = map[string]interface{}{
	"brightness": 0,
	"color":      4.278190335e+09,
	"contrast":   0,
	"duration":   10000,
	"filter":     ColorCorrectionFilterType,
	"gamma":      0,
	"hue_shift":  0,
	"opacity":    32,
	"saturation": 0,
}
