package filters

type ColorCorrection struct {
	Brightness    float64
	Color         string
	Contrast      float64
	Duration      float64
	Filter        string
	Gamma         float64
	HueShift      float64
	Opacity       float64
	Saturation    float64
	SingleSetting bool
	StartTrigger  int
	StopTrigger   int
	ValueType     int
}
