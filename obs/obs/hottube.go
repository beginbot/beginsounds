package obs

import "gitlab.com/beginbot/beginsounds/pkg/memes"

func riseFromHottube(client *OBSClient, meme *memes.Meme) {
	filterSettings := map[string]interface{}{
		"Filter.Transform.Camera.FieldOfView": 90.00,
		"Filter.Transform.Position.Y":         300,
		"Filter.Transform.Position.Z":         0.00,
		"Filter.Transform.Rotation.X":         0.00,
		"Filter.Transform.Rotation.Y":         0.00,
		"Filter.Transform.Rotation.Z":         0.00,
		"Filter.Transform.Scale.X":            90.00,
		"Filter.Transform.Scale.Y":            90.00,
		"Filter.Transform.Shear.X":            0.0,
		"Filter.Transform.Shear.Y":            0.0,
	}
	sourceName := meme.Name
	filterName := "viewer_transform"
	SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
	ToggleFilter(client, sourceName, "viewer_transform", true)
	ToggleSource(client, MemeScene, sourceName, true)

	// Good for melkey
	// r1, r2 := MoveXAndYWithFilter(client, meme, 75, 200)

	// For Prime
	// r1, r2 := MoveXAndYWithFilter(client, meme, 50, 250)

	// Intween
	r1, r2 := MoveXAndYWithFilter(client, meme, 50, 200)
	reqs := []map[string]interface{}{}
	reqs = append(reqs, r1)
	reqs = append(reqs, r2)
	BatchRequest(client, reqs)

	filterSettings["Filter.Transform.Position.Y"] = 0.0

	SetSourceFilterSettings(client, sourceName, "viewer_move", filterSettings)
	// ResetTransform(client, meme.Name)
	ToggleFilter(client, sourceName, "viewer_move", true)
}
