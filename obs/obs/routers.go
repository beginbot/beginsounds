package obs

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

// UserEffects are effects related to actual Twitch Users/Other Streamers
func FilterEffects(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	filterName := "greenscreen"
	if len(obsCmd.Parts) > 2 {
		filterName = obsCmd.Parts[2]
	}

	// This is for handling rotx, roty, rotz
	for key := range threeDTransformFilterCommands {
		if cmd == key {
			subCmds := strings.Split(obsCmd.Message.Message, "|")
			for _, cmd := range subCmds {
				subMsg := strings.TrimSpace(cmd)
				fmt.Printf("\n\tTriggering subMsg: %+v\n", subMsg)
				settings := buildComboFilter(subMsg)
				parts := strings.Split(subMsg, " ")
				triggerFullViewer3DTransform(client, parts, settings)
				<-time.NewTimer(2 * time.Second).C
			}

		}
	}

	switch cmd {

	case "hideinscene":
		if len(obsCmd.Parts) > 1 {
			potentialScene := obsCmd.Parts[1]
			for nickname, scene := range Scenes {
				if potentialScene == nickname || potentialScene == scene {
					sources, _ := SourcesByVisibility(client, potentialScene)
					for _, source := range sources {
						ToggleSource(client, scene, source, false)
					}
				}
			}
		}
	case "!gottacatchemall":
		// Iterate through all memes in the Pokemon Scene
		// and reset, transform

		fmt.Println("Triggering new pokemon effectd")
		pokemen, _ := memes.FromScene(client.DB, PokemonScene)
		for _, pokemon := range pokemen {
			ResetMeme(client, pokemon)
		}
	case "!hotkey":
		hotkey := "l"
		if len(obsCmd.Parts) > 1 {
			hotkey = obsCmd.Parts[1]
		}
		keyName := fmt.Sprintf("OBS_KEY_%s", strings.ToUpper(hotkey))
		req := obsws.NewTriggerHotkeyBySequenceRequest(
			keyName,
			map[string]interface{}{
				"shift":   true,
				"control": true,
				"command": true,
				"alt":     true,
			},
			true, // shift
			true, // alt
			true, // control
			true, // command
		)
		_, err := client.execOBSCommand(&req)
		if err != nil {
			fmt.Printf("err = %+v\n", err)
		}

	case "!artride":
		// x := obsCmd.Settings["x"].(float64)
		// y := obsCmd.Settings["y"].(float64)
		x := 0.0
		y := 500.0
		NewMoveSource(client, obsCmd.Scene, obsCmd.Source, x, y, 1.0)
		desiredSettings := []TransformSetting{
			{
				Name:   "Filter.Transform.Rotation.Y",
				Value:  0.0,
				Range:  0,
				Random: false,
			},
			{
				Name:   "Filter.Transform.Rotation.X",
				Value:  180,
				Range:  0,
				Random: false,
			},
			{
				Name:   "Filter.Transform.Scale.X",
				Value:  0.01,
				Range:  0.0,
				Random: false,
			},
			{
				Name:   "Filter.Transform.Scale.Y",
				Value:  0.01,
				Range:  0.0,
				Random: false,
			},
		}
		reqs := []map[string]interface{}{}
		toggleMeme, _ := memes.FindDefault(client.DB, obsCmd.Source)
		settings := generateTransformFilterSettingsFromList(desiredSettings)
		settings["duration"] = 5000
		toggleReqs := UpdateTransformFilterAndCall(client, toggleMeme, settings)
		reqs = append(reqs, toggleReqs...)
		BatchRequest(client, reqs)
	case "!attac":
		ToggleBitLightSaberBattle(client)
	case "!clonefilter":
		// !clonefilter bart MoveSourceGopher MoveSourceBart
		// !clonefilter gopher technofroggo

		if len(obsCmd.Parts) == 3 {
			sourceSource := obsCmd.Parts[1]
			destSource := obsCmd.Parts[2]
			sourceFilter := fmt.Sprintf("MoveSource%s", strings.Title(sourceSource))
			destFilter := fmt.Sprintf("MoveSource%s", strings.Title(destSource))
			CloneFilter(client, MemeScene, destSource, sourceFilter, destFilter)
		}
	case "!move":
		x := obsCmd.Settings["x"].(float64)
		y := obsCmd.Settings["y"].(float64)
		scale := obsCmd.Settings["scale"].(float64)

		fmt.Printf("Moving Scene: %s Source: %s\n", obsCmd.Scene, obsCmd.Source)
		NewMoveSource(client, obsCmd.Scene, obsCmd.Source, x, y, scale)
	case "!move2":
		x := obsCmd.Settings["x"].(float64)
		y := obsCmd.Settings["y"].(float64)
		meme, _ := memes.FindCurrent(client.DB, obsCmd.Source)
		MoveXAndYRelativeWithFilter(client, meme, x, y)
	case "!memetriforce":
		fmt.Println("Meme Triforce")
		memeTriforce(client)
	case "!protect2":
		CallMoveSourceOnMemes(client, ProtectBeginSettings2)
	case "!protect":
		CallMoveSourceOnMemes(client, ProtectBeginSettings)
	case "!vibecat_time":
		CallMoveSourceOnMemes(client, VibeCatTakeOver)
	case "!vortex":
		CallMoveSourceOnMemes(client, VortexSettings)
	case "!newmovex":
		source := "gopher"
		if len(obsCmd.Parts) > 1 {
			source = obsCmd.Parts[1]
		}
		// How do we find the source
		// MoveSource(client, MemeScene, meme.Name, moveSettings)
		settings := defaultMoveSourceSetings(source)
		meme, _ := memes.FindDefault(client.DB, source)

		// pos: x 1100 y 50 rot: 1150.0 scale: x 0.017 y 0.017 bounds: x 0 y 0 crop: l 0 t 0 r 0 b 0
		// This means fade into the background
		// pos: x 1103 y 583 rot: 0.0 scale: x 1.000 y 1.000 bounds: x 117 y 155 crop: l 0 t 0 r 0 b 0
		settings.Pos.X = meme.X
		settings.Pos.Y = meme.Y - 100
		settings.Bounds.X = 0
		settings.Bounds.Y = 0
		settings.Rot = 0.0
		settings.EasingFunctionMatch = 5

		// lets save the JSON Settings

		// It's always
		settings.Scale.X = -1
		settings.Scale.Y = 1

		// settings.Rot = 36000
		// settings.Rot = 720
		// settings.EasingFunctionMatch = 7
		// settings.TransformText.Pos.X = 200
		// settings.TransformText.Pos.Y = -200

		filterSettings := settings.ToMap()
		fmt.Printf("filterSettings = %+v\n", filterSettings)

		// save the settings

		jsonRes, _ := json.Marshal(filterSettings)
		// we need to save to file
		buildFile := fmt.Sprintf("tmp/test.json")
		f, err := os.Create(buildFile)
		if err != nil {
			fmt.Printf("\nError Creating Build File: %s", err)
		}
		json.NewEncoder(f).Encode(jsonRes)

		// We have to Target the right Filter name here
		// setSourceFilterSettings(client, "viewer_memes", "MoveSourceAlex", filterSettings)
		// ToggleFilter(client, MemeScene, "MoveSourceAlex", true)

		// Then we need to Toggle
	case "!stretch":
		UpdateViewerMoveDuration(client)
	case "!rand2":
		RandomEffectOnVisibleMemes(client, true)
	case "!rand":
		RandomEffectOnVisibleMemes(client, false)
	case "!pure":
		reqs := []map[string]interface{}{}

		unorderedMemes, _ := memes.All(client.DB)

		genericMemes := make([]interface{}, len(unorderedMemes))
		for i, v := range unorderedMemes {
			genericMemes[i] = v
		}
		genShufMemes := shuffle(genericMemes)

		for i, m := range genShufMemes {
			meme := m.(*memes.Meme)
			// panic: interface conversion: interface {} is *memes.Meme, not memes.Meme

			if i > 10 {
				break
			}
			renderReq := obsws.NewSetSceneItemRenderRequest(MemeScene, meme.Name, 0, true)
			rand.Seed(time.Now().UnixNano())
			ymin := 0
			ymax := 720
			yval := rand.Intn(ymax-ymin) + ymin

			xmin := 0
			xmax := 1280
			xval := rand.Intn(xmax-xmin) + xmin

			// This needs to be random
			genericEffectOps := make([]interface{}, len(effectLimits))
			// how do get the keys of a map
			//
			for i, v := range effectLimits {
				genericEffectOps[i] = v
			}
			shufEffects := shuffle(genericEffectOps)

			effect := shufEffects[0].(Effect)
			randVal := effect.RandomVal()
			settings := triggerViewer3DTransformSettings(
				client,
				meme.Name,
				effect.Command,
				randVal,
			)
			tReq := obsws.NewSetSourceFilterSettingsRequest(meme.Name, "viewer_transform", settings)
			tReqMap := requestToMap(&tReq)

			ttReq := obsws.NewSetSourceFilterVisibilityRequest(meme.Name, "viewer_move", true)
			ttReqMap := requestToMap(&ttReq)

			req1 := obsws.NewSetSceneItemPositionRequest(MemeScene, meme.Name, float64(xval), float64(yval))
			reqMap := requestToMap(&req1)
			renderReqMap := requestToMap(&renderReq)
			reqs = append(reqs, reqMap)
			reqs = append(reqs, renderReqMap)
			reqs = append(reqs, tReqMap)
			reqs = append(reqs, ttReqMap)
		}
		BatchRequest(client, reqs)

	case "!batch":
		scene := PrimaryScene
		req1 := obsws.NewSetSceneItemPositionRequest(scene, "BeginCam", 100.0, 100.0)
		req2 := obsws.NewSetSceneItemPositionRequest(MemeScene, "grill", 100.0, 400.0)

		req1Map := requestToMap(&req1)
		req2Map := requestToMap(&req2)

		reqs := []map[string]interface{}{
			req1Map,
			req2Map,
		}
		BatchRequest(client, reqs)

	case "!addmemefilters":
		source := "gopher"
		if len(obsCmd.Parts) > 1 {
			source = obsCmd.Parts[1]
		}
		// !viewer_move
		AddViewerMoveFilter(client, MemeScene, source)
		// !viewer_transform
		AddViewerTransformFilter(client, MemeScene, source)
		// !viewer_normie_move
		AddViewerNormieMoveFilter(client, MemeScene, source)
	case "!normiememe", "!norm":
		// If the filter is transforming incorrectly
		// we are screwed
		// we could update the filter settings here to be the default
		ToggleFilter(client, obsCmd.Source, "viewer_normie_move", true)
	case "!viewer_filter":
		source := "gopher"
		// - We need to return the transform filter to normal
		// - flip it off
		//
		// - update the filter settings of viewer_move
		// - turn on the viewer_transform
		// - trigger viewer_move

		if len(obsCmd.Parts) > 1 {
			source = obsCmd.Parts[1]
		}
		x := fetchFloat(obsCmd.Parts, 2, 0.0)
		y := fetchFloat(obsCmd.Parts, 3, 0.0)
		xRotation := fetchFloat(obsCmd.Parts, 4, 0.0)
		yRotation := fetchFloat(obsCmd.Parts, 5, 0.0)
		zRotation := fetchFloat(obsCmd.Parts, 6, 0.0)

		// can we update the view
		filterSettings := map[string]interface{}{
			"Filter.Transform.Scale.X":    x,
			"Filter.Transform.Scale.Y":    y,
			"Filter.Transform.Rotation.X": xRotation,
			"Filter.Transform.Rotation.Y": yRotation,
			"Filter.Transform.Rotation.Z": zRotation,
			"duration":                    7000,
		}
		// setSourceFilterSettings(client, source, "viewer_transform", filterSettings)
		SetSourceFilterSettings(client, source, "viewer_move", filterSettings)
		ToggleFilter(client, source, "viewer_move", true)
		// ToggleFilter(client, source, "viewer_transform", true)
	case "!hittheroad":
		ShowSource(client, PrimaryScene, "BGMemes", map[string]interface{}{})
	case "!greenscreen":
		fmt.Println("TRYING TO ADD GREEN SCREEN")
		AddGreenScreenFilter(client, obsCmd.Scene, obsCmd.Source)
	case "!filteron":
		ToggleFilter(client, obsCmd.Source, filterName, true)
	case "!filteroff":
		ToggleFilter(client, obsCmd.Source, filterName, false)
	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

// UserEffects are effects related to actual Twitch Users/Other Streamers
func UserEffects(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	switch cmd {
	case "!bg":
		BlackGlasses(client)
		ReturnToNormie(client)
	case "!obieru":
		sourceName := "obieru"
		ToggleSource(client, MemeScene, sourceName, true)
		max := 1000
		min := 200
		x := float64(rand.Intn(max-min) + min)
		obieruRising := map[string]interface{}{
			"start_y": 600.0,
			"end_y":   -500.0,
			"x":       x,
		}
		RiseSource(client, MemeScene, sourceName, obieruRising)
		ToggleSource(client, MemeScene, sourceName, false)
	case "!melkman2":
		ToggleSource(client, MemeScene, "melkman", true)
		SetSourcePosition(client, MemeScene, "melkman", 0, 0)
		ZoomAndSpin(client, "melkman", 6)
	case "!melkman":
		ToggleSource(client, MemeScene, "melkman", true)
		max := 1000
		min := 200
		x := float64(rand.Intn(max-min) + min)
		settings := map[string]interface{}{
			"start_y": 600.0,
			"end_y":   -500.0,
			"x":       x,
		}
		RiseSource(client, MemeScene, "melkman", settings)
		ToggleSource(client, MemeScene, "melkman", false)
	case "!primeagen":
		ToggleSource(client, MemeScene, "primeagen", true)
		sSettings := map[string]interface{}{
			"start_x": 0.0,
			"end_x":   1165.0,
			"y":       631.0,
		}
		SlideSource(client, MemeScene, "primeagen", sSettings)
	case "!roxkstar1":
		settings := map[string]interface{}{}
		ToggleSource(client, MemeScene, "roxkstar", true)
		BigBrain(client, "BeginCam", "roxkstar", settings)
		ToggleSource(client, MemeScene, "roxkstar", false)
	case "!teej1":
		settings := map[string]interface{}{}
		SlideInFromSide(client, MemeScene, "teejpopup", settings)
	case "!teej3":
		ToggleSource(client, MemeScene, "teejpopup", true)
		max := 500
		min := -100
		startX := 0.0
		endX := 275.0
		y := float64(rand.Intn(max-min) + min)
		// Should we pass these in like this???
		go func(
			c *OBSClient,
			start float64,
			end float64,
			yValue float64,
		) {
			// This min max is great for side teej
			settings := map[string]interface{}{
				"start_x": start,
				"end_x":   end,
				"y":       yValue,
			}
			SlideSource(c, obsCmd.Scene, "teejpopup", settings)
			<-time.NewTimer(time.Second * 1).C
			rSettings := map[string]interface{}{
				"start_x":  end,
				"end_x":    start,
				"y":        yValue,
				"modifier": -1.0,
			}
			ReverseSlideSource(c, obsCmd.Scene, "teejpopup", rSettings)
		}(client, startX, endX, y)
	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

// ==================== //
// Begin Camera Effects //
// ==================== //
func BeginEffects(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	switch cmd {
	case "!keeb":
		ToggleFilter(client, "primary", "KeyboardKeyboardPosition", true)
		ToggleFilter(client, "primary", "KeyboardBeginPosition", true)
		ToggleFilter(client, "primary", "KeyboardChatPosition", true)
	case "!risebegin":
		go RiseFromTheGrave(client)
	case "!pulse":
		sourceName := "BeginCam"
		ToggleFilter(client, sourceName, "outline", true)
		PulsatingHighlight(client, sourceName, 50, 6, false)
	case "!reaction":
		ReactionZoom(client, "BeginCam")
	case "!chadbegin":
		settings := map[string]interface{}{}
		go ChadSource(client, PrimaryScene, "BeginCam", settings)
	case "!zoombegin2":
		ToggleFilter(client, "BeginCam", ViewerTransformFilter, true)
		ToggleFilter(client, "BeginCam", "outline", true)
		// False means don't turn this source off!
		go ZoomAndSpin2(client, "BeginCam", 10)
		go PulsatingHighlight(client, "BeginCam", 50, 10, false)
	case "!spazzbegin":
		ToggleFilter(client, "BeginCam", ViewerTransformFilter, true)
		ToggleFilter(client, "BeginCam", "spazzbegin", true)
	case "!rollbegin":
		ToggleFilter(client, "BeginCam", ViewerTransformFilter, true)
		ToggleFilter(client, "BeginCam", "rollbegin", true)
	case "!flipbegin":
		ToggleFilter(client, "BeginCam", ViewerTransformFilter, true)
		ToggleFilter(client, "BeginCam", "flipbegin", true)
	case "!bigbrainbegin":
		ToggleFilter(client, "BeginCam", ViewerTransformFilter, true)
		ToggleFilter(client, "BeginCam", "bigbrainbegin", true)
	case "!angrybegin":
		ToggleFilter(client, "BeginCam", "AngryBegin", true)
		ToggleFilter(client, "BeginCam", "BeginMad", true)
	case "!tallbegin":
		ToggleFilter(client, "BeginCam", ViewerTransformFilter, true)
		TallBegin(client, "BeginCam", 30)
	case "!widebegin":
		ToggleFilter(client, "BeginCam", ViewerTransformFilter, true)
		go func() {
			WideBegin(client, "BeginCam", 7)
		}()
	case "!highlight":
		ToggleFilter(client, "BeginCam", "outline", true)
		randomIndex := rand.Intn(len(colors))
		color, _ := strconv.ParseFloat(colors[randomIndex], 32)
		OutlineColor(client, color, "BeginCam")
	case "!color_green":
		color, _ := strconv.ParseFloat(Green, 32)
		OutlineColor(client, color, "BeginCam")
	case "!colorbrown":
		color, err := strconv.ParseFloat(Brown, 32)
		if err != nil {
			fmt.Printf("err = %+v\n", err)
		}
		OutlineColor(client, color, "BeginCam")
	case "!hidebegin":
		go Hide(client)
	case "!intro":
		// ChangeScene(client, "startin")
		// ToggleFilter(client, "keyboard", "outline", true)
		// ToggleSource(client, "startin", "keyboard", true)
		fmt.Println("inside of intro")
		toggleTransform(client, "BeginCam", true)
		// PulsatingHighlight(client, "keyboard", 50, 30, false)
		// Rise2(client, PrimaryScene, "keyboard", map[string]interface{}{})
		// ZoomAndSpin2(client, "keyboard", 8)
	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

func ProcessNormalizeRequests(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	switch cmd {
	case "!normie_transform":
		parts := obsCmd.Parts
		// parts := strings.Split(msg.Message, " ")
		if len(parts) > 1 {
			source := parts[1]
			ToggleFilter(client, source, ViewerTransformFilter, true)
			ToggleFilter(client, source, "normie_transform", true)
			ToggleFilter(client, source, "color_correction", false)
		} else {
			// We need to fix the camera color
			ToggleFilter(client, "BeginCam", ViewerTransformFilter, true)
			ToggleFilter(client, "BeginCam", "normie_transform", true)
			ToggleFilter(client, "BeginCam", "color_correction", false)
		}
	case "!norm_all":
		sources, _ := SourcesByVisibility(client, MemeScene)
		for _, source := range sources {
			// ToggleSource(c, mutex, MemeScene, source, true)
			go func(c1 *OBSClient, s string) {
				ToggleFilter(c1, s, "normie_transform", true)
				ToggleFilter(c1, s, "zoom_out", true)
				<-time.NewTimer(time.Millisecond * 200).C
				ToggleSource(c1, MemeScene, s, false)
			}(client, source)
			// <-time.NewTimer(time.Millisecond * 200).C
		}
	case "!nomeme":
		for _, scene := range MemeScenes {
			sources, _ := SourcesByVisibility(client, scene)
			for _, source := range sources {
				ToggleSource(client, scene, source, false)
			}
		}

	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

func ProcessHelpers(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	parts := obsCmd.Parts
	scene := obsCmd.DefaultMeme.Scene

	switch cmd {

	case "!find", "!reset":
		if len(parts) > 1 {
			source := parts[1]

			meme, err := memes.FindDefault(client.DB, source)
			// This coding feels defensive
			// why not rely on the error?
			if err != nil {
				fmt.Printf("Error Finding Default: %s %+v\n", source, err)

				return effectTriggered, err
			}

			ResetMeme(client, meme)
		}
	case "!idk":
		var name string
		if len(parts) < 2 {
			name = "test1"
		} else {
			name = parts[1]
		}
		ms := []memes.Meme{}
		sources, _ := SourcesByVisibility(client, scene)
		for _, source := range sources {
			meme := MemeInfo(client, scene, source)
			ms = append(ms, meme)
		}
		memeFile, err := os.Create(fmt.Sprintf("assets/ViewerSceneJson/%s.json", name))
		if err != nil {
			fmt.Printf("\nError Creating Built JSON: %s", err)
		}
		encoder := json.NewEncoder(memeFile)
		encoder.Encode(ms)
		memeFile.Close()

		for _, meme := range ms {
			scaleSettings := map[string]interface{}{
				"scale": meme.Scale,
			}
			ScaleSource(client, scene, meme.Name, scaleSettings)
			x, y := RandomPos()
			moveSettings := map[string]interface{}{
				"x": x,
				"y": y,
			}
			MoveSource(client, scene, meme.Name, moveSettings)

			// MoveSource2(c, mutex, scene, meme.Name, moveSettings)
			// ToggleSource(c, mutex, scene, meme.Name, true)
			// ToggleFilter(c, mutex, meme.Name, "color_correction", false)
			// ToggleFilter(c, mutex, meme.Name, ViewerTransformFilter, true)
			// ToggleFilter(c, mutex, meme.Name, "normie_transform", true)
		}
	case "!images":
		images := ImageSources(client)
		results <- images
	case "!filtersettings":
		res, _ := FetchFilterSettings(client, "BeginCam", ViewerTransformFilter)
		results <- fmt.Sprintf("%v", res)
	case "!settings":
		FetchSourceSettings(client, "sealspin", "ffmpeg_source")
	// case "!clone":
	// 	if len(parts) > 1 {
	// 		source := parts[1]
	// 		fmt.Println("IT'S CLONING TIME: ", source)
	// 		CloneThang(client, scene, source, 5)
	// 	}
	case "!addfilters":
		if len(parts) > 1 {
			source := parts[1]
			// AddOutlineFilter(c, mutex, scene, source)
			// AddTransformFilter(c, mutex, scene, source)
			AddNormieTransformFilter(client, scene, source)
			AddColorCorrectionFilter(client, scene, source)
			AddColorFadeFilter(client, scene, source)
			AddOutlineFilter(client, scene, source)
			AddTransformFilter(client, scene, source)
		}
	case "!fixnorm":
		if len(parts) > 1 {
			source := parts[1]
			UpdateNormieTransform(client, scene, source)
		}
	case "!memes":
		fmt.Println("LOOKING ")
		// I could iterate through every meme
		// and call stats on it,
		// and save those stats to a file
		// I then could create, just call
		// each command on its own
		sources, _ := SourcesByVisibility(client, scene)
		results <- fmt.Sprintf("Memes: %s", strings.Join(sources, ", "))
		results <- "https://beginworld.website-us-east-1.linodeobjects.com/memes.html"
		website_generator.SyncMemesPage()
	case "!hiddenmemes":
		results <- "http://www.beginworld.exchange/memes.html"
		website_generator.SyncPage("memes")
	// TODO: this needs to iterate through the meme scenes as welll
	case "!stats":
		if len(parts) > 1 {
			source := parts[1]
			CreateFakeSource(client)
			meme := MemeInfo(client, MemeScene, source)
			results <- fmt.Sprintf("Source: %s | X: %f Y: %f", source, meme.X, meme.Y)
		}
	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

func ProcessEpicEffects(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	// !clippy Begin use an interface
	// That would result in Clippy rising
	// with a message saying:
	//   "Hey have you thought of: Begin us an interface"
	switch cmd {
	case "!test":
		sources := FetchSources(client)

		for _, source := range sources {
			fmt.Printf("source = %+v\n", source)
			sourceName := source["name"]
			typeKind, _ := source["typeId"]
			name := sourceName.(string)
			kind := typeKind.(string)

			settings := FetchSceneItemSettings(client, MemeScene, name, kind)
			// settings.BoundsAlignment
			fmt.Printf("PositionAlignment = %+v | ", settings.PositionAlignment)
		}

	// How do I find
	// The current Scenek
	//
	case "!communitysunny":

		currentScene := GetCurrentScene(client)
		// How do we know support has reached?
		// What kind of special message?
		if obsCmd.Message.Streamgod {
			msg := strings.Join(obsCmd.Parts[1:], " ")

			ToggleSource(client, "iasip", "SunnyHtml", false)
			website_generator.CreateSunnyPage(client.DB, msg)
			ToggleSource(client, "iasip", "SunnyHtml", true)
			ChangeScene(client, "iasip")
			go func() {
				<-time.NewTimer(5 * time.Second).C

				ChangeScene(client, currentScene)
			}()
		}

	case "!scene":
		currentScene := GetCurrentScene(client)
		fmt.Printf("c = %+v\n", currentScene)
	case "!iasip", "!sunny":
		currentScene := GetCurrentScene(client)
		if obsCmd.Message.Streamgod {
			msg := strings.Join(obsCmd.Parts[1:], " ")
			ToggleSource(client, "iasip", "SunnyHtml", false)
			website_generator.CreateSunnyPage(client.DB, msg)
			ToggleSource(client, "iasip", "SunnyHtml", true)
			ChangeScene(client, "iasip")
			go func() {
				<-time.NewTimer(5 * time.Second).C
				ChangeScene(client, currentScene)
			}()
		}

	case "!clippy":
		ToggleSource(client, MemeScene, "clippy", true)
		ToggleSource(client, MemeScene, "clippytextboxhd", true)
		clippyRiseSettings := map[string]interface{}{
			"start_y": -400.0,
			"end_y":   425.0,
			"x":       700.0,
		}
		clippyTextRiseSettings := map[string]interface{}{
			"start_y": -400.0,
			"end_y":   300.0,
			"x":       575.0,
		}
		RiseSource(client, MemeScene, "clippy", clippyRiseSettings)
		RiseSource(client, MemeScene, "clippytextboxhd", clippyTextRiseSettings)
		ToggleSource(client, MemeScene, "ClippyAdvice", true)
	case "!demo":
		// if msg.Streamgod {
		_, sources := SourcesByVisibility(client, MemeScene)
		for _, _ = range sources {
			// ToggleSource(c, mutex, MemeScene, source, true)
			// f := randomTransformFunc()
			// go f(c, mutex, MemeScene, source)
			// <-time.NewTimer(time.Millisecond * 200).C
		}
		// }
	case "!credits":
		// if msg.Streamgod {
		_, sources := SourcesByVisibility(client, MemeScene)
		for _, source := range sources {
			ToggleSource(client, MemeScene, source, true)
			settings := map[string]interface{}{
				"rotation": 0.0,
			}
			RotateSource(client, MemeScene, source, settings)
			SetSourcePosition(client, MemeScene, source, 0.0, 100.0)
			ToggleFilter(client, source, "normie_transform", true)
			slideSettings := map[string]interface{}{
				"start_x": 0.0,
				"end_x":   1450.0,
				"y":       100.0,
			}
			SlideSource(client, MemeScene, source, slideSettings)
			ToggleSource(client, MemeScene, source, false)
		}
		// }
	case "!allmeme":
		// if msg.Streamgod {
		_, sources := SourcesByVisibility(client, MemeScene)
		for _, source := range sources {
			ToggleSource(client, MemeScene, source, true)
		}
		// }
	case "!danceparty":
		sources := []string{"vibecat", "puppy", "technofroggo", "sealspin"}
		for _, source := range sources {
			// Make em wacky!
			ToggleFilter(client, source, ViewerTransformFilter, true)
			FunZoom(client, source)
			// Turn it on
			ToggleSource(client, MemeScene, source, true)
			// Make it normal
			ToggleFilter(client, source, "normie_transform", true)
		}
		SetSourcePosition(client, MemeScene, "technofroggo", 1275.0, -1.0)
		SetSourcePosition(client, MemeScene, "puppy", 0.0, -5.0)
		SetSourcePosition(client, MemeScene, "vibecat", -12.0, 415)
		SetSourcePosition(client, MemeScene, "sealspin", 850, 300)
	case "!dealwithitboys":
		// Source: dealwithit | X: 943.000000 Y: 433.194702
		// Source: blunt | X: 905.000000 Y: 529.000000
		scene := MemeScene
		for _, source := range []string{"dealwithit", "blunt"} {
			FunZoom(client, source)
			ToggleSource(client, scene, source, true)
			ToggleFilter(client, source, ViewerTransformFilter, true)
			ToggleFilter(client, source, "normie_transform", true)
		}
	case "!dealwithit":
		scene := MemeScene
		// sunglassess := MemeInfo(c, mutex, "dealwithit")
		// blunt := MemeInfo(c, mutex, "blunt")
		for _, source := range []string{"dealwithit", "blunt"} {
			ToggleSource(client, scene, source, true)
		}
		// Source: dealwithit | X: 857.000000 Y: 432.000000
		//  Source: blunt | X: 842.000000 Y: 521.000000
		modifier := 1.5
		go Fall(
			client,
			scene,
			"dealwithit",
			875.0,
			0,
			432.0,
			modifier,
		)
		// <-time.NewTimer(time.Millisecond * 200).C
		go Slide(client, scene, "blunt", 300, 842.0, 521.0, modifier)
	case "!pulseseal":
		ToggleSource(client, MemeScene, "sealspin", true)
		ToggleFilter(client, "sealspin", ViewerTransformFilter, true)
		ToggleFilter(client, "sealspin", "outline", true)
		PulsatingHighlight(client, "sealspin", 50, 7, true)
		ZoomAndSpin(client, "sealspin", 5)
		// player.UpdateMana(db, *msg.PlayerID, uint(mana-10))

	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

func ProcessCreateSourcesRequests(
	client *OBSClient,
	db *gorm.DB,
	cmd string,
	obsCmd Command,
	results <-chan string,
) (bool, error) {

	effectTriggered := true
	var err error

	msgBreakdown := obsCmd.Parts
	switch cmd {
	case "!defaults":
		m := []memes.Meme{}
		tx := db.Model(&memes.Meme{}).Find(&m)
		if tx.Error != nil {
			fmt.Printf("tx.Error = %+v\n", tx.Error)
		}

		// settings := map[string]interface{}{}
		for _, meme := range m {
			settings := map[string]interface{}{
				"x":       meme.X,
				"start_x": 0.0,
				// "start_x":  meme.X,
				"end_x":    meme.X,
				"y":        meme.Y,
				"start_y":  meme.Y,
				"end_y":    meme.Y,
				"rotation": meme.Rotation,
				"scale":    meme.Scale,
			}
			// if meme.X < 400.0 {
			// 	settings["start_x"] = 1200.0
			// } else {
			// 	settings["start_x"] = 0.0
			// }
			SetSourcePosition(client, MemeScene, meme.Name, 0.0, 0.0)
			SlideSource(client, MemeScene, meme.Name, settings)
		}

	case "!update_gif", "!update_video":
		sourceKind := "ffmpeg_source"
		memeFolder := "/home/begin/stream/Stream/ViewerVideos"
		f := fmt.Sprintf("%s/%s%s", memeFolder, obsCmd.Source, ".gif")
		CreateMemes(client, obsCmd.Source, sourceKind, f)
	case "!update_image":
		memeFolder := "/home/begin/stream/Stream/ViewerImages"
		f := fmt.Sprintf("%s/%s%s", memeFolder, obsCmd.Source, ".png")
		sourceKind := "image"
		CreateMemes(client, obsCmd.Source, sourceKind, f)
	case "!save":
		var name string
		if len(obsCmd.Parts) > 1 {
			name = obsCmd.Parts[1]
		}

		fmt.Printf("\n\tAttempting to Save: %+v | Parts: %v\n", name, obsCmd.Parts)

		if name != "" {
			meme := MemeInfo(client, obsCmd.Scene, name)

			fmt.Printf("\t+++ Meme: %+v", meme)

			err := meme.Save(db)
			if err == nil {
				fmt.Println(fmt.Sprintf("Saving Position of: %s", name))
			} else {
				fmt.Println(fmt.Sprintf("Error Saving Position of: %v", err))
			}
		}
	case "!create_image_source":
		obsCmd.Scene = MemeLimbo
		if len(msgBreakdown) > 1 {
			// imageName := strings.ToLower(msgBreakdown[1])
			imageName := msgBreakdown[1]
			fmt.Printf("\t -- We are going to try and Create a new image: %s", imageName)
			CreateAndPrepareImageSource(client, db, obsCmd.Scene, imageName)
			settings := map[string]interface{}{
				"scale": 0.5,
			}
			ScaleSource(client, obsCmd.Scene, imageName, settings)

			slideSettings := map[string]interface{}{
				"start_x": 0.0,
				"end_x":   1365.0,
				"y":       300.0,
			}
			SlideSource(client, obsCmd.Scene, imageName, slideSettings)
		}
	case "!create_video_source":
		obsCmd.Scene = MemeLimbo

		if len(msgBreakdown) > 2 {
			obsCmd.Scene = msgBreakdown[2]
		}

		if len(msgBreakdown) > 0 {
			videoName := msgBreakdown[1]
			CreateAndPrepareVideoSource(client, db, obsCmd.Scene, videoName)

			// settings := map[string]interface{}{
			// 	"scale": 0.5,
			// }
			// ScaleSource(client, obsCmd.Scene, videoName, settings)
			// slideSettings := map[string]interface{}{
			// 	"start_x": 0.0,
			// 	"end_x":   1365.0,
			// 	"y":       300.0,
			// }
			// SlideSource(client, obsCmd.Scene, videoName, slideSettings)
		}

	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

func RandomRouter(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {
	switch cmd {

	case "!zoom_out":
		// This probably needs  error handling?
		settings := map[string]interface{}{}
		ToggleFilter(client, obsCmd.Source, "zoom_out", true)
		BigBrain(client, obsCmd.Scene, obsCmd.Source, settings)
	case "!zs":
		if len(obsCmd.Parts) > 1 {
			ZoomSpinSource(
				client,
				obsCmd.Scene,
				obsCmd.Source,
				obsCmd.Settings,
			)
		}

	case "!random":
		if obsCmd.Scene != "" && obsCmd.Source != "" {
			// obsCmd.Settings is uninitialized???
			f := randomTransformFunc()
			f(
				client,
				obsCmd.Scene,
				obsCmd.Source,
				obsCmd.Settings,
			)
		}

	// ==================== //
	// Toggle and Transform //
	// ==================== //
	case "!alerts":
		sourceName := "Alerts"
		ToggleFilter(client, sourceName, ViewerTransformFilter, true)
		ZoomAndSpin(client, sourceName, 5)
	// Theory: We aren't getting
	// effects lower in this list
	case "!beginworld":
		ToggleSource(client, MemeScene, "beginworld", true)
		toggleTransform(client, "beginworld", true)
		ZoomAndSpin(client, "beginworld", 8)
	case "!seal":
		ToggleSource(client, MemeScene, "sealspin", true)
	case "!puppy":
		ToggleSource(client, MemeScene, "puppy", true)
		var duration time.Duration
		duration = 30
		DelaySpin(client, MemeScene, "puppy", 2, duration)
	case "!zoomseal":
		zoomseal(client)
	case "!otherspin":
		ToggleFilter(client, "BeginCam", ViewerTransformFilter, true)
		ZoomAndSpin(client, "BeginCam", 7)
	case "!spinseal":
		ToggleSource(client, MemeScene, "sealspin", true)
		ToggleFilter(client, "sealspin", ViewerTransformFilter, true)
		ZoomAndSpin(client, "BeginCam", 5)
	case "!beginboy":
		beginboy(client)

	// ============= //
	// Toggle Scenes //
	// ============= //
	case "!hateflow":
		ChangeScene(client, "emperor")

	case "!cube_news":
		go func() {
			ChangeScene(client, "breakin")
			<-time.NewTimer(time.Second * 6).C
			ChangeScene(client, "news")
		}()

	}

	// TODO: Update passing to charge
	return false, nil
}
