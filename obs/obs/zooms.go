package obs

import (
	"fmt"
	"time"
)

func ZoomAndSpin3(
	client *OBSClient,
	sourceName string,
	delay time.Duration,
) {

	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * delay)

	go func() {
		x := 0.0
		y := 0.0
		z := 0.0
		xModifier := -0.5
		yModifier := 0.8
		zModifier := -0.4

		for {
			select {
			case <-timer.C:
				return
			case t := <-ticker.C:
				fmt.Println("t: ", t)

				// Cool Weird slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    y,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": y,
					"Filter.Transform.Rotation.Z": z,
				}
				SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
				x = x + xModifier
				y = y + yModifier
				z = z + zModifier
			}
		}
	}()
}

func ZoomAndSpin2(client *OBSClient, sourceName string, delay time.Duration) {
	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * delay)

	go func() {
		x := 0.0
		y := 0.0
		z := 0.0
		modifier := 0.7
		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				// Cool Weird slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    y,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": y,
					"Filter.Transform.Rotation.Z": z,
				}

				SetSourceFilterSettings(
					client,
					sourceName,
					filterName,
					filterSettings,
				)
				x = x + modifier
				y = y - modifier
				z = z + (modifier + 0.3)
			}
		}
	}()
}

func ZoomAndSpin(client *OBSClient, sourceName string, delay time.Duration) {
	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * delay)

	go func() {
		x := 0
		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				// Cool Wierd slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    x,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": x,
					"Filter.Transform.Rotation.Z": x,
				}

				SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
				x++
			}
		}
	}()

}

// ZoomSpinSource takes a source and zooms and spins it
// 	until the duration
func ZoomSpinSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {

	filterName := ViewerTransformFilter
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * 5)

	x := 0
	for {
		select {
		case <-timer.C:
			break
			// return nil
		case _ = <-ticker.C:
			filterSettings := map[string]interface{}{
				"Filter.Transform.Scale.X":    x,
				"Filter.Transform.Scale.Y":    x,
				"Filter.Transform.Rotation.X": x,
				"Filter.Transform.Rotation.Y": x,
				"Filter.Transform.Rotation.Z": x,
			}
			SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
			x++
		}
	}

	return nil
}
