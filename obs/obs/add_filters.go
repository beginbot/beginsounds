package obs

import (
	"strconv"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/obs/obs/filters"
)

// =========== //
// FilterNames //
// =========== //

// We should comment what each of these are for

var GreenscreenFilter = "greenscreen"

var ColorCorrectionFilter = "color_correction"

var ViewerTransformFilter = "viewer_transform"

var ViewerNormieFilter = "viewer_normie_move"

var ViewerMoveFilter = "viewer_move"

var NormieTransformFilter = "normie_transform"

var OutlineFilter = "outline"

var ColorFadeFilter = "colorfade"

// ============ //
// Filter Types //
// ============ //

var StreamFXFilterType = "streamfx-filter-sdf-effects"

var MoveValueFilterType = "move_value_filter"

var MoveSourceFilterType = "move_source_filter"

var ColorCorrectionFilterType = "color_correction"

var GreenscreenFilterType = "chroma_key_filter"

// ===============================================================

// FilterSetting - Represents the values needed to Create of Set Filter Settings
type FilterSetting struct {
	Name       string
	FilterType string
	Settings   map[string]interface{}
}

// AddColorFadeFilter - Adds filter fade from one color to another from the color_correction
//
// TODO: we might want to extract out settings for color_correction
func AddColorFadeFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	// This is ColorCorrectFilter
	settings := FilterSetting{
		Name:       ColorFadeFilter,
		FilterType: MoveValueFilterType,
		Settings:   filters.MoveColorFilterSettings,
	}

	return addFilter(client, sceneName, sourceName, settings)
}

// AddColorCorrectionFilter - Add an OBS color correction filter
func AddColorCorrectionFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	settings := FilterSetting{
		Name:       ColorCorrectionFilter,
		FilterType: ColorCorrectionFilterType,
		Settings:   filters.ColorCorrectionSettings,
	}

	return addFilter(client, sceneName, sourceName, settings)
}

// AddViewerMoveFilter - Add the filter to return all
func AddViewerNormieMoveFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	filterSettings := filters.DefaultTransformFilter
	filterSettings["filter"] = ViewerTransformFilter

	settings := FilterSetting{
		Name:       ViewerNormieFilter,
		FilterType: MoveValueFilterType,
		Settings:   filterSettings,
	}

	return addFilter(client, sceneName, sourceName, settings)
}

// AddViewerMoveFilter - Add the filter to return all
func AddViewerMoveFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	filterSettings := filters.DefaultTransformFilter
	filterSettings["filter"] = ViewerTransformFilter

	settings := FilterSetting{
		Name:       ViewerMoveFilter,
		FilterType: MoveValueFilterType,
		Settings:   filterSettings,
	}

	return addFilter(client, sceneName, sourceName, settings)
}

// AddNormieTransformFilter - Add the filter to return all
func AddNormieTransformFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	filterSettings := filters.DefaultTransformFilter
	filterSettings["filter"] = ViewerTransformFilter

	settings := FilterSetting{
		Name:       NormieTransformFilter,
		FilterType: MoveValueFilterType,
		Settings:   filterSettings,
	}

	return addFilter(client, sceneName, sourceName, settings)
}

// AddGreenScreenFilter - Adds a Chroma Key Source in OBS
func AddGreenScreenFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {
	settings := FilterSetting{
		Name:       GreenscreenFilter,
		FilterType: GreenscreenFilterType,
		Settings: map[string]interface{}{
			"filter": GreenscreenFilter,
		},
	}

	return addFilter(client, sceneName, sourceName, settings)
}

// AddMoveSourceFilter adds a filter to a source
func AddMoveSourceFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
	filterName string,
) error {

	filterSettings := filters.DefaultTransformFilter
	filterSettings["filter"] = ViewerTransformFilter
	filterSettings["source"] = sourceName

	settings := FilterSetting{
		Name:       filterName,
		FilterType: MoveSourceFilterType,
		Settings:   filterSettings,
	}

	return addFilter(client, sceneName, sceneName, settings)
}

// AddOutlineFilter adds a filter to a source
func AddOutlineFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	color, _ := strconv.ParseFloat(Purple, 64)
	settings := FilterSetting{
		Name:       OutlineFilter,
		FilterType: StreamFXFilterType,
		Settings: map[string]interface{}{
			"Filter.SDFEffects.Outline.Color": color,
		},
	}

	return addFilter(client, sceneName, sourceName, settings)
}

// ======================================================================================================

func addFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings FilterSetting,
) error {
	req := rawAddFilter(sceneName, sourceName, settings)
	_, err := client.execOBSCommand(&req)
	return err
}

func rawAddFilter(
	sceneName string,
	sourceName string,
	settings FilterSetting,
) obsws.AddFilterToSourceRequest {
	return obsws.NewAddFilterToSourceRequest(
		sourceName,
		settings.Name,
		settings.FilterType,
		settings.Settings,
	)
}
