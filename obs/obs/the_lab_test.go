package obs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

func NoTestPokemonDance(t *testing.T) {
	client := TestingClient()
	ms, _ := memes.FromScene(client.DB, PokemonScene)
	for _, meme := range ms {
		ToggleSource(client, meme.Scene, meme.Name, true)
		settings := map[string]interface{}{
			"scale": 0.4,
		}
		ScaleSource(client, meme.Scene, meme.Name, settings)
	}

	RandomEffectOnMemes(client, ms, true)
}

func NoTestTransformMeme(t *testing.T) {
	client := TestingClient()
	defer client.Client.Disconnect()
	desiredSettings := []TransformSetting{
		{
			Name:   "Filter.Transform.Rotation.Y",
			Value:  360,
			Range:  3600,
			Random: true,
		},
	}

	meme, _ := memes.FindDefault(client.DB, "quiGonTogglebit")

	fmt.Printf("Wat = %+v\n", meme)

	allReqs := []map[string]interface{}{}
	settings := generateTransformFilterSettingsFromList(desiredSettings)
	reqs := UpdateTransformFilterAndCall(client, meme, settings)
	allReqs = append(allReqs, reqs...)
	BatchRequest(client, allReqs)
}

func NoTestTransformMemes(t *testing.T) {
	client := TestingClient()
	fmt.Println("whyyy")
	defer client.Client.Disconnect()
	desiredSettings := []TransformSetting{
		{
			Name:   "Filter.Transform.Rotation.Y",
			Value:  180,
			Range:  3600,
			Random: true,
		},
		// {
		// 	Name:   "Filter.Transform.Rotation.X",
		// 	Value:  0,
		// 	Range:  10000,
		// 	Random: true,
		// },
		// {
		// 	Name:   "Filter.Transform.Camera.FieldOfView",
		// 	Value:  0,
		// 	Range:  1000,
		// 	Random: true,
		// },
	}
	CallEffectOnVisibleMemes(client, UpdateTransformFilterAndCall, desiredSettings)
}

func NoTestLoopWithBatch(t *testing.T) {
	client := TestingClient()
	defer client.Client.Disconnect()

	duration := 50
	// source := "bart"
	// filtername := fmt.Sprintf("MoveSource%s", strings.Title(source))

	var results map[string]interface{}
	filename := fmt.Sprintf("move_default.json")
	filepath := fmt.Sprintf("../../tmp/%s", filename)

	f, err := ioutil.ReadFile(filepath)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(f, &results); err != nil {
		panic(err)
	}

	ms, _ := memes.Visible(client.DB)
	moveSource := MapToMoveFilter(results)
	moveSource.EasingFunctionMatch = 2
	moveSource.Duration = duration

	moveSource.Pos.X = 000
	moveSource.Pos.Y = 450
	for {
		for _, m := range ms {
			if m.Name == "primetime" || m.Name == "melkeycam" {
				continue
			}
			rand.Seed(time.Now().UnixNano())
			settings := moveSource
			settings.Source = m.Name
			filtername := fmt.Sprintf("MoveSource%s", strings.Title(settings.Source))
			moveSource.Rot = float64(rand.Intn(360))
			settings.Scale.X = m.Scale
			settings.Scale.Y = m.Scale
			settings.Pos.X = settings.Pos.X + float64(rand.Intn(300))
			settings.Pos.Y = settings.Pos.Y + float64(rand.Intn(300))
			moveSource.EasingFunctionMatch = rand.Intn(10)

			newSettings := settings.ToMap()

			SetSourceFilterSettings(client, MemeScene, filtername, newSettings)
			ToggleFilter(client, MemeScene, filtername, true)
		}
	}
}

func NoTestLoop(t *testing.T) {
	client := TestingClient()
	defer client.Client.Disconnect()
	ToggleBitLightSaberBattle(client)
}

func NoTestJSON2Filter(t *testing.T) {
	client := TestingClient()
	defer client.Client.Disconnect()

	source := "dkdance"
	filtername := fmt.Sprintf("MoveSource%s", strings.Title(source))

	var results map[string]interface{}
	filename := fmt.Sprintf("move_default.json")
	filepath := fmt.Sprintf("../../tmp/%s", filename)

	f, err := ioutil.ReadFile(filepath)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(f, &results); err != nil {
		panic(err)
	}
	fmt.Printf("results: %+v\n", results)

	moveSource := MapToMoveFilter(results)

	moveSource.Source = source
	// moveSource.EasingFunctionMatch = 0
	moveSource.EasingFunctionMatch = rand.Intn(10)
	moveSource.Duration = 500
	moveSource.Pos.X = moveSource.Pos.X + 245
	moveSource.Pos.Y = moveSource.Pos.Y + 100

	// no tt right now
	newSettings := moveSource.ToMap()

	fmt.Println("Sending Requests")
	SetSourceFilterSettings(client, MemeScene, filtername, newSettings)
	ToggleFilter(client, MemeScene, filtername, true)
}

func NoTestSaveJSON(t *testing.T) {
	client := TestingClient()
	defer client.Client.Disconnect()
	source := "gopher"
	filename := fmt.Sprintf("%s.json", source)
	filtername := fmt.Sprintf("MoveSource%s", strings.Title(source))

	// This is directly from OBS
	rawSettings, _ := FetchFilterSettings(client, MemeScene, filtername)

	fmt.Printf("\t --- rawSettings = %+v\n", rawSettings)
	var settings MoveSourceSettings
	err := mapstructure.Decode(rawSettings, &settings)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}
	settings.Pos.Y = settings.Pos.Y - 400
	settings.Duration = 3000
	settings.EasingFunctionMatch = 9

	tt := TransformTextSettings{
		Pos: Coordinates{
			// X: settings.Pos.X - 100,
			// Y: settings.Pos.Y - 100,
			X: settings.Pos.X,
			Y: settings.Pos.Y - 400,
		},
		Rot: settings.Rot,
		Scale: Coordinates{
			X: settings.Scale.X,
			Y: settings.Scale.Y,
		},
		Bounds: Coordinates{
			X: settings.Bounds.X,
			Y: settings.Bounds.Y,
		},
		Crop: CropValues{
			L: 0,
			R: 0,
			B: 0,
			T: 0,
		},
	}
	fmt.Printf("tt = %+v\n", tt)

	settings.TransformText = tt
	newSettings := settings.ToMap()
	fmt.Printf("newSettings = %+v\n", newSettings)
	SetSourceFilterSettings(client, MemeScene, filtername, newSettings)

	file, err := os.OpenFile(
		fmt.Sprintf("../../tmp/%s", filename),
		os.O_CREATE|os.O_APPEND|os.O_WRONLY,
		0645,
	)

	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
	}
	encoder := json.NewEncoder(file)
	jsonErr := encoder.Encode(settings)
	if jsonErr != nil {
		fmt.Printf("Error saving to file: %s %s\n", filename, jsonErr)
		return
	}

	ToggleFilter(client, MemeScene, filtername, true)
}

func NoTestAddHotKeyA(t *testing.T) {
	client := TestingClient()
	defer client.Client.Disconnect()

	ms, _ := memes.Visible(client.DB)
	for _, meme := range ms {
		AddMoveSourceFilter(
			client,
			MemeScene,
			meme.Name,
			fmt.Sprintf("MoveSourceHotKeyA%s", strings.Title(meme.Name)),
		)
	}
	// meme, _ := memes.FindDefault(client.DB, "technofroggo")
}
