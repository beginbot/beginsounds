package obs

import (
	"fmt"
	"strings"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

func CloneFilter(
	client *OBSClient,
	scene string,
	source string,
	sourceFilter string,
	destFilter string,
) {
	rawSettings, _ := FetchFilterSettings(client, MemeScene, sourceFilter)

	var settings MoveSourceSettings
	err := mapstructure.Decode(rawSettings, &settings)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	settings.Source = source

	tt := TransformTextSettings{
		Pos: Coordinates{
			X: settings.Pos.X,
			Y: settings.Pos.Y,
		},
		Rot: settings.Rot,
		Scale: Coordinates{
			X: settings.Scale.X,
			Y: settings.Scale.Y,
		},
		Bounds: Coordinates{
			X: settings.Bounds.X,
			Y: settings.Bounds.Y,
		},
		Crop: CropValues{
			L: settings.CropLeft,
			R: settings.CropRight,
			B: settings.CropBottom,
			T: settings.CropTop,
		},
	}

	settings.TransformText = tt
	newSettings := settings.ToMap()

	fmt.Printf("tt = %+v\n", tt)
	fmt.Printf("newSettings = %+v\n", newSettings)

	SetSourceFilterSettings(client, MemeScene, destFilter, newSettings)
}

func NewMoveSourceRelative(
	client *OBSClient,
	scene string,
	source string,
	xOffset float64,
	yOffset float64,
) {
	filtername := fmt.Sprintf("MoveSource%s", strings.Title(source))

	// This is directly from OBS
	// this is assumes, the last move, is were the X,Y and coordinates
	// we should really be also checking the DB
	meme, err := memes.FindCurrent(client.DB, source)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
		return
	}

	rawSettings, _ := FetchFilterSettings(client, scene, filtername)

	var settings MoveSourceSettings
	err = mapstructure.Decode(rawSettings, &settings)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	fmt.Printf("meme = %+v\n", meme)
	// if meme.X != settings.Pos.X || meme.Y != settings.Pos.Y {
	// 	fmt.Println("Filter and DB don't match X and Y coordinates")
	// 	return
	// }

	newX := settings.Pos.X - xOffset
	newY := settings.Pos.Y - yOffset

	// settings.Pos.Y = settings.Pos.Y - 400
	settings.Pos.Y = newY
	settings.Pos.X = newX
	settings.Rot = 0.0

	scaleX := 1.0
	scaleY := 1.0
	settings.Scale.X = scaleX
	settings.Scale.Y = scaleY

	settings.Duration = 1000
	// settings.EasingFunctionMatch = 9

	tt := TransformTextSettings{
		Pos: Coordinates{
			X: newX,
			Y: newY,
		},
		Rot: settings.Rot,
		Scale: Coordinates{
			X: scaleX,
			Y: scaleY,
		},
		Bounds: Coordinates{
			X: settings.Bounds.X,
			Y: settings.Bounds.Y,
		},
		Crop: CropValues{
			L: settings.CropLeft,
			R: settings.CropRight,
			B: settings.CropBottom,
			T: settings.CropTop,
		},
	}

	settings.TransformText = tt
	newSettings := settings.ToMap()

	fmt.Printf("tt = %+v\n", tt)
	fmt.Printf("newSettings = %+v\n", newSettings)

	SetSourceFilterSettings(client, scene, filtername, newSettings)
	ToggleFilter(client, scene, filtername, true)
}

func NewMoveSource(
	client *OBSClient,
	scene string,
	source string,
	x float64,
	y float64,
	scale float64,
) {
	filtername := fmt.Sprintf("MoveSource%s", strings.Title(source))

	// This is directly from OBS
	// this is assumes, the last move, is were the X,Y and coordinates
	// we should really be also checking the DB
	meme, err := memes.FindCurrent(client.DB, source)
	if err != nil {
		fmt.Printf("Error Finding Current Meme: %+v\n", err)

		// we should make a current if theres none
		meme, _ = memes.FindDefault(client.DB, source)
	}

	rawSettings, err := FetchFilterSettings(client, scene, filtername)
	if err != nil {
		fmt.Printf(
			"Error Fetching Filter Settings in Scene %s | Filter: %s | Error: %+v\n",
			scene,
			filtername,
			err,
		)
	}

	var settings MoveSourceSettings
	err = mapstructure.Decode(rawSettings, &settings)
	if err != nil {
		fmt.Printf("Error Decoding Move Source Settings: %+v\n", err)
	}

	fmt.Printf("meme = %+v\n", meme)
	// if meme.X != settings.Pos.X || meme.Y != settings.Pos.Y {
	// 	fmt.Println("Filter and DB don't match X and Y coordinates")
	// 	return
	// }

	newX := x
	newY := y

	// settings.Pos.Y = settings.Pos.Y - 400
	settings.Pos.Y = newY
	settings.Pos.X = newX
	settings.Rot = meme.Rotation

	scaleX := scale
	scaleY := scale
	settings.Scale.X = scaleX
	settings.Scale.Y = scaleY

	settings.Duration = 3000

	// We should allow passing this in
	settings.EasingFunctionMatch = 3

	tt := TransformTextSettings{
		Pos: Coordinates{
			X: newX,
			Y: newY,
		},
		Rot: settings.Rot,
		Scale: Coordinates{
			X: scaleX,
			Y: scaleY,
		},
		Bounds: Coordinates{
			X: settings.Bounds.X,
			Y: settings.Bounds.Y,
		},
		Crop: CropValues{
			L: settings.CropLeft,
			R: settings.CropRight,
			B: settings.CropBottom,
			T: settings.CropTop,
		},
	}

	settings.TransformText = tt
	newSettings := settings.ToMap()

	fmt.Printf("tt = %+v\n", tt)
	fmt.Printf("newSettings = %+v\n", newSettings)

	SetSourceFilterSettings(client, scene, filtername, newSettings)
	ToggleFilter(client, scene, filtername, true)
}
