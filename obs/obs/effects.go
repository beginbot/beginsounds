package obs

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

func WinningPokemon(client *OBSClient, pokemon string) {
	filterName := fmt.Sprintf("MoveSource%s", strings.Title(pokemon))
	settings := ScaleToCenterSettings(client, PokemonScene)
	settings.Source = pokemon

	newSettings := settings.ToMap()
	SetSourceFilterSettings(client, PokemonScene, filterName, newSettings)

	ToggleFilter(client, PokemonScene, filterName, true)
}

func ResetMeme(client *OBSClient, meme *memes.Meme) {
	settings := map[string]interface{}{
		"scale": meme.Scale,
	}
	SetSourcePosition(client, meme.Scene, meme.Name, meme.X, meme.Y)
	ScaleSource(client, meme.Scene, meme.Name, settings)

	ToggleSource(client, meme.Scene, meme.Name, true)
	ToggleFilter(client, meme.Name, ViewerTransformFilter, true)

	// ToggleFilter(client, meme.Name, "normie_transform", true)
	ToggleFilter(client, meme.Name, "viewer_normie_move", true)
	// Do we have this on every filter
	ToggleFilter(client, meme.Name, "color_correction", false)
}

// ======= //
// Sliding //
// ======= //

// SlideSource - Slides the source across the screen
// TODO: this should use the other filter
func SlideSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {

	ToggleSource(client, scene, source, false)
	ScaleSource(client, scene, source, settings)

	rawStartX, _ := settings["start_x"]
	rawEndX, _ := settings["end_x"]
	rawY, _ := settings["y"]

	var rawModifier interface{}
	rawModifier, ok := settings["modifier"]
	if !ok {
		rawModifier = 0.8
	}

	modifier := rawModifier.(float64)
	startX := rawStartX.(float64)
	endX := rawEndX.(float64)
	y := rawY.(float64)

	moveSettings := map[string]interface{}{
		"x": startX,
		"y": y,
	}

	MoveSource(client, scene, source, moveSettings)
	RotateSource(client, scene, source, settings)
	// ToggleFilter(client, source, "color_correction", false)
	ToggleFilter(client, source, ViewerTransformFilter, true)
	ToggleFilter(client, source, "normie_transform", true)
	ToggleSource(client, scene, source, true)

	fmt.Printf("\tSliding Source: %s source, settings = %+v\n", source, settings)

	if startX < endX {
		for x := startX; x < endX; x += modifier {
			SetSourcePosition(client, scene, source, x, y)
		}
	} else {
		for x := startX; x > endX; x -= modifier {
			SetSourcePosition(client, scene, source, x, y)
		}
	}

	return nil
}

func SlideOutSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {

	rawStartX, _ := settings["start_x"]
	rawEndX, _ := settings["end_x"]
	rawY, _ := settings["y"]

	var rawModifier interface{}
	rawModifier, ok := settings["modifier"]
	if !ok {
		rawModifier = 0.8
	}
	modifier := rawModifier.(float64)

	startX := rawStartX.(float64)
	endX := rawEndX.(float64)
	y := rawY.(float64)

	for x := startX; x > endX; x -= modifier {
		SetSourcePosition(client, sceneName, sourceName, x, y)
	}

	return nil
}

func Slide(
	client *OBSClient,
	sceneName string,
	sourceName string,
	startX float64,
	endX float64,
	y float64,
	modifier float64,
) {
	for x := startX; x < endX; x += modifier {
		SetSourcePosition(client, sceneName, sourceName, x, y)
	}
}

// SlideInFromSide peaks out a source from the side
func SlideInFromSide(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	rand.Seed(time.Now().UnixNano())

	ToggleSource(client, sceneName, sourceName, true)
	settings["rotation"] = 90.0
	RotateSource(client, sceneName, sourceName, settings)

	startX := 0.0
	endX := 1275.0
	y := 400.0
	settings = map[string]interface{}{
		"start_x": startX,
		"end_x":   endX,
		"y":       y,
	}
	SlideSource(client, sceneName, sourceName, settings)
	<-time.NewTimer(time.Second * 1).C
	reverseSettings := map[string]interface{}{
		"start_x":  endX,
		"end_x":    startX,
		"y":        y,
		"modifier": -1.0,
	}
	ReverseSlideSource(client, sceneName, sourceName, reverseSettings)
	return nil
}

// Color =============================================

func FadeToColor(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	color, ok := settings["color"].(float64)
	if ok {
		SetColorFade(client, sourceName, "colorfade", color)
		ToggleFilter(client, sourceName, "colorfade", true)
		ToggleFilter(client, sourceName, "color_correction", true)
	}
	return nil
}

// ===============================================================================

func floatSourceThroughScene(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	ToggleSource(client, sourceName, sceneName, true)
	RotateSource(client, sceneName, sourceName, settings)

	rand.Seed(time.Now().UnixNano())
	max := 1400
	min := 200
	x := float64(rand.Intn(max-min) + min)
	startY := 600.0
	endY := -600.0

	riseSettings := map[string]interface{}{
		"x":       x,
		"start_y": startY,
		"end_y":   endY,
	}
	RiseSource(client, sceneName, sourceName, riseSettings)
	return nil
}

func Zoom(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {
	rl, ok := settings["level"]
	var level float64
	if !ok {
		level = 1.0
	} else {
		level = rl.(float64)
	}

	filterSettings := map[string]interface{}{
		"Filter.Transform.Camera":             1,
		"Filter.Transform.Camera.FieldOfView": level,
	}
	filterName := ViewerTransformFilter
	SetSourceFilterSettings(client, source, filterName, filterSettings)
	return nil
}

func ZoomOut(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	ToggleFilter(client, sourceName, "zoom_out", true)
	// BigBrain(c, mutex, sceneName, sourceName, settings)
	return nil
}

// =============== //
// Color / Outling //
// =============== //

func SuperSaiyan(client *OBSClient) {
	// go BigBrain(c, mutex)
	PulsatingHighlight(client, "BeginCam", 50, 60, true)
	RiseFromTheGrave(client)
}
