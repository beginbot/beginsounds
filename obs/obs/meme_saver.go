package obs

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gorm.io/gorm"
)

func MemeSaver(
	client *OBSClient,
	db *gorm.DB,
	cmd string,
	obsCmd Command,
	results <-chan string,
) (bool, error) {

	effectTriggered := true
	parts := obsCmd.Parts

	switch cmd {
	case "!water_the_memes":
		ms, _ := memes.Visible(db)
		for _, m := range ms {
			m.PositionType = "current"
			m.Scale *= 2.0
			obsCmd.Settings["scale"] = m.Scale
			ScaleSource(client, MemeScene, m.Name, obsCmd.Settings)
			m.Save(db)
		}
	case "!honey_i_shrunk_the_memes":
		ms, _ := memes.Visible(db)
		for _, m := range ms {
			m.PositionType = "current"
			m.Scale /= 2.0

			obsCmd.Settings["scale"] = m.Scale
			ScaleSource(client, MemeScene, m.Name, obsCmd.Settings)
			m.Save(db)
		}

	case "!save_meme":
		meme := MemeInfo(client, obsCmd.Scene, obsCmd.Source)
		meme.PositionType = "current"
		err := meme.Save(db)
		if err != nil {
			fmt.Printf("err = %+v\n", err)
		}
	case "!set_default", "!save_default":
		SaveDefault(client, obsCmd.DefaultMeme)
	case "!restore_scene":
		var name string
		if len(parts) < 2 {
			name = "piiq"
		} else {
			name = parts[1]
		}

		fmt.Printf("Attempting to restore Scene: %+v\n", name)

		// return true, nil

		var ms []memes.Meme
		memeFile := fmt.Sprintf("assets/ViewerSceneJson/%s.json", name)
		data, err := ioutil.ReadFile(memeFile)
		if err != nil {
			errMsg := fmt.Sprintf(
				fmt.Sprintf("%v Error Reading in %s", err, memeFile),
			)
			return false, errors.New(errMsg)
		}
		err = json.Unmarshal(data, &ms)

		for _, meme := range ms {
			defaultMeme, _ := memes.FindDefault(client.DB, meme.Name)
			// xModifier := 10.0
			// settings := map[string]interface{}{
			// 	"modifier": xModifier,
			// 	"start_x":  0.0,
			// 	"end_x":    meme.X,
			// 	"start_y":  -200.0,
			// 	"end_y":    meme.Y,
			// 	"y":        meme.Y,
			// 	"x":        meme.X,
			// 	"scale":    meme.Scale,
			// 	"rotation": meme.Rotation,
			// }

			// x := obsCmd.Settings["x"].(float64)
			// y := obsCmd.Settings["y"].(float64)
			// memes
			fmt.Printf("\tmeme.Name = %+v\n", meme.Name)
			// I think we need to toggle each on
			ToggleSource(client, defaultMeme.Scene, meme.Name, true)
			NewMoveSource(client, defaultMeme.Scene, meme.Name, meme.X, meme.Y, meme.Scale)
			// I want to use our specific

			// We should make this wackier here
			// and batch these requests
			// if i%2 == 0 {
			// 	go SlideSource(client, meme.Scene, meme.Name, settings)
			// } else {
			// 	go FallSource(client, meme.Scene, meme.Name, settings)
			// }

			// err := meme.Save(db)
			// if err != nil {
			// 	fmt.Printf("err Saving Meme %+v\n", err)
			// }

		}

	// TODO: This should iterate through ALL meme scenes
	case "!save_scene":
		fmt.Println("Attempting to Save Scene")
		ms := []memes.Meme{}
		// This saves the position of all
		// sources thought a goddamn hack
		var name string
		if len(parts) < 2 {
			name = "begintest"
		} else {
			name = parts[1]
		}

		CreateFakeSource(client)
		sources, _ := SourcesByVisibility(client, MemeScene)
		for _, source := range sources {
			meme := MemeInfo(client, MemeScene, source)
			ms = append(ms, meme)
		}
		memeFile, err := os.Create(fmt.Sprintf("assets/ViewerSceneJson/%s.json", name))
		if err != nil {
			fmt.Printf("\nError Creating Built JSON: %s", err)
		}
		encoder := json.NewEncoder(memeFile)
		encoder.Encode(ms)
		memeFile.Close()
	default:
		effectTriggered = false
	}

	return effectTriggered, nil
}

func SaveDefault(client *OBSClient, meme *memes.Meme) {
	fmt.Printf("Attempting Save Default: %s", meme.Name)

	// The reason why we create a fake source,
	// is force OBS to trigger exporting the position
	// of all sources
	// This is hack
	CreateFakeSource(client)

	memeInfo := MemeInfo(client, meme.Scene, meme.Name)

	fmt.Printf("New Meme info Fetched FROM OBS: %+v\n", memeInfo)

	// Create New Default Meme
	if meme.ID == 0 {
		fmt.Println("Creating New Default Meme")

		memeInfo.PositionType = "default"
		fmt.Printf("Saving Settings %s\n", meme.Name)

		err := client.DB.Save(memeInfo)
		if err != nil {
			fmt.Printf("err = %+v\n", err)
		}
		return
	}

	fmt.Println("Update Default Meme")
	// Update the settings of the already existing Meme
	tx := client.DB.Model(&meme).
		Where("ID = ?", meme.ID).
		Updates(memeInfo)

	if tx.Error != nil {
		fmt.Printf("err = %+v\n", tx.Error)
	}
}
