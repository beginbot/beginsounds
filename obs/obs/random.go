package obs

import (
	"fmt"
	"math/rand"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

var XMin = 0
var XMax = 1000
var YMin = 0
var YMax = 600

func randomY() int {
	return rand.Intn(YMax-YMin) + YMin
}

func randomX() int {
	return rand.Intn(XMax-XMin) + XMin
}

// This returns a batch of OBS Websocket Requests
type MemeTransformFunc = func(
	*OBSClient,
	*memes.Meme,
	map[string]interface{},
) []map[string]interface{}

// Of This can we remove
func RandomTransformRequestRaw(
	client *OBSClient,
	meme memes.Meme,
	moveSource bool,
) []map[string]interface{} {

	// First We must make the Scene visible
	renderReq := obsws.NewSetSceneItemRenderRequest(meme.Scene, meme.Name, 0, true)

	genericEffectOps := make([]interface{}, len(effectLimits))
	for i, v := range effectLimits {
		genericEffectOps[i] = v
	}
	shufEffects := shuffle(genericEffectOps)
	effect := shufEffects[0].(Effect)
	randVal := effect.RandomVal()

	settings := triggerViewer3DTransformSettings(
		client,
		meme.Name,
		effect.Command, // Filter command
		randVal,        // val
	)

	reqs := []map[string]interface{}{}

	tReq := obsws.NewSetSourceFilterSettingsRequest(meme.Name, "viewer_transform", settings)
	tReqMap := requestToMap(&tReq)

	ttReq := obsws.NewSetSourceFilterVisibilityRequest(meme.Name, "viewer_move", true)
	ttReqMap := requestToMap(&ttReq)

	if moveSource {
		xval := randomX()
		yval := randomY()
		req1 := obsws.NewSetSceneItemPositionRequest(meme.Scene, meme.Name, float64(xval), float64(yval))
		reqMap := requestToMap(&req1)
		reqs = append(reqs, reqMap)
	}

	renderReqMap := requestToMap(&renderReq)
	reqs = append(reqs, renderReqMap)
	reqs = append(reqs, tReqMap)
	reqs = append(reqs, ttReqMap)
	return reqs
}

func RandomTransformRequest(
	client *OBSClient,
	meme memes.Meme,
	moveSource bool,
) {

	// First We must make the Scene visible
	renderReq := obsws.NewSetSceneItemRenderRequest(meme.Scene, meme.Name, 0, true)

	genericEffectOps := make([]interface{}, len(effectLimits))
	for i, v := range effectLimits {
		genericEffectOps[i] = v
	}
	shufEffects := shuffle(genericEffectOps)
	effect := shufEffects[0].(Effect)
	randVal := effect.RandomVal()

	fmt.Printf("\teffect = %+v\n", effect)
	settings := triggerViewer3DTransformSettings(
		client,
		meme.Name,
		effect.Command, // Filter command
		randVal,        // val
	)
	fmt.Printf("\tsettings = %+v\n", settings)
	reqs := []map[string]interface{}{}

	tReq := obsws.NewSetSourceFilterSettingsRequest(meme.Name, "viewer_transform", settings)
	tReqMap := requestToMap(&tReq)

	ttReq := obsws.NewSetSourceFilterVisibilityRequest(meme.Name, "viewer_move", true)
	ttReqMap := requestToMap(&ttReq)

	if moveSource {
		xval := randomX()
		yval := randomY()
		req1 := obsws.NewSetSceneItemPositionRequest(meme.Scene, meme.Name, float64(xval), float64(yval))
		reqMap := requestToMap(&req1)
		reqs = append(reqs, reqMap)
	}

	renderReqMap := requestToMap(&renderReq)
	reqs = append(reqs, renderReqMap)
	reqs = append(reqs, tReqMap)
	reqs = append(reqs, ttReqMap)
	fmt.Println("Batch Request")
	BatchRequest(client, reqs)
}
