package obs

import (
	obsws "github.com/davidbegin/go-obs-websocket"
)

// PrimaryScene the main scene you stream from
var PrimaryScene = "Primary"

// MemeScene all the viewer added memes
var MemeScene = "viewer_memes"

// BeginCam the name of Begin's camera source
var BeginCam = "BeginCam"

var OutfitsScene = "outfits"

var BackgroundsScene = "backgrounds_scene"

var MemeLimbo = "meme_limbo"

var PokemonScene = "pokemon_scene"

var StreamerScene = "streamer_scene"

// We could use this for hiding all sources on a scene
var Scenes = map[string]string{
	"memes":       MemeScene,
	"outfits":     OutfitsScene,
	"backgrounds": BackgroundsScene,
	"limbo":       MemeLimbo,
	"pokemon":     PokemonScene,
	"streamers":   StreamerScene,
}

var MemeScenes = []string{
	MemeScene,
	BeginCam,
	OutfitsScene,
	BackgroundsScene,
	MemeLimbo,
	PokemonScene,
	StreamerScene,
}

// ChangeScene simple call to change the OBS Scene
func ChangeScene(client *OBSClient, sceneName string) {
	req := obsws.NewSetCurrentSceneRequest(sceneName)
	client.execOBSCommand(&req)
}

func GetCurrentScene(client *OBSClient) string {
	req := obsws.NewGetCurrentSceneRequest()
	res, _ := client.execOBSCommand(&req)
	r := res.(obsws.GetCurrentSceneResponse)
	return r.Name
}

func CreateScene(client *OBSClient, name string) {
	req := obsws.NewCreateSceneRequest(name)
	client.execOBSCommand(&req)
	// res.(obsws.GetCurrentSceneResponse)
}

func AddSceneToScene(client *OBSClient, destScene string, sourceScene string) {
	req := obsws.NewAddSceneItemRequest(destScene, sourceScene, true)
	client.execOBSCommand(&req)
}
