package obs

import (
	"encoding/json"
	"io/ioutil"
)

type SourceToggleConfig struct {
	Scene  string
	Source string
	Toggle bool
}

// ToggleSources shortcuts to toggle various sources on and off
var ToggleSources = map[string]SourceToggleConfig{
	"!vibecat": {
		Scene:  MemeScene,
		Source: "vibecat",
		Toggle: true,
	},
	"!freebegin": {
		Scene:  MemeScene,
		Source: "cage",
		Toggle: false,
	},
	"!cage": {
		Scene:  MemeScene,
		Source: "cage",
		Toggle: true,
	},
}

func parseConfig(configFile string) []ObsCommand {
	var results []ObsCommand
	f, err := ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(f, &results); err != nil {
		panic(err)
	}
	return results
}
