package obs

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"sync"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

// These need to take in the OBSClient
type TransformFuncCollection = func(
	*OBSClient,
	string,
	Command,
	chan<- string,
) (bool, error)

var funcOpts = []transformFunc{
	ChadSource,
	BigBrain,
	ZoomSpinSource,
	SlideInFromSide,
	floatSourceThroughScene,
}

// ===================================================

func RandomPos() (float64, float64) {
	x := float64(rand.Intn(1280))
	y := float64(rand.Intn(720))
	return x, y
}

func ProcessObsCommands(
	client *OBSClient,
	cmds <-chan Command,
) <-chan string {

	results := make(chan string)

	go func() {
		defer close(results)

		for cmd := range cmds {
			cmd.Func(client, cmd.Scene, cmd.Source, cmd.Settings)
		}
	}()

	return results
}

func FindSourceAndScene(message chat.ChatMessage) Command {
	msg := message.Message
	parts := strings.Split(msg, " ")
	cmd := Command{
		Parts:   parts,
		Message: message,
	}

	if len(parts) < 2 {
		return cmd
	}

	source := parts[1]
	// cmd.Source = strings.ToLower(source)
	cmd.Source = source

	switch cmd.Source {
	case "922":
		cmd.Scene = PrimaryScene
	case "twitchchat", "chat":
		cmd.Scene = PrimaryScene
		cmd.Source = "twitchchat"
	case "begin":
		cmd.Scene = PrimaryScene
		cmd.Source = "BeginCam"
	default:
		cmd.Scene = MemeScene
	}

	filters, ok := FilterMap[parts[0]]
	if ok {
		cmd.Filters = filters
	}
	return cmd
}

func randomTransformFunc() transformFunc {
	randInt := rand.Intn(len(funcOpts))
	return funcOpts[randInt]
}

// TODO: Move this
func FindPlayer(db *gorm.DB, msg chat.ChatMessage) *player.Player {
	if msg.PlayerID != 0 {
		return player.FindByID(db, msg.PlayerID)
	}

	if msg.PlayerName != "" {
		return player.Find(db, msg.PlayerName)
	}

	return &player.Player{}
}

// The goal of this was pipe
// this is where we want pipe
func ChatExploder(commands <-chan chat.ChatMessage) <-chan chat.ChatMessage {
	results := make(chan chat.ChatMessage, 10000)

	go func() {
		defer close(results)

		for msg := range commands {

			// We found a special 3D transform
			for key := range threeDTransformFilterCommands {
				parts := strings.Split(msg.Message, " ")
				if parts[0] == key {
					results <- msg
					continue
				}
			}

			// I almost want to to check for the appearance of &s
			// and let other code handle
			subCmds := strings.Split(msg.Message, "|")

			for _, cmd := range subCmds {

				c := chat.ChatMessage{
					PlayerName: msg.PlayerName,
					PlayerID:   msg.PlayerID,
					Streamlord: msg.Streamlord,
					Streamgod:  msg.Streamgod,
					StreetCred: msg.StreetCred,
					CoolPoints: msg.CoolPoints,
					Message:    strings.TrimSpace(cmd),
				}
				results <- c

			}
		}
	}()

	return results
}

func TestingClient() *OBSClient {
	db := database.CreateDBConn("beginsounds4")
	c := obsws.Client{Host: "localhost", Port: 4444}
	var mutex = &sync.Mutex{}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	// WE can't have disconnect here I don't think
	// defer c.Disconnect()

	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	client := OBSClient{
		Client: &c,
		Mutex:  mutex,
		Logger: logger,
		DB:     db,
	}

	return &client
}

func shuffle(items []interface{}) []interface{} {
	rand.Shuffle(len(items), func(i, j int) {
		items[i], items[j] = items[j], items[i]
	})
	return items
}

func fetchFloat(parts []string, index int, defaultVal float64) float64 {
	res := defaultVal

	if len(parts) > index {
		rawVal := parts[index]
		intVal, err := strconv.Atoi(rawVal)
		if err == nil {
			res = float64(intVal)
		}
	}

	return res
}

func fetchInt(parts []string, index int, defaultVal int) int {
	res := defaultVal

	if len(parts) > index {
		rawVal := parts[index]
		intVal, err := strconv.Atoi(rawVal)
		if err == nil {
			res = int(intVal)
		}
	}

	return res
}

func requestToMap(req obsws.Request) map[string]interface{} {
	jsonReq1, _ := json.Marshal(req)
	var req1Map map[string]interface{}
	json.Unmarshal(jsonReq1, &req1Map)
	return req1Map
}
