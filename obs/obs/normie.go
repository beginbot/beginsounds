package obs

// ReturnToNormie This is meant to return all of BeginWorld
// 	back to a normal state
func ReturnToNormie(client *OBSClient) {
	standardSources := []string{
		"Screen",
		"Chat",
		"BeginCam",
		"keyboard",
	}

	for _, source := range standardSources {
		ToggleFilter(client, source, "outline", false)
		ToggleFilter(client, source, ViewerTransformFilter, false)
	}

}
