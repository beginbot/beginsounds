package obs

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"

	obsws "github.com/davidbegin/go-obs-websocket"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

var defaultMoveMemeTransformTextSettings = TransformTextSettings{
	Rot: 0,
	Pos: Coordinates{
		X: 0,
		Y: 0,
	},
	Scale: Coordinates{
		X: 1,
		Y: 1,
	},
	Bounds: Coordinates{
		X: 0,
		Y: 0,
	},
	Crop: CropValues{
		L: 0,
		T: 0,
		R: 0,
		B: 0,
	},
}

var scaleToCenterTransformTextSetings = TransformTextSettings{
	Rot: 900,
	Pos: Coordinates{
		X: 251,
		Y: 127,
	},
	Scale: Coordinates{
		X: 140.7,
		Y: 140.7,
	},
	Bounds: Coordinates{
		X: 251,
		Y: 127,
	},
	Crop: CropValues{
		L: 0,
		T: 0,
		R: 0,
		B: 0,
	},
}

// scale:map[x:14.788461685180664 y:14.79411792755127] setting_float:0 single_setting:false source:paras start_trigger:0 transform_text:pos: x 251 y 127 rot: 0.0 scale: x 14.788 y 14.794 bounds: x 0 y 0
// Filter.Transform.Camera.FieldOfView:90 Filter.Transform.Position.X:0 Filter.Transform.Position.Y:0 Filter.Transform.Position.Z:0 Filter.Transform.Rotation.X:0 Filter.Transform.Rotation.Y:0 Filter.Transform.Rotation.Z:0 Filter.Transform.Scale.X:100 Filter.Transform.Scale.Y:100 Filter.Transform.Shear.X:0 Filter.Transform.Shear.Y:0 bounds:map[x:0 y:0] crop_bottom:0 crop_left:0 crop_right:0
// crop_top:0 duration:3000 easing_function_match:3 filter:viewer_transform pos:map[x:251 y:127] rot:0
// scale:map[x:14.788461685180664 y:14.79411792755127] setting_float:0 single_setting:false source:paras start_trigger:0 transform_text:pos: x 251 y 127 rot: 0.0 scale: x 14.788 y 14.794 bounds: x 0 y 0
// crop: l 0 t 0 r 0 b 0 value_type:0]
var scaleToCenter = MoveSourceSettings{
	Bounds:              Coordinates{X: 661, Y: 462},
	CropBottom:          0,
	CropLeft:            0,
	CropRight:           0,
	CropTop:             0,
	Duration:            30000,
	EasingFunctionMatch: EasingFunctionNums["Cubic"],
	// EasingFunctionMatch: EasingFunctionNums["Bounce"],
	Pos:           Coordinates{X: 251, Y: 127},
	Rot:           9000,
	Scale:         Coordinates{X: 100, Y: 100},
	TransformText: scaleToCenterTransformTextSetings,
}

var defaultMoveBeginCamTransformTextSettings = TransformTextSettings{
	Rot: 0,
	Pos: Coordinates{
		X: 700,
		Y: 301,
	},
	Scale: Coordinates{
		X: -1,
		Y: 1,
	},
	Bounds: Coordinates{
		X: 661,
		Y: 462,
	},
	Crop: CropValues{
		L: 0,
		T: 0,
		R: 0,
		B: 0,
	},
}

var EasingFunctionNums = map[string]int{
	"Quadratic":   1,
	"Cubic":       2,
	"Quartic":     3,
	"Quintic":     4,
	"Sine":        5,
	"Circular":    6,
	"Exponential": 7,
	"Elastic":     8,
	"Bounce":      9,
	"Back":        10,
}

func newScaleToCenter(source string) MoveSourceSettings {
	settings := scaleToCenter
	settings.Source = source
	return settings
}

func newDefaultMoveSourceSetings(source string) MoveSourceSettings {
	settings := defaultMoveMemeSettings
	settings.Source = source
	return settings
}

func defaultMoveSourceSetings(source string) MoveSourceSettings {
	settings := defaultMoveBeginCamSettings
	settings.Source = source
	return settings
}

var defaultMoveMemeSettings = MoveSourceSettings{
	Bounds:              Coordinates{X: 0, Y: 0},
	CropBottom:          0,
	CropLeft:            0,
	CropRight:           0,
	CropTop:             0,
	Duration:            3000,
	EasingFunctionMatch: EasingFunctionNums["Bounce"],
	Pos:                 Coordinates{X: 0, Y: 0},
	Rot:                 0,
	Scale:               Coordinates{X: 1, Y: 1},
	TransformText:       defaultMoveMemeTransformTextSettings,
}

var defaultMoveBeginCamSettings = MoveSourceSettings{
	Bounds:              Coordinates{X: 661, Y: 462},
	CropBottom:          0,
	CropLeft:            0,
	CropRight:           0,
	CropTop:             0,
	Duration:            3000,
	EasingFunctionMatch: EasingFunctionNums["Bounce"],
	Pos:                 Coordinates{X: 700, Y: 301},
	Rot:                 0,
	Scale:               Coordinates{X: -1, Y: 1},
	Source:              "BeginCam",
	TransformText:       defaultMoveBeginCamTransformTextSettings,
}

// pos: x 36 y -15 rot: 50.0 scale: x 0.140 y 0.140 bounds: x 0 y 0 crop: l 13 t 47 r 341 b 0
// map[bounds:map[x:0 y:0] crop_bottom:0 crop_left:13 crop_right:341 crop_top:47 duration:1000 easing_function_match:10 pos:map[x:36 y:-15] rot:50 scale:map[x:0.14000000059604645 y:0.14000000059604645] source:vibecat transform_text:pos: x 36 y -15 rot: 50.0 scale: x 0.140 y 0.140 bounds: x 0 y 0 crop: l 13 t 47 r 341 b 0]

type MoveSourceSettings struct {
	Bounds              Coordinates `json:"bounds"`
	CropBottom          int         `json:"crop_bottom" mapstructure:"crop_bottom"`
	CropTop             int         `json:"crop_top" mapstructure:"crop_top"`
	CropLeft            int         `json:"crop_left" mapstructure:"crop_left"`
	CropRight           int         `json:"crop_right" mapstructure:"crop_right"`
	Duration            int         `json:"duration"`
	EasingFunctionMatch int         `json:"easing_function_match" mapstructure:"easing_function_match"`
	Pos                 Coordinates `json:"pos"`
	Rot                 float64     `json:"rot"`
	Scale               Coordinates `json:"scale"` // not really coordinates
	Source              string      `json:"source"`

	TransformText    TransformTextSettings `json:"transform_text" mapstructure:"-"`
	RawTransformText string                `json:"-" mapstructure:"-"`
	// RawTransformText string                 `json:"-" mapstructure:"transform_text"`
	Other map[string]interface{} `json:"-" mapstructure:",remain"`
}

func (settings *MoveSourceSettings) ToMap() map[string]interface{} {
	jsonSettings, _ := json.Marshal(settings)
	var sourceSettingsMap map[string]interface{}
	json.Unmarshal(jsonSettings, &sourceSettingsMap)
	// sourceSettingsMap["transform_text"] = settings.TransformText.String()
	return sourceSettingsMap
}

func (settings *TransformTextSettings) String() string {
	return fmt.Sprintf(
		"pos: x %f y %f rot: %f scale: x %f y %f bounds: x %f y %f crop: l %d t %d r %d b %d",
		settings.Pos.X,
		settings.Pos.Y,
		settings.Rot,
		settings.Scale.X,
		settings.Scale.Y,
		settings.Bounds.X,
		settings.Bounds.Y,
		settings.Crop.L,
		settings.Crop.T,
		settings.Crop.R,
		settings.Crop.B,
	)
}

// pos: x 36 y -15 rot: 50.0 scale: x 0.140 y 0.140 bounds: x 0 y 0 crop: l 13 t 47 r 341 b 0
type TransformTextSettings struct {
	Pos    Coordinates `json:"pos"`
	Rot    float64     `json:"rot"`
	Scale  Coordinates `json:"scale"`
	Bounds Coordinates `json:"bounds"`
	Crop   CropValues  `json:"crop"`
}

type Coordinates struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

type CropValues struct {
	L int `json:"l"`
	T int `json:"t"`
	R int `json:"r"`
	B int `json:"b"`
}

func parseTransformValues(rawText string, regEx *regexp.Regexp) map[string]string {
	match := regEx.FindStringSubmatch(rawText)

	paramsMap := make(map[string]string)
	for i, name := range regEx.SubexpNames() {
		if i > 0 && i <= len(match) {
			paramsMap[name] = match[i]
		}
	}
	return paramsMap
}

// pos: x 36 y -15 rot: 50.0 scale: x 0.140 y 0.140 bounds: x 0 y 0 crop: l 13 t 47 r 341 b 0
func ParseRawTransformText(rawText string) TransformTextSettings {
	var posRegex = regexp.MustCompile(`pos: x (?P<X>-?\d+) y (?P<Y>-?\d+)`)
	var rotRegex = regexp.MustCompile(`rot: (?P<Rot>-?\d+)`)
	var scaleRegex = regexp.MustCompile(`scale: x (?P<X>-?\d+.\d+) y (?P<Y>-?\d+.\d+)`)
	var boundsRegex = regexp.MustCompile(`bounds: x (?P<X>-?\d+) y (?P<Y>-?\d+)`)
	var cropRegex = regexp.MustCompile(`crop: l (?P<L>-?\d+) t (?P<T>-?\d+) r (?P<R>-?\d+) b (?P<B>-?\d+)`)

	posVals := parseTransformValues(rawText, posRegex)
	rotVals := parseTransformValues(rawText, rotRegex)
	scaleVals := parseTransformValues(rawText, scaleRegex)
	boundsVals := parseTransformValues(rawText, boundsRegex)
	cropVals := parseTransformValues(rawText, cropRegex)

	posX, _ := strconv.ParseFloat(posVals["X"], 64)
	posY, _ := strconv.ParseFloat(posVals["Y"], 64)
	rot, _ := strconv.ParseFloat(rotVals["Rot"], 64)

	scaleX, _ := strconv.ParseFloat(scaleVals["X"], 64)
	scaleY, _ := strconv.ParseFloat(scaleVals["Y"], 64)
	boundsX, _ := strconv.ParseFloat(boundsVals["X"], 64)
	boundsY, _ := strconv.ParseFloat(boundsVals["Y"], 64)

	cropT, _ := strconv.ParseInt(cropVals["T"], 10, 64)
	cropB, _ := strconv.ParseInt(cropVals["B"], 10, 64)
	cropL, _ := strconv.ParseInt(cropVals["L"], 10, 64)
	cropR, _ := strconv.ParseInt(cropVals["R"], 10, 64)

	settings := TransformTextSettings{
		Pos: Coordinates{
			X: posX,
			Y: posY,
		},
		Rot: rot,
		Scale: Coordinates{
			X: scaleX,
			Y: scaleY,
		},
		Bounds: Coordinates{
			X: boundsX,
			Y: boundsY,
		},
		Crop: CropValues{
			T: int(cropT),
			B: int(cropB),
			L: int(cropL),
			R: int(cropR),
		},
	}

	fmt.Printf("settings = %+v\n", settings)

	return settings
}

func MapToMoveFilter(rawSettings map[string]interface{}) MoveSourceSettings {
	var settings MoveSourceSettings
	err := mapstructure.Decode(rawSettings, &settings)
	if err != nil {
		fmt.Printf("Error decoding move source settings: %+v\n", err)
	}
	return settings
}

func UpdateTransformFilterAndCall(
	client *OBSClient,
	meme *memes.Meme,
	settings map[string]interface{},
) []map[string]interface{} {
	// First We must make the Scene visible
	renderReq := obsws.NewSetSceneItemRenderRequest(meme.Scene, meme.Name, 0, true)

	reqs := []map[string]interface{}{}

	tReq := obsws.NewSetSourceFilterSettingsRequest(meme.Name, "viewer_transform", settings)
	tReqMap := requestToMap(&tReq)

	ttReq := obsws.NewSetSourceFilterVisibilityRequest(meme.Name, "viewer_move", true)
	ttReqMap := requestToMap(&ttReq)

	// if moveSource {
	// 	xval := randomX()
	// 	yval := randomY()
	// 	req1 := obsws.NewSetSceneItemPositionRequest(
	// 		meme.Scene,
	// 		meme.Name,
	// 		float64(xval),
	// 		float64(yval),
	// 	)
	// 	reqMap := requestToMap(&req1)
	// 	reqs = append(reqs, reqMap)
	// }

	renderReqMap := requestToMap(&renderReq)
	reqs = append(reqs, renderReqMap)
	reqs = append(reqs, tReqMap)
	reqs = append(reqs, ttReqMap)
	return reqs
}
