package obs

import (
	"log"

	obsws "github.com/davidbegin/go-obs-websocket"
)

// Do we want running Event handlers,
// or do we want to make temporary ones?

// "SwitchScenes"
// "ScenesChanged"
// "SceneItemVisibilityChanged"
// "SceneItemTransformChanged"
// "SourceFilterAdded"
// "SourceFilterRemoved"
// "SourceFilterVisibilityChanged"
func HandleEvents(client *OBSClient) {
	client.Client.AddEventHandler("SceneChange", func(e obsws.Event) {
		// * @name SourceFilterVisibilityChanged
		// Make sure to assert the actual event type.
		log.Println("new scene:", e.(obsws.SwitchScenesEvent).SceneName)
	})

	client.Client.AddEventHandler("SwitchScenes", func(e obsws.Event) {
		// Make sure to assert the actual event type.
		log.Println("Switched scenes:", e.(obsws.SwitchScenesEvent).SceneName)
	})

	client.Client.AddEventHandler("SourceFilterAdded", func(e obsws.Event) {
		// Make sure to assert the actual event type.
		log.Println("Added Filter:", e.(obsws.SourceFilterAddedEvent).FilterName)
	})

	client.Client.AddEventHandler("SourceFilterRemoved", func(e obsws.Event) {
		// Make sure to assert the actual event type.
		log.Println("Added Filter:", e.(obsws.SourceFilterRemovedEvent).FilterName)
	})

	client.Client.AddEventHandler("SourceFilterVisibilityChanged", func(e obsws.Event) {
		event := e.(obsws.SourceFilterVisibilityChangedEvent)
		log.Printf("Filter Visibility Changed:%s - %s | %t\n", event.SourceName, event.FilterName, event.FilterEnabled)
	})

	client.Client.AddEventHandler("SceneItemVisibilityChanged", func(e obsws.Event) {
		event := e.(obsws.SceneItemVisibilityChangedEvent)
		log.Printf("Scene Item Changed:%s - %s | %t\n", event.SceneName, event.ItemName, event.ItemVisible)
	})
	// "SceneItemVisibilityChanged"
	// "SceneItemTransformChanged"
}

// ======= //
// Sources //
// ======= //

// "SourceCreated"
// "SourceDestroyed"
// "SourceVolumeChanged"
// "SourceMuteStateChanged"
// "SourceAudioSyncOffsetChanged"
// "SourceAudioMixersChanged"
// "SourceRenamed"
// "SourceFiltersReordered"
// "SourceOrderChanged"

// ====== //
// Scenes //
// ====== //

// "SceneCollectionChanged"
// "SceneCollectionListChanged"
// "SceneItemAdded"
// "SceneItemRemoved"
// "SceneItemSelected"
// "SceneItemDeselected"
// "PreviewSceneChanged"

// ========== //
// Transition //
// ========== //

// "SwitchTransition"
// "TransitionListChanged"
// "TransitionDurationChanged"
// "TransitionBegin"

// ========= //
// Recording //
// ========= //

// "RecordingStarting"
// "RecordingStarted"
// "RecordingStopping"
// "RecordingStopped"
// "RecordingPaused"
// "RecordingResumed"

// ============= //
// Stream Status //
// ============= //

// "StreamStarting"
// "StreamStarted"
// "StreamStopping"
// "StreamStopped"
// "StreamStatus"
// "Exiting"
// "Heartbeat"

// ====== //
// Replay //
// ====== //

// "ReplayStarting"
// "ReplayStarted"
// "ReplayStopping"
// "ReplayStopped"

// ===== //
// Other //
// ===== //

// "ProfileChanged"
// "ProfileListChanged"
// "BroadcastCustomMessage"
// "StudioModeSwitched"
