package obs

// TODO: combine this with AddTransformFilter
// AddViewerTransformFilter adds a filter to a source
func AddViewerTransformFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	filterName := ViewerTransformFilter
	settings := FilterSetting{
		Name:       filterName,
		FilterType: "streamfx-filter-transform",
		Settings: map[string]interface{}{
			"Backlight Compensation":              0,
			"Brightness":                          0,
			"Contrast":                            0,
			"Exposure (Absolute)":                 0,
			"Filter.Transform.Camera":             1,
			"Filter.Transform.Camera.FieldOfView": 90,
			"Filter.Transform.Position.X":         0,
			"Filter.Transform.Position.Y":         0,
			"Filter.Transform.Position.Z":         0,
			"Filter.Transform.Rotation.X":         0,
			"Filter.Transform.Rotation.Y":         0,
			"Filter.Transform.Rotation.Z":         0,
			"Filter.Transform.Scale.X":            100,
			"Filter.Transform.Scale.Y":            100,
			"Filter.Transform.Shear.X":            0,
			"Filter.Transform.Shear.Y":            0,
			"Focus (absolute)":                    0,
			"Gain":                                0,
			"Pan (Absolute)":                      0,
			"Saturation":                          0,
			"Sharpness":                           0,
			"Tilt (Absolute)":                     0,
			"White Balance Temperature":           0,
			"Zoom Absolute":                       0,
		},
	}

	// setSourceFilterSettings(client, sourceName, filterName, settings.Settings)
	// return nil
	return addFilter(
		client,
		sceneName,
		sourceName,
		settings,
	)
}

// AddTransformFilter adds a filter to a source
func AddTransformFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	settings := FilterSetting{
		Name:       "transform",
		FilterType: "streamfx-filter-transform",
		Settings: map[string]interface{}{
			"Backlight Compensation":              0,
			"Brightness":                          0,
			"Contrast":                            0,
			"Exposure (Absolute)":                 0,
			"Filter.Transform.Camera":             1,
			"Filter.Transform.Camera.FieldOfView": 90,
			"Filter.Transform.Position.X":         0,
			"Filter.Transform.Position.Y":         0,
			"Filter.Transform.Position.Z":         0,
			"Filter.Transform.Rotation.X":         0,
			"Filter.Transform.Rotation.Y":         0,
			"Filter.Transform.Rotation.Z":         0,
			"Filter.Transform.Scale.X":            100,
			"Filter.Transform.Scale.Y":            100,
			"Filter.Transform.Shear.X":            0,
			"Filter.Transform.Shear.Y":            0,
			"Focus (absolute)":                    0,
			"Gain":                                0,
			"Pan (Absolute)":                      0,
			"Saturation":                          0,
			"Sharpness":                           0,
			"Tilt (Absolute)":                     0,
			"White Balance Temperature":           0,
			"Zoom Absolute":                       0,
		},
	}

	return addFilter(
		client,
		sceneName,
		sourceName,
		settings,
	)
}

func UpdateNormieTransform(
	client *OBSClient,
	sceneName string,
	sourceName string,
) {
	filterSettings := map[string]interface{}{
		"Filter.Transform.Camera.FieldOfView": 90,
		"Filter.Transform.Position.X":         0,
		"Filter.Transform.Position.Y":         0,
		"Filter.Transform.Position.Z":         0,
		"Filter.Transform.Rotation.X":         0,
		"Filter.Transform.Rotation.Y":         0,
		"Filter.Transform.Rotation.Z":         0,
		"Filter.Transform.Scale.X":            100,
		"Filter.Transform.Scale.Y":            100,
		"Filter.Transform.Shear.X":            0,
		"Filter.Transform.Shear.Y":            0,
		"duration":                            3000,
		"filter":                              "transform",
		"setting_float":                       0,
		"single_setting":                      false,
		"start_trigger":                       0,
		"value_type":                          0,
	}
	filterName := "normie_transform"
	SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
}

// ResetTransform sets all the settings for the "transform" filter
// 		back to defaults
func ResetTransform(client *OBSClient, sourceName string) {
	filterSettings := map[string]interface{}{
		"Filter.Transform.Camera.FieldOfView": 90.00,
		"Filter.Transform.Position.Y":         0.00,
		"Filter.Transform.Position.Z":         0.00,
		"Filter.Transform.Rotation.X":         0.00,
		"Filter.Transform.Rotation.Y":         0.00,
		"Filter.Transform.Rotation.Z":         0.00,
		"Filter.Transform.Scale.X":            90.00,
		"Filter.Transform.Scale.Y":            90.00,
		"Filter.Transform.Shear.X":            0.0,
		"Filter.Transform.Shear.Y":            0.0,
	}

	filterName := "viewer_transform"
	SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
}

// This is just some settings and a filter
func FunZoom(
	client *OBSClient,
	sourceName string,
) {
	filterSettings := map[string]interface{}{
		"Filter.Transform.Camera":             0,
		"Filter.Transform.Camera.FieldOfView": 90,
		"Filter.Transform.Position.X":         0,
		"Filter.Transform.Position.Y":         -0.03,
		"Filter.Transform.Position.Z":         0,
		"Filter.Transform.Rotation.X":         -180,
		"Filter.Transform.Rotation.Y":         180,
		"Filter.Transform.Rotation.Z":         0,
		"Filter.Transform.Scale.X":            0,
		"Filter.Transform.Scale.Y":            0,
		"Filter.Transform.Shear.X":            200,
		"Filter.Transform.Shear.Y":            200,
	}
	filterName := "transform"
	SetSourceFilterSettings(client, sourceName, filterName, filterSettings)
}
