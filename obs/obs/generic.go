package obs

import (
	"fmt"

	obsws "github.com/davidbegin/go-obs-websocket"
)

// ToggleFilter is meant to take a Scene, a filter
// and you can toggle a filter off easily!
func ToggleFilter(
	client *OBSClient,
	sourceName string,
	filterName string,
	toggle bool,
) (obsws.Response, error) {
	req := obsws.NewSetSourceFilterVisibilityRequest(sourceName, filterName, toggle)
	resp, err := client.execOBSCommand(&req)
	if err != nil {
		fmt.Printf("Error Setting Filter Settings: %s %s = %+v\n", sourceName, filterName, err)
	}
	return resp, err
}

// Request - A Request to OBS
type Request interface {
	Send(obsws.Client) error
	Receive() (obsws.Response, error)
}

func (client *OBSClient) execOBSCommand(req Request) (obsws.Response, error) {
	client.Mutex.Lock()
	defer client.Mutex.Unlock()

	if err := req.Send(*client.Client); err != nil {
		fmt.Println(err)
		client.Logger.Println(err)
	}
	resp, err := req.Receive()
	if err != nil {
		fmt.Println(err)
		client.Logger.Println(err)
	}
	return resp, err
}

func SetSourceFilterSettings(
	client *OBSClient,
	source string,
	filterName string,
	settings map[string]interface{},
) (obsws.Response, error) {
	req := obsws.NewSetSourceFilterSettingsRequest(source, filterName, settings)
	return client.execOBSCommand(&req)
}

func processMoveSceneItem(
	client *OBSClient,
	req obsws.Request,
) (obsws.SetSceneItemPositionResponse, error) {

	resp, err := client.execOBSCommand(req)
	r := resp.(obsws.SetSceneItemPositionResponse)
	return r, err
}

func toggleTransform(client *OBSClient, sceneName string, toggle bool) {
	req := obsws.NewSetSourceFilterVisibilityRequest(sceneName, ViewerTransformFilter, toggle)
	reqs := []map[string]interface{}{
		{
			"request":      req,
			"request-type": "GetVersion",
			"request-id":   "fake_id",
		},
	}
	fmt.Println("about to Batch Request")
	BatchRequest(client, reqs)
}
