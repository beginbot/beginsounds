package obs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"strings"
	"sync"
	"time"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/obs/obs/filters"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

func ToggleBitLightSaberBattle(client *OBSClient) {
	duration := 100

	var results map[string]interface{}
	filename := fmt.Sprintf("move_default.json")
	// filepath := fmt.Sprintf("../../tmp/%s", filename)
	filepath := fmt.Sprintf("tmp/%s", filename)

	f, err := ioutil.ReadFile(filepath)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(f, &results); err != nil {
		panic(err)
	}

	ms, _ := memes.Visible(client.DB)
	moveSource := MapToMoveFilter(results)
	moveSource.EasingFunctionMatch = 2
	moveSource.Duration = duration

	// Attac
	// moveSource.Pos.X = 001
	// moveSource.Pos.Y = 450

	moveSource.Pos.X = 100
	moveSource.Pos.Y = 200
	// !scale snorlax 0.4

	// moveSource.Pos.X = 400
	// moveSource.Pos.Y = 0

	// for every
	desiredSettings := []TransformSetting{
		{
			Name:   "Filter.Transform.Rotation.Y",
			Value:  360,
			Range:  3600,
			Random: true,
		},
		{
			Name:   "Filter.Transform.Rotation.X",
			Value:  360,
			Range:  21,
			Random: true,
		},
	}
	reqs := []map[string]interface{}{}
	toggleMeme, _ := memes.FindDefault(client.DB, "melkeycam")
	// toggleMeme, _ := memes.FindDefault(client.DB, "quiGonTogglebit")
	settings := generateTransformFilterSettingsFromList(desiredSettings)
	toggleReqs := UpdateTransformFilterAndCall(client, toggleMeme, settings)
	reqs = append(reqs, toggleReqs...)
	BatchRequest(client, reqs)

	var count int

	timer := time.NewTimer(time.Second * 10)
	// I want to listen a counter
	// for {
	for {
		select {
		case <-timer.C:
			return
		default:
			reqs := []map[string]interface{}{}
			for _, m := range ms {
				if m.Name == "primetime" || m.Name == "melkeycam" || m.Name == "quiGonTogglebit" {
					continue
				}
				rand.Seed(time.Now().UnixNano())
				settings := moveSource
				settings.Source = m.Name
				filtername := fmt.Sprintf("MoveSource%s", strings.Title(settings.Source))
				moveSource.Rot = float64(rand.Intn(360))
				settings.Scale.X = m.Scale
				settings.Scale.Y = m.Scale
				settings.Pos.X = settings.Pos.X + float64(rand.Intn(300))
				settings.Pos.Y = settings.Pos.Y + float64(rand.Intn(300))
				moveSource.EasingFunctionMatch = rand.Intn(10)

				newSettings := settings.ToMap()

				r1 := obsws.NewSetSourceFilterSettingsRequest(m.Scene, filtername, newSettings)
				r2 := obsws.NewSetSourceFilterVisibilityRequest(m.Scene, filtername, true)
				r1Map := requestToMap(&r1)
				r2Map := requestToMap(&r2)
				reqs = append(reqs, r1Map)
				reqs = append(reqs, r2Map)
				count++
			}

			if count%200 == 0 {
				// lightsaberCmd := stream_command.Find(client.DB, "lightsaber")
				// _, err := soundboard.CreateAudioRequest(client.DB, "lightsaber", "beginbotbot")
				// p := player.Find(client.DB, "beginbot")
				// aq := audio_request.AudioRequest{
				// 	PlayerID:     p.ID,
				// 	Name:         "lightsaber",
				// 	StreamJester: true,
				// 	Streamlord:   true,
				// 	Notify:       false,
				// 	Filename:     lightsaberCmd.Filename,
				// 	Path:         fmt.Sprintf("/home/begin/stream/Stream/Samples/%s", lightsaberCmd.Filename),
				// }

				// audio_request.Save(client.DB, &aq)
				// if err != nil {
				// 	fmt.Printf("err = %+v\n", err)
				// }
				settings := generateTransformFilterSettingsFromList(desiredSettings)
				toggleReqs := UpdateTransformFilterAndCall(client, toggleMeme, settings)
				reqs = append(reqs, toggleReqs...)

			}
			BatchRequest(client, reqs)
		}
	}
}

type TransformSetting struct {
	Name   string
	Value  float64
	Range  int
	Random bool
}

func generateTransformFilterSettingsFromList(
	transformSettings []TransformSetting,
) map[string]interface{} {
	settings := filters.DefaultTransformFilter

	for _, setting := range transformSettings {
		var val = setting.Value
		if setting.Random {
			val = float64(rand.Intn(setting.Range))
		}
		settings[setting.Name] = val
	}
	return settings
}

// NOTE: This is ONLY for transform filter effects on sources
func CallEffectOnVisibleMemes(
	client *OBSClient,
	transformFunc MemeTransformFunc,
	desiredSettings []TransformSetting,
) {

	allMemes, _ := memes.Visible(client.DB)

	allReqs := []map[string]interface{}{}

	for _, meme := range allMemes {
		settings := generateTransformFilterSettingsFromList(desiredSettings)
		reqs := transformFunc(client, meme, settings)
		allReqs = append(allReqs, reqs...)
	}

	BatchRequest(client, allReqs)
}

func RandomEffectOnMemes(
	client *OBSClient,
	allMemes []*memes.Meme,
	moveSource bool,
) {

	allReqs := []map[string]interface{}{}

	for _, meme := range allMemes {
		rand.Seed(time.Now().UnixNano())
		reqs := RandomTransformRequestRaw(client, *meme, moveSource)
		allReqs = append(allReqs, reqs...)
	}

	BatchRequest(client, allReqs)
}

func UpdateViewerMoveDuration(client *OBSClient) {
	allMemes, _ := memes.Visible(client.DB)

	for _, meme := range allMemes {
		fmt.Printf("meme = %+v\n", meme)

		settings, err := FetchFilterSettings(client, meme.Name, "viewer_move")
		if err == nil {
			settings["duration"] = 10000
			SetSourceFilterSettings(client, meme.Name, "viewer_move", settings)
		}
	}
}

// This is inter
func RandomEffectOnVisibleMemes(
	client *OBSClient,
	moveSource bool,
) {

	allMemes, _ := memes.Visible(client.DB)

	allReqs := []map[string]interface{}{}

	for _, meme := range allMemes {
		rand.Seed(time.Now().UnixNano())
		reqs := RandomTransformRequestRaw(client, *meme, moveSource)
		allReqs = append(allReqs, reqs...)
	}

	BatchRequest(client, allReqs)
}

// CloneThang takes a source and clones it in the scene
func CloneThang(
	client *OBSClient,
	sceneName string,
	baseSource string,
	count int,
) {

	baseName := baseSource

	var wg sync.WaitGroup
	wg.Add(count)

	x := 0.0

	for i := 1; i < count+1; i++ {
		go func(c *OBSClient, w *sync.WaitGroup, num int, xValue *float64) {
			sourceKind := "streamfx-source-mirror"
			newSourceName := fmt.Sprintf("%s-%d", baseName, num)

			settings := map[string]interface{}{
				"Source.Mirror.Source": baseName,
			}

			CreateSource(c, sceneName, newSourceName, sourceKind, settings)
			AddOutlineFilter(c, sceneName, newSourceName)
			AddTransformFilter(c, sceneName, newSourceName)
			AddNormieTransformFilter(c, sceneName, newSourceName)
			// This timer is too long!
			<-time.NewTimer(time.Second * 2).C
			UpdateSourceSettings(c, newSourceName, sourceKind, settings)

			slideSettings := map[string]interface{}{
				"start_x": 0.0,
				"end_x":   1365.0,
				"y":       *xValue,
			}
			SlideSource(client, sceneName, newSourceName, slideSettings)

			*xValue += 50.0
			wg.Done()
		}(client, &wg, i, &x)

	}
	wg.Wait()
}

func zoomseal(client *OBSClient) {
	toggleTransform(client, "BeginCam", true)
	toggleTransform(client, "Screen", true)
	toggleTransform(client, "Chat", true)
	ToggleSource(client, PrimaryScene, "sealspin", true)
	ZoomAndSpin(client, "Screen", 5)
	ZoomAndSpin(client, "Chat", 5)
	ZoomAndSpin(client, "BeginCam", 5)
	PulsatingHighlight(client, "sealspin", 50, 7, true)
	ZoomAndSpin(client, "sealspin", 4)
}

func beginboy(client *OBSClient) {
	ToggleSource(client, MemeScene, "beginboy", true)
	beginboyRiseSettings := map[string]interface{}{
		"start_y": 750.0,
		"end_y":   300.0,
		"x":       0.0,
	}
	RiseSource(client, MemeScene, "beginboy", beginboyRiseSettings)
	ToggleSource(client, MemeScene, "artmattcardflat", true)
	ZoomFieldOfView(client, "artmattcardflat")
	ToggleSource(client, MemeScene, "artmattfloppycard", true)
	AppearFromThinAir(client, "artmattfloppycard")
	ToggleSource(client, PrimaryScene, "JesterCode", true)
	PulsatingHighlight(client, "JesterCode", 50, 7, true)
	<-time.NewTimer(time.Second * 5).C
	ReturnToNormie(client)
}
