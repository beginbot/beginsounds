package obs

import (
	"encoding/json"
	"fmt"
	"strings"

	obsws "github.com/davidbegin/go-obs-websocket"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

var bottomRightX = 1100.0
var bottomRightY = 600.0
var topRightX = 1150.0
var topRightY = 0.0
var topLeftX = 0.0
var topLeftY = 0.0
var bottomLeftX = 0.0
var bottomLeftY = 600.0

// 0 200 - fall 200
func MoveXAndYRelativeWithFilter(
	client *OBSClient,
	meme *memes.Meme,
	xOffset float64,
	yOffset float64,
) (map[string]interface{}, map[string]interface{}) {

	// homeFilterName := fmt.Sprintf("MoveSourceHome%s", strings.Title(meme.Name))
	filterName := fmt.Sprintf("MoveSource%s", strings.Title(meme.Name))
	filterSettings, _ := FetchFilterSettings(
		client,
		"viewer_memes",
		filterName,
		// homeFilterName,
	)

	var result MoveSourceSettings
	err := mapstructure.Decode(filterSettings, &result)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	transformText := ParseRawTransformText(result.RawTransformText)

	// result.Duration = 1000

	// Fall 200
	result.Pos.Y = result.Pos.Y - yOffset

	// Move Left 200
	result.Pos.X = result.Pos.X - xOffset

	// Not Sure
	// result.Scale.X = 1
	// result.Scale.Y = 1

	inrec, _ := json.Marshal(result)
	var inInterface map[string]interface{}
	json.Unmarshal(inrec, &inInterface)
	inInterface["transform_text"] = transformText.String()

	fmt.Printf("\nsinterfacse5 = %+v\n", inInterface)

	updateFilterReq := obsws.NewSetSourceFilterSettingsRequest(meme.Scene, filterName, inInterface)
	req1Map := requestToMap(&updateFilterReq)
	showFilterReq := obsws.NewSetSourceFilterVisibilityRequest(meme.Scene, filterName, true)
	req2Map := requestToMap(&showFilterReq)
	return req1Map, req2Map
}

func MoveXAndYWithFilter(
	client *OBSClient,
	meme *memes.Meme,
	x float64,
	y float64,
	// ) {
	// ) map[string]interface{} {
) (map[string]interface{}, map[string]interface{}) {

	// This is executed right away
	homeFilterName := fmt.Sprintf("MoveSourceHome%s", strings.Title(meme.Name))
	filterName := fmt.Sprintf("MoveSource%s", strings.Title(meme.Name))
	filterSettings, _ := FetchFilterSettings(
		client,
		"viewer_memes",
		homeFilterName,
		// filterName,
	)

	var result MoveSourceSettings

	err := mapstructure.Decode(filterSettings, &result)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	transformText := ParseRawTransformText(result.RawTransformText)

	result.Pos.X = x
	result.Pos.Y = y
	result.Duration = 1000
	result.Rot = 720

	inrec, _ := json.Marshal(result)
	var inInterface map[string]interface{}
	json.Unmarshal(inrec, &inInterface)
	inInterface["transform_text"] = transformText.String()

	fmt.Printf("\tfilterSettings -- %+v\n", filterSettings)

	updateFilterReq := obsws.NewSetSourceFilterSettingsRequest(meme.Scene, filterName, inInterface)
	req1Map := requestToMap(&updateFilterReq)
	showFilterReq := obsws.NewSetSourceFilterVisibilityRequest(meme.Scene, filterName, true)
	req2Map := requestToMap(&showFilterReq)

	return req1Map, req2Map
}

func CallMoveSourceOnMemes(client *OBSClient, settingsBuilder moveSourceSettingsBuilder) {
	reqs := []map[string]interface{}{}

	ms, _ := memes.Visible(client.DB)
	duration := 5000

	for _, meme := range ms {
		settings := settingsBuilder(client, meme, duration)

		filterSettings := settings.ToMap()

		filterName := fmt.Sprintf("MoveSource%s", strings.Title(meme.Name))

		updateFilterReq := obsws.NewSetSourceFilterSettingsRequest(meme.Scene, filterName, filterSettings)
		showFilterReq := obsws.NewSetSourceFilterVisibilityRequest(meme.Scene, filterName, true)

		req1Map := requestToMap(&updateFilterReq)
		req2Map := requestToMap(&showFilterReq)

		reqs = append(reqs, req1Map)
		reqs = append(reqs, req2Map)
	}

	BatchRequest(client, reqs)
}
