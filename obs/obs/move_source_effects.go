package obs

import (
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

func memeTriforce(client *OBSClient) {
	reqs := []map[string]interface{}{}
	ms, _ := memes.Visible(client.DB)

	blx := bottomLeftX
	bly := bottomLeftY
	brx := bottomRightX
	bry := bottomRightY

	for i, meme := range ms {
		if i%2 == 0 {
			r1, r2 := MoveXAndYWithFilter(client, meme, blx, bly)
			reqs = append(reqs, r1)
			reqs = append(reqs, r2)
			blx = blx + 50
			bly = bly - 50
		} else {
			r1, r2 := MoveXAndYWithFilter(client, meme, brx, bry)
			reqs = append(reqs, r1)
			reqs = append(reqs, r2)
			brx = brx - 50
			bry = bry - 50
		}
	}
	BatchRequest(client, reqs)
}

func splitMemes(client *OBSClient) {
	reqs := []map[string]interface{}{}
	ms, _ := memes.Visible(client.DB)

	for i, meme := range ms {
		if i%2 == 0 {
			r1, r2 := MoveXAndYWithFilter(client, meme, bottomLeftX, bottomLeftY)
			reqs = append(reqs, r1)
			reqs = append(reqs, r2)
		} else {
			r1, r2 := MoveXAndYWithFilter(client, meme, bottomRightX, bottomRightY)
			reqs = append(reqs, r1)
			reqs = append(reqs, r2)
		}
	}

	fmt.Println("sHello4")
	BatchRequest(client, reqs)
}
