package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strings"
	"sync"
	"time"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/obs/obs"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gitlab.com/beginbot/beginsounds/pkg/router"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gitlab.com/beginbot/beginsounds/pkg/tee"
	"gorm.io/gorm"
)

func obsRequestsFromDB(db *gorm.DB) <-chan chat.ChatMessage {
	messages := make(chan chat.ChatMessage)

	go func() {
		defer close(messages)

		ticker := time.NewTicker(1 * time.Second)
		for {
			chatMsgs := obs.UnfufilledRequests(db)
			for _, m := range chatMsgs {
				messages <- m
			}
			<-ticker.C
		}
	}()
	return messages
}

// main is the main loop processing players messages
//   to "troll" Begin and trigger various OBS effects with Websockets
func main() {
	db := database.CreateDBConn("beginsounds4")
	commands := router.GimmieTheCommands(db)
	explicitOBSRequests := obsRequestsFromDB(db)

	results := make(chan string, 10000)

	go func() {
		fmt.Println("We got here")
		defer close(results)

		logger := log.New(ioutil.Discard, "obsws3 ", log.LstdFlags)
		c := obsws.Client{Host: "localhost", Port: 4444, Logger: logger}
		if err := c.Connect(); err != nil {
			log.Print(err)
		}
		defer c.Disconnect()

		var mutex = &sync.Mutex{}
		f, err := os.OpenFile(
			"tmp/obs_errors.txt",
			os.O_CREATE|os.O_APPEND|os.O_WRONLY,
			0644,
		)
		if err != nil {
			log.Fatal(err)
		}
		errLogger := log.New(f, "Error: ", log.LstdFlags)

		client := obs.OBSClient{
			Client: &c,
			Mutex:  mutex,
			Logger: errLogger,
			DB:     db,
		}

		// This allows command chaining
		// Lets Fan in
		msgs := tee.ChatFanIn(commands, explicitOBSRequests)
		explodedCommands := obs.ChatExploder(msgs)

		sourceFilterChannels := make(map[string](chan obs.Command))

	Loop:
		for msg := range explodedCommands {

			// Do we need to call this on every loop?
			rand.Seed(time.Now().UnixNano())

			p := obs.FindPlayer(db, msg)
			if p.ID == 0 {
				fmt.Println("Didn't find a Didn't find a playerplayer")
				continue Loop
			}

			if p.InJail {
				// TODO: figure out how to first check if the jailed player
				// is actually attempting to trigger an OBS
				// results <- fmt.Sprintf("@%s is in Jail. No OBS for you\n", p.Name)
				continue Loop
			}

			// We Will need to use this mana one day again
			// mana := p.Mana

			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0]))

			// We allow everyone to return Begin to Normie
			if cmd == "!normie" {
				obs.ReturnToNormie(&client)
				obs.ResetTransform(&client, "Alerts")
				obs.ResetTransform(&client, "keyboard")
				obs.ResetTransform(&client, "Chat")
				continue Loop
			}

			// TODO: Clean this logic up
			// Check if the Jester is current player
			// or if we are in Chaos Mode
			jester, _ := stream_jester.CurrentJester(db)
			if !jester.ChaosMode && !msg.Streamgod {
				if jester.Secret == cmd[1:] && jester.PlayerID == 0 {
					db.Model(&jester).Update("player_id", msg.PlayerID)
					results <- fmt.Sprintf("New Jester! @%s", msg.PlayerName)
					waitForJester := time.NewTimer(1 * time.Second)
					<-waitForJester.C
					obs.ChangeScene(&client, "jester_scene")
					continue Loop
				}
				if jester == nil || jester.PlayerID == 0 || jester.PlayerID != p.ID {
					continue Loop
				}
			}

			// fmt.Println("Pass the Jester/Streamgod Check")

			if cmd == "!passthejester" {
				waitForJester := time.NewTimer(500 * time.Millisecond)
				<-waitForJester.C
				obs.ChangeScene(&client, "jester_scene")
				continue Loop
			}

			// This is for generic commands on sources
			resultsChannels := make(map[string](<-chan string))
			obsCmd := obs.FindSourceAndScene(msg)

			// I could pass in the user to settingsBuilder
			// Downsides:
			//  - We are now mixing the checking of permissions
			//  - With the setting of Settings
			// We could do the permission check afterwords
			// Which maybe be less efficient
			settings, defaultMeme, currentMeme, _ := obs.SettingsBuilder(&client, cmd, obsCmd)
			if defaultMeme.ID != 0 && !defaultMeme.Enabled {
				fmt.Printf("\n\t\t ### OBS Effect Disabled: %+v", obsCmd)
				continue Loop
			}
			obsCmd.Settings = settings
			obsCmd.DefaultMeme = defaultMeme
			obsCmd.CurrentMeme = currentMeme
			obsCmd.Scene = defaultMeme.Scene

			for _, filter := range obsCmd.Filters {
				chanKey := fmt.Sprintf("%s+%s", obsCmd.Source, filter)
				_, ok := sourceFilterChannels[chanKey]
				if !ok {
					channel := make(chan obs.Command)
					sourceFilterChannels[chanKey] = channel
					results := obs.ProcessObsCommands(&client, channel)
					resultsChannels[chanKey] = results
				}
			}

			for _, filter := range obsCmd.Filters {
				chanKey := fmt.Sprintf("%s+%s", obsCmd.Source, filter)
				channel, ok := sourceFilterChannels[chanKey]
				f, funcFound := obs.CmdFuncMap[cmd]
				if ok && funcFound {
					obsCmd.Func = f
					channel <- obsCmd
				}
			}

			obsEffectFuncs := []obs.TransformFuncCollection{
				obs.ProcessNormalizeRequests,
				obs.UserEffects,
				obs.BeginEffects,
				obs.FilterEffects,
				obs.ProcessHelpers,
				obs.ProcessEpicEffects,
			}

			var cost int
			for _, f := range obsEffectFuncs {
				triggered, _ := f(&client, cmd, obsCmd, results)
				if triggered {
					cm, err := memes.FindCurrent(db, obsCmd.Source)
					if err != nil {
						fmt.Printf("err Finding Current %+v\n", err)
					}
					err = cm.SaveSettings(db, obsCmd.Settings)
					if err != nil {
						fmt.Printf("err Save Current Settings %+v\n", err)
					}
					// How can we save the settings at this point
					// without querying OBS
					cost++
				}
			}

			tsc, ok := obs.ToggleSources[cmd]
			if ok {
				cost++

				obs.ToggleSource(
					&client,
					tsc.Scene,
					tsc.Source,
					tsc.Toggle,
				)
			}

			type obsSourceFunc = func(
				*obs.OBSClient,
				*gorm.DB,
				string,
				obs.Command,
				<-chan string,
			) (bool, error)
			obsSourceFuncs := []obsSourceFunc{
				obs.MemeSaver,
				obs.ProcessCreateSourcesRequests,
			}
			for _, f := range obsSourceFuncs {
				triggered, _ := f(&client, db, cmd, obsCmd, results)
				// m, _ := memes.Find(client.DB, obsCmd.Source)

				switch cmd {
				case "!create_image_source", "!create_video_source":

				default:
					if obsCmd.CurrentMeme.ID != 0 {
						err := obsCmd.CurrentMeme.SaveSettings(client.DB, obsCmd.Settings)
						if err != nil {
							fmt.Printf("\t -- Error Saving Current Settings: = %+v\n", err)
						}
					}

					if triggered {
						cost++
					}
				}

			}

			if jester.ChaosMode && !msg.Streamgod && cost > 0 {
				// newMana := p.Mana - cost
				// p.UpdateMana(db, newMana)
				p.UpdateMana(db, 0)
			}

		}

	}()

	obsws.SetReceiveTimeout(time.Second * 2)

	// return results
	for {
		select {
		case <-context.TODO().Done():
		default:
		}
	}
}
