package webhooks

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/davidbegin/helix"
	"gitlab.com/beginbot/beginsounds/obs/obs"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/forpeople"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/rcon"
	"gorm.io/gorm"
)

type TwitchToRcon struct {
	RconClient *rcon.RemoteConsole
	DB         *gorm.DB
}

// What else do we want????
// What do we want???
var factorioRconCommands = map[string]string{
	"Nuclear Boots":             "!nuclear",
	"Fire Mode":                 "!firemode",
	"Create Biters":             "!biters",
	"I'm an Oil Man":            "!oilmode",
	"Control Player for 5 Mins": "!nothing",
}

var OBSCommands = map[string]string{
	"Wide Begin": "!wide",
}

// local x = "for x=1, 5 do :" .. ":hahabegin:" .. ": print(x .. ' + 1 = ?'); go" .. "to hahabegin end"; _G['load' .. 'string'](x)()
// for element in function() return 1 end do print("Iterate me papa") end
var nastyCmds = []string{
	"repeat",
	"while",
	"huge",
	"redo",
	"goto",
	"loadstring",
	"_G",
}

func isNastyCode(rawLua string) bool {
	var isNasty bool
	for _, cmd := range nastyCmds {
		if strings.Contains(rawLua, cmd) {
			return true
		}
	}
	return isNasty
}

// ================================ //
// Viewers Interacting With Rewards //
// ================================ //

func (client *TwitchToRcon) RedeemRewardEvent(w http.ResponseWriter, r *http.Request) {
	s, err := VerifyEvent(w, r)
	fmt.Printf("\t\tevent = %+v\n", string(s))

	if err != nil {
		return
	}

	var event helix.EventSubChannelPointsCustomRewardRedemptionEvent
	// fmt.Printf("event = %+v\n", event)
	json.NewDecoder(bytes.NewReader(s)).Decode(&event)
	playerName := strings.ToLower(event.UserName)
	p := player.Find(client.DB, playerName)

	switch event.Reward.Title {
	case "Squad Down":
		fmt.Println("Squad Down!")
		client.RconClient.Write("!destroysquad")
	case "Squad Up":
		client.RconClient.Write("!squad")
	case "Nuke Time":
		client.RconClient.Write("!nuke")
	case "Raw Lua":
		rawLua := strings.TrimSpace(event.UserInput)
		fmt.Printf("rawLua = %+v\n", rawLua)
		nastyCode := isNastyCode(rawLua)
		if nastyCode {
			return
		}

		playerInit := "/c local player = game.get_player(1)"
		cmd := fmt.Sprintf("%s; %s", playerInit, rawLua)
		_, err := client.RconClient.Write(cmd)
		if err != nil {
			remoteConsole, _ := rcon.Dial("127.0.0.1:27015", "password")
			client.RconClient = remoteConsole
			_, _ = client.RconClient.Write(cmd)
		}
		return
	case "Wide Begin":
		cm := chat.ChatMessage{
			Message:    "!widebegin",
			PlayerID:   p.ID,
			PlayerName: p.Name,
		}
		req := obs.OBSRequest{
			ChatMessageID: cm.ID,
		}
		tx := client.DB.Save(&req)
		if tx.Error != nil {
			fmt.Printf("Error Saving OBS Request %+v", tx.Error)
		}
		return
	case "Control Player for 5 Mins":
		_, err := forpeople.SetCurrentForperson(client.DB, p, 5)
		if err != nil {
			fmt.Printf("Error Creating New Forperson = +%v\n", err)
		}
		return
	}

	cmd, _ := factorioRconCommands[event.Reward.Title]

	// Check if you are the current Forperson
	fp, err := forpeople.GetCurrentForperson(client.DB)
	if err != nil {
		fmt.Printf("err For GetCurrentForPerson = %+v\n", err)
	}
	if fp != nil {
		fmt.Printf("\n fp.PlayerName = %+v\n", fp.PlayerName)

		if fp.PlayerName != p.Name {
			log.Printf(
				"%s IS NOT THE FORPERSON: %s\n",
				fp.PlayerName,
				p.Name,
			)
			// Send a message back
			return
		}
	}

	client.RconClient.Write(cmd)

	log.Printf(
		"%s Reedemed %s %s for Cost: %d BeginBux\n",
		event.UserName,
		event.Reward.ID,
		event.Reward.Title,
		event.Reward.Cost,
	)
	w.Write([]byte("ok"))
}

func (client *TwitchToRcon) CheckoffRewardEvent(w http.ResponseWriter, r *http.Request) {
	s, err := VerifyEvent(w, r)
	if err != nil {
		return
	}
	var event helix.EventSubChannelPointsCustomRewardRedemptionEvent
	json.NewDecoder(bytes.NewReader(s)).Decode(&event)

	log.Printf(
		"Reward Completed!!! %s - %s - Cost: %d | %s\n",
		event.Reward.Title,
		event.Reward.Prompt,
		event.Reward.Cost,
		event.UserInput,
	)
	w.Write([]byte("ok"))
}

// ======================= //
// Editing Channel Rewards //
// ======================= //

func (client *TwitchToRcon) UpdateRewardEvent(w http.ResponseWriter, r *http.Request) {
	s, err := VerifyEvent(w, r)
	if err != nil {
		return
	}
	var event helix.EventSubChannelPointsCustomRewardEvent
	json.NewDecoder(bytes.NewReader(s)).Decode(&event)

	// cmd := "!mine"
	// client.RconClient.Write(cmd)

	log.Printf("Channel Reward Edited %s %s - %s - Cost: %d\n", event.Title, event.ID, event.Prompt, event.Cost)
	w.Write([]byte("ok"))
}

func (client *TwitchToRcon) AddRewardEvent(w http.ResponseWriter, r *http.Request) {
	s, err := VerifyEvent(w, r)
	if err != nil {
		return
	}
	var event helix.EventSubChannelPointsCustomRewardEvent
	json.NewDecoder(bytes.NewReader(s)).Decode(&event)

	log.Printf("Channel Reward Created %s - %s - Cost: %d\n", event.Title, event.Prompt, event.Cost)
	w.Write([]byte("ok"))
}

func (client *TwitchToRcon) RemoveRewardEvent(w http.ResponseWriter, r *http.Request) {
	s, err := VerifyEvent(w, r)
	if err != nil {
		return
	}
	var event helix.EventSubChannelPointsCustomRewardEvent
	json.NewDecoder(bytes.NewReader(s)).Decode(&event)

	log.Printf("Remove Reward Created %s - %s - Cost: %d\n", event.Title, event.Prompt, event.Cost)
	w.Write([]byte("ok"))
}
