package webhooks

import (
	"fmt"

	"github.com/davidbegin/helix"
)

func RegisterCallback(client *helix.Client, ngrokURL string, eventType string, path string) {
	callbackURL := fmt.Sprintf("%s/%s", ngrokURL, path)

	resp, err := client.CreateEventSubSubscription(&helix.EventSubSubscription{
		Version: "1",
		Type:    eventType,
		Condition: helix.EventSubCondition{
			BroadcasterUserID: beginbotID,
		},
		Transport: helix.EventSubTransport{
			Method:   "webhook",
			Callback: callbackURL,
			Secret:   secret,
		},
	})
	if err != nil {
		// handle error
	}

	fmt.Printf("%+v\n", resp)
}
