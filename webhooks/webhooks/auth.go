package webhooks

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/davidbegin/helix"
	"golang.org/x/oauth2/clientcredentials"
	"golang.org/x/oauth2/twitch"
)

var beginbotID = "424038378"
var secret = os.Getenv("TWITCH_WEBHOOK_SECRET")
var clientID = os.Getenv("TWITCH_CLIENT_ID")
var clientSecret = os.Getenv("TWITCH_CLIENT_SECRET")

type eventSubNotification struct {
	Subscription helix.EventSubSubscription `json:"subscription"`
	Challenge    string                     `json:"challenge"`
	Event        json.RawMessage            `json:"event"`
}

// We need to create the object that holds the info we need

// RCON
// Twitch Client

func CreateTwitchClient() *helix.Client {
	var oauth2Config *clientcredentials.Config
	oauth2Config = &clientcredentials.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		TokenURL:     twitch.Endpoint.TokenURL,
	}

	token, err := oauth2Config.Token(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	client, err := helix.NewClient(&helix.Options{
		ClientID:       clientID,
		AppAccessToken: token.AccessToken,
	})
	if err != nil {
	}

	client2, err := helix.NewClient(&helix.Options{
		ClientID:    clientID,
		RedirectURI: "http://localhost:1987",
	})
	if err != nil {
	}

	// Figure out what we want to print off
	_ = client2.GetAuthorizationURL(&helix.AuthorizationURLParams{
		ResponseType: "code", // or "token"
		Scopes:       []string{"channel:manage:redemptions", "channel:read:subscriptions", "channel:moderate"},
		// WE don't need this
		State: "some-state",
		// This is key
		ForceVerify: true,
	})

	// Print off status
	// fmt.Printf("url = %+v\n", url)
	return client
}

// Hnmmm
func VerifyEvent(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	var vals eventSubNotification
	var s []byte

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		return s, err
	}
	defer r.Body.Close()
	// verify that the notification came from twitch using the secret.
	if !helix.VerifyEventSubNotification(secret, r.Header, string(body)) {
		log.Println("no valid signature on subscription")
		return s, err
	}

	// log.Println("verified signature for subscription")
	err = json.NewDecoder(bytes.NewReader(body)).Decode(&vals)
	if err != nil {
		log.Println(err)
		return s, err
	}
	// if there's a challenge in the request, respond with only the challenge to verify your eventsub.
	if vals.Challenge != "" {
		w.Write([]byte(vals.Challenge))
		log.Printf("Verifying Challenge")
		return s, errors.New("Verifying")
	}

	s, _ = vals.Event.MarshalJSON()
	return s, nil
}
