package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"github.com/davidbegin/helix"
	"github.com/gorilla/mux"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/rcon"
	"gitlab.com/beginbot/beginsounds/webhooks/webhooks"
)

var beginbotID = "424038378"
var secret = os.Getenv("TWITCH_WEBHOOK_SECRET")
var clientID = os.Getenv("TWITCH_CLIENT_ID")
var clientSecret = os.Getenv("TWITCH_CLIENT_SECRET")

func main() {
	password := "password"
	host := "127.0.0.1:27015"
	remoteConsole, _ := rcon.Dial(host, password)

	db := database.CreateDBConn("beginsounds4")

	cmd := "/promote beginbot"
	_, _ = remoteConsole.Write(cmd)
	cmd = "/promote begin"
	_, _ = remoteConsole.Write(cmd)

	rconClient := webhooks.TwitchToRcon{
		RconClient: remoteConsole,
		DB:         db,
	}

	ngrokURL := flag.String("ngrok", "INSERT_NGROK_URL", "If the market is open")
	fresh := flag.Bool("fresh", false, "Delete Old Subscriptions")
	flag.Parse()

	var client = webhooks.CreateTwitchClient()
	r := mux.NewRouter()

	if *fresh {
		resp, _ := client.GetEventSubSubscriptions(&helix.EventSubSubscriptionsParams{})
		subs := resp.Data.EventSubSubscriptions
		for _, sub := range subs {
			r, _ := client.RemoveEventSubSubscription(sub.ID)
			fmt.Printf("resp = %+v\n", r)
		}
	}

	webhooks.RegisterCallback(client, *ngrokURL, helix.EventSubTypeChannelPointsCustomRewardRedemptionAdd, "redeem_channel_points")
	r.HandleFunc("/redeem_channel_points", rconClient.RedeemRewardEvent)

	webhooks.RegisterCallback(client, *ngrokURL, helix.EventSubTypeChannelPointsCustomRewardRedemptionUpdate, "checkoff_channel_points")
	r.HandleFunc("/checkoff_channel_points", rconClient.CheckoffRewardEvent)

	webhooks.RegisterCallback(client, *ngrokURL, helix.EventSubTypeChannelPointsCustomRewardUpdate, "update_channel_points")
	r.HandleFunc("/update_channel_points", rconClient.UpdateRewardEvent)

	webhooks.RegisterCallback(client, *ngrokURL, helix.EventSubTypeChannelPointsCustomRewardAdd, "add_channel_points")
	r.HandleFunc("/add_channel_points", rconClient.AddRewardEvent)

	webhooks.RegisterCallback(client, *ngrokURL, helix.EventSubTypeChannelPointsCustomRewardRemove, "remove_channel_points")
	r.HandleFunc("/remove_channel_points", rconClient.RemoveRewardEvent)

	http.ListenAndServe(":1987", r)
}
