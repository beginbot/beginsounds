ALTER TABLE public.memes ADD COLUMN visible boolean DEFAULT false;
ALTER TABLE public.memes ADD COLUMN verified boolean DEFAULT false;
ALTER TABLE public.memes ADD COLUMN scene character varying(255) NOT NULL DEFAULT 'viewer_memes';
