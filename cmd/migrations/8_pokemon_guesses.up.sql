CREATE TABLE public.pokemon_guesses(
    id SERIAL,
    pokemon character varying(255) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    player_id int references players(id),
    pokemon_answer_id int references pokemon_answers(id),
    CONSTRAINT pokemon_guesses_pkey PRIMARY KEY (id)
);
