CREATE TABLE public.packs (
    id SERIAL,
    name character varying(255) NOT NULL UNIQUE,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT packs_pkey PRIMARY KEY (id)
);
