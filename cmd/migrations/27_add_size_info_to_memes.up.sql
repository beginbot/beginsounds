ALTER TABLE public.memes ADD COLUMN width smallint;
ALTER TABLE public.memes ADD COLUMN height smallint;
CREATE TYPE meme_type AS ENUM ('image', 'video', 'gif');
ALTER TABLE public.memes ADD COLUMN meme_type meme_type;
ALTER TABLE public.memes ADD COLUMN filename character varying(255);
