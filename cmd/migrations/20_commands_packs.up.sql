CREATE TABLE public.commands_packs (
    id SERIAL,

    command_id int references stream_commands(id),
    pack_id int references packs(id),

    created_at timestamp with time zone DEFAULT now() NOT NULL,

    CONSTRAINT commands_packs_unique UNIQUE(pack_id,command_id),
    CONSTRAINT commands_packs_pkey PRIMARY KEY (id)
);
