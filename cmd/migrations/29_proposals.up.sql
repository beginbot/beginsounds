CREATE TABLE public.proposals (
    id SERIAL,

    command character varying(255) NOT NULL,

    chat_message_id int references chat_messages(id),

    rejected_at timestamp with time zone,
    approved_at timestamp with time zone,
    triggered_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT now() NOT NULL,

    CONSTRAINT proposals_pkey PRIMARY KEY (id)
);
