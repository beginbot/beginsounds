CREATE TABLE public.cube_games (
    id SERIAL,

    solve_time smallint NOT NULL,

    created_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone,
    started_at timestamp with time zone,

    CONSTRAINT cube_games_pkey PRIMARY KEY (id)
);
