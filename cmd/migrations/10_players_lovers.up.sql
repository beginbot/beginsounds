CREATE TABLE public.players_lovers (
    id SERIAL,
    player_id int references players(id),
    lover_id int references players(id),
    CONSTRAINT players_lovers_pkey PRIMARY KEY (id),
    CONSTRAINT players_lovers_unique UNIQUE(player_id,lover_id)
);
