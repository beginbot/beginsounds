CREATE TABLE public.forpeople (
    id SERIAL,

    motto text,

    created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
    deleted_at TIMESTAMP WITH TIME ZONE,
    expires_at TIMESTAMP WITH TIME ZONE,

    player_id int references players(id),
    player_name character varying(255),

    CONSTRAINT forpeople_pkey PRIMARY KEY (id)
);
