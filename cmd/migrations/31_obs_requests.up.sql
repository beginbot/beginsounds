CREATE TABLE public.obs_requests(
    id SERIAL,
    chat_message_id int references chat_messages(id),

    raw_message text NOT NULL,

    failed_at timestamp with time zone,
    triggered_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT now() NOT NULL,

    CONSTRAINT obs_requests_pkey PRIMARY KEY (id)
);
