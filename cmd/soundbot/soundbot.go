package main

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/router"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

func main() {
	db := database.CreateDBConn("beginsounds4")
	ctx := context.Background()

	// Should we pass in the ctx here?
	noMemes := lookForNoMemes(db)

	for {

		select {
		// Will this ever be hit???
		case <-ctx.Done():
			break

		default:
			aqs := audio_request.UnplayedSounds(db)

			for _, audioRequest := range aqs {

				// We fetch how long each player can play a sounds
				// we allow dynamic lengths of sample
				timer := player.FindTimerByID(db, audioRequest.PlayerID)

				// Jesters get special powers
				isJester := isJesterRequest(db, audioRequest.PlayerID)

				var p player.Player
				tx := db.Table("players").Where("ID = ?", audioRequest.PlayerID).Find(&p)
				if tx.Error != nil {
					fmt.Printf("Couldn't find User %v", tx.Error)
					continue
				}

				if !isJester {
					allowed, errMsg := isAllowed(&p)

					if !allowed {
						fmt.Println(errMsg)

						// If you aren't allowed to play a sound
						// we mark it as complete
						//
						// We potentially want to save in the DB, that this didn't
						// actually play
						db.Model(&audioRequest).Update("played", true)
						continue
					}
				}

				// We are not going to trigger pack effects
				// for soundeffects right now
				// TODO: make this work, better than ever
				// packTalk(db, audioRequest)

				fmt.Printf("@%s Attempting to play Sound: %s\n", p.Name, audioRequest.Name)

				// We always mark as true, right before attempt
				db.Model(&audioRequest).Update("played", true)

				success := soundboard.PlayAudio(*audioRequest, timer, noMemes)

				if !success {
					fmt.Printf("%s didn't play successfully\n", audioRequest.Name)
					continue
				}

				if !isJester {
					subtractMana(db, &p, audioRequest)
				}

			}

			<-time.NewTimer(time.Millisecond * 100).C
		}
	}
}

func subtractMana(
	db *gorm.DB,
	p *player.Player,
	audioRequest *audio_request.AudioRequest,
) {

	if p.Streamgod {
		return
	}

	if audioRequest.Free {
		return
	}

	tx := db.Exec(
		"UPDATE players SET mana = mana - 1 WHERE ID = ?",
		audioRequest.PlayerID,
	)

	if tx.Error != nil {
		fmt.Printf("Error Updating Mana: %+v\n", tx.Error)
	}
}

func packTalk(db *gorm.DB, audioRequest *audio_request.AudioRequest) {
	packEffects := map[string][]string{
		"teej":     {"!teej1", "!teej2"},
		"prime":    {"!primeagen"},
		"melkey":   {"!melkman2", "!melkman"},
		"roxkstar": {"!roxkstar"},
		"dean":     {"!peak dean"},
	}

	potentialPack := pack.EffectForPackSound(db, audioRequest.Name)
	// How do I check if a key is in dah map
	if potentialPack != "" {
		obsCommands, ok := packEffects[potentialPack]

		if ok {
			i := rand.Intn(len(obsCommands))
			fmt.Printf("\t == Choosing OBS Index! %+v\n", i)
			obsCommand := obsCommands[i]
			fmt.Printf("\t == Choosing OBS Command! %+v\n", obsCommand)
		}
	}
}

func isJesterRequest(db *gorm.DB, playerID int) bool {
	var isJester bool
	j, _ := stream_jester.CurrentJester(db)
	if j.PlayerID == 0 {
		isJester = false
	} else {
		isJester = playerID == j.PlayerID
	}
	return isJester
}

func isAllowed(p *player.Player) (bool, string) {
	if !p.Streamgod {

		if p.Mana < 1 {
			return false, fmt.Sprintf("%s doesn't have any Mana", p.Name)
		}

		if p.InJail {
			return false, fmt.Sprintf("%s is in Jail", p.Name)
		}

	}
	return true, ""
}

func lookForNoMemes(db *gorm.DB) <-chan bool {
	commands := router.GimmieTheCommands(db)
	noMemes := make(chan bool, 1)

	go func() {
		defer close(noMemes)

		for msg := range commands {
			name := msg.ParsedCmd.Name

			if (msg.Streamlord || msg.Streamgod) &&
				(name == "silence" || name == "nomeme") {
				fmt.Println("Adding to noMemes Channel")

				noMemes <- true
			}
		}

	}()

	return noMemes
}
