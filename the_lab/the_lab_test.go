package main

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/obs/obs"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

var pokemonChoices = [151]string{"bulbasaur", "ivysaur", "venusaur", "charmander", "charmeleon", "charizard", "squirtle", "wartortle", "blastoise", "caterpie", "metapod", "butterfree", "weedle", "kakuna", "beedrill", "pidgey", "pidgeotto", "pidgeot", "rattata", "raticate", "spearow", "fearow", "ekans", "arbok", "pikachu", "raichu", "sandshrew", "sandslash", "nidoran-f", "nidorina", "nidoqueen", "nidoran-m", "nidorino", "nidoking", "clefairy", "clefable", "vulpix", "ninetales", "jigglypuff", "wigglytuff", "zubat", "golbat", "oddish", "gloom", "vileplume", "paras", "parasect", "venonat", "venomoth", "diglett", "dugtrio", "meowth", "persian", "psyduck", "golduck", "mankey", "primeape", "growlithe", "arcanine", "poliwag", "poliwhirl", "poliwrath", "abra", "kadabra", "alakazam", "machop", "machoke", "machamp", "bellsprout", "weepinbell", "victreebel", "tentacool", "tentacruel", "geodude", "graveler", "golem", "ponyta", "rapidash", "slowpoke", "slowbro", "magnemite", "magneton", "farfetchd", "doduo", "dodrio", "seel", "dewgong", "grimer", "muk", "shellder", "cloyster", "gastly", "haunter", "gengar", "onix", "drowzee", "hypno", "krabby", "kingler", "voltorb", "electrode", "exeggcute", "exeggutor", "cubone", "marowak", "hitmonlee", "hitmonchan", "lickitung", "koffing", "weezing", "rhyhorn", "rhydon", "chansey", "tangela", "kangaskhan", "horsea", "seadra", "goldeen", "seaking", "staryu", "starmie", "mr.mime", "scyther", "jynx", "electabuzz", "magmar", "pinsir", "tauros", "magikarp", "gyarados", "lapras", "ditto", "eevee", "vaporeon", "jolteon", "flareon", "porygon", "omanyte", "omastar", "kabuto", "kabutops", "aerodactyl", "snorlax", "articuno", "zapdos", "moltres", "dratini", "dragonair", "dragonite", "mewtwo", "mew"}

func NotTestCreateMeme(t *testing.T) {
	client := obs.TestingClient()
	defer client.Client.Disconnect()

	// scene := PokemonScene

	// baseURL := "https://www.pkparaiso.com/imagenes/xy/sprites/animados"
	// beginbot := player.Find(client.DB, "beginbot")

	for _, pokemon := range pokemonChoices {
		meme, _ := memes.FindDefault(client.DB, pokemon)
		if meme.Scene == "" {
			fmt.Printf("meme.Name = %+v\n", meme.Name)
		}

		// meme.Scene = obs.PokemonScene
		// err := client.DB.Where("id = ?", meme.ID).Save(&meme)
		// if err != nil {
		// 	fmt.Printf("err = %+v\n", err)
		// }

		// // if meme.Name == "" {
		// fmt.Printf("meme = %+v\n", pokemon)
		// url := fmt.Sprintf("%s/%s.gif", baseURL, pokemon)

		// meme, _ := memes.FindDefault(client.DB, pokemon)
		// meme.Scene = obs.PokemonScene
		// meme.MemeType = "video"
		// err := client.DB.Where("id = ?", meme.ID).Save(&meme)
		// if err != nil {
		// 	fmt.Printf("err = %+v\n", err)
		// }

		// meme2, _ := memes.FindCurrent(client.DB, pokemon)
		// meme2.MemeType = "video"
		// meme2.Scene = obs.PokemonScene
		// client.DB.Where("id = ?", meme2.ID).Save(&meme2)

		// return
		// fakeMsg := chat.ChatMessage{
		// 	Streamgod:  true,
		// 	PlayerID:   beginbot.ID,
		// 	PlayerName: beginbot.Name,
		// 	Message:    fmt.Sprintf("!gif %s %s pokemon_scene", url, pokemon),
		// }
		// // fmt.Sprintf("!gif %s %s", url, pokemon)

		// media, _ := media_parser.Parse("video", fakeMsg)
		// // if err != nil {
		// // 	fmt.Printf("Error Parsing = %+v\n", err)
		// // }

		// // fmt.Printf("media = %+v\n", media.Approved)
		// // fmt.Printf("media = %+v\n", media)
		// // tx := client.DB.Create(&media)
		// // if tx.Error != nil {
		// // 	fmt.Printf("tx.Error = %+v\n", tx.Error)
		// // }

		// // meme := memes.Meme{}

		// sourceKind := "video"
		// filename := fmt.Sprintf("/home/begin/stream/Stream/ViewerVideos/%s", media.Filename)
		// obs.CreateMemes(client, pokemon, sourceKind, filename)

		// // obs.NewCreateImageOrVideo(client, obs.PokemonScene, meme)
		// // meme.Verified = true
		// // meme.Scene = scene
		// // client.DB.Save(&meme)
		// // }

		// // fmt.Printf("meme = %+v\n", meme.Name)
		// obs.NewCreateImageOrVideo(client, "pokemon_scene", meme)
		// // meme.Verified = true
		// // meme.Scene = scene
		// // client.DB.Save(&meme)
	}

}

func NoTestCreateMeme(t *testing.T) {
	client := obs.TestingClient()
	defer client.Client.Disconnect()

	scene := obs.PokemonScene

	for _, pokemon := range pokemonChoices {
		currentMeme, _ := memes.FindCurrent(client.DB, pokemon)
		obs.AddFiltersForMeme(client, scene, currentMeme)

		// if currentMeme.ID != 0 {
		// 	return
		// }

		// defaultMeme, _ := memes.FindDefault(client.DB, pokemon)

		// newCurrentMeme := defaultMeme
		// newCurrentMeme.PositionType = "current"
		// err := client.DB.Where("id = ?", newCurrentMeme.ID).Save(&newCurrentMeme)
		// if err != nil {
		// 	fmt.Printf("err = %+v\n", err)
		// }
	}

}

func NoTestFetchSettings(t *testing.T) {
	client := obs.TestingClient()
	defer client.Client.Disconnect()

	// !reset paras
	// !norm paras
	fmt.Println("Tony time 5")
	obs.WinningPokemon(client, "graveler")

}
