package media_parser

import (
	"database/sql"
	"errors"
	"fmt"
	"net/url"
	"path/filepath"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/utils"
)

// Parse parses the msg to save a Media Request
func Parse(
	mediaType string,
	msg chat.ChatMessage,
) (*media_request.MediaRequest, error) {
	var isApproved bool
	if msg.Streamgod || msg.Streamlord {
		isApproved = true
	}

	rID := sql.NullInt32{
		Int32: int32(msg.PlayerID),
		Valid: true,
	}
	request := media_request.MediaRequest{
		MediaType:   mediaType,
		Approved:    isApproved,
		Requester:   msg.PlayerName,
		RequesterID: rID,
	}

	// !gif URL name [SCENE]
	parts := strings.Split(msg.Message, " ")

	if len(parts) < 2 {
		return &request, errors.New("Missing parts")
	}

	if len(parts) > 3 {
		scene := parts[3]
		request.Scene = scene
	}

	_, err := url.ParseRequestURI(parts[1])
	if err != nil {
		em := fmt.Sprintf("@%s passed invalid URL %+v\n", msg.PlayerName, err)
		return &media_request.MediaRequest{}, errors.New(em)
	}
	request.URL = parts[1]

	// If we don't include a command name
	if len(parts) > 2 {
		for _, part := range parts[2:] {
			if utils.IsTimeStamp(part) && request.StartTime == "" {
				request.StartTime = part
			} else if utils.IsTimeStamp(part) {
				request.EndTime = part
			} else if part != request.Scene {
				request.Name = part
			}
		}
	}

	// If we haven't found a command name yet
	// Assume it's the Players Name
	if request.Name == "" {
		request.Name = msg.PlayerName
	}

	validExts := []string{
		".gif",
		".jpg",
		".jpeg",
		".png",
	}

	rawExt := filepath.Ext(request.URL)

	var ext string
	for _, pe := range validExts {
		if strings.Contains(rawExt, pe) {
			ext = pe
		}
	}
	request.Filename = fmt.Sprintf("%s%s", request.Name, ext)
	fmt.Printf("request.Filename = %+v\n", request.Filename)

	return &request, nil
}
