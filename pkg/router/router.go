package router

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/chat_parser"
	"gitlab.com/beginbot/beginsounds/pkg/chat_saver"
	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/filter"
	"gitlab.com/beginbot/beginsounds/pkg/irc"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/tee"
	"gorm.io/gorm"
)

func GimmieTheCommands(db *gorm.DB) <-chan chat.ChatMessage {
	twitchChannel := flag.String("channel", "beginbot", "The main IRC Channel to connect to")
	secretTwitchChannel := flag.String("botchannel", "beginbotbot", "The main IRC Channel to connect to")

	ircConfig := config.NewIrcConfig(*twitchChannel)
	secretIrcConfig := config.NewIrcConfig(*secretTwitchChannel)
	messages := irc.ReadIrc(context.TODO(), ircConfig.Conn)
	secretMsgs := irc.ReadIrc(context.TODO(), secretIrcConfig.Conn)

	beginbotMsgs := SimpleFilterChatMsgs(context.TODO(), db, &ircConfig, messages)
	secretBeginbotMsgs := SimpleFilterChatMsgs(context.TODO(), db, &secretIrcConfig, secretMsgs)

	allMessages := tee.ChatFanIn(beginbotMsgs, secretBeginbotMsgs)
	commands, _ := filter.RouteUserCommands(context.TODO(), db, allMessages)
	return commands
}

func FilterChatMsgs(
	ctx context.Context,
	db *gorm.DB,
	c *config.Config,
	messages <-chan string,
) (
	<-chan chat.ChatMessage,
	<-chan audio_request.AudioRequest,
	<-chan chat.ChatMessage,
) {

	UsersWeDoNotLike := []string{"nightbot"}

	// Why the magic limit of 1000
	userMsgs := make(chan chat.ChatMessage, 1000)
	themeSongs := make(chan audio_request.AudioRequest, 1000)
	obsRequests := make(chan chat.ChatMessage)

	go func() {
		defer close(userMsgs)
		defer close(themeSongs)
		defer close(obsRequests)

		for msg := range messages {
		outside:

			select {
			case <-ctx.Done():
				return
			default:
				// welcomePack, _ := pack.Find(db, "welcome")

				if parser.IsPrivmsg(msg) {
					user, m := parser.ParsePrivmsg(msg)

					fmt.Printf("%s: %s\n", user, m)
					// If this creates a new user
					// We should give them the welcome pack
					p, _ := player.FindOrWelcome(db, user)

					// if newPlayer {
					// 	// TODO come back and add this
					// 	// give them the welcome pack
					// 	// chatMessage <- "@%s Welcome to Beginworld: Welcome Gifts: !hello !clap"
					// 	pack.Give(db, welcomePack.ID, p.ID)
					// }
					chatMessage, _ := chat_parser.Parse(db, p, m)
					themeSong, obsRequest := chat_saver.SaveChatAndTheme(db, &chatMessage)

					if themeSong != nil {
						themeSongs <- *themeSong
					}

					if obsRequest.Message != "" {
						obsRequests <- obsRequest
					}

					for _, u := range UsersWeDoNotLike {
						if u == user {
							break outside
						}
					}

					userMsgs <- chatMessage

				} else if parser.IsPing(msg) {
					irc.Pong(c)
				}

			}
		}
	}()

	return userMsgs, themeSongs, obsRequests
}

func SimpleFilterChatMsgs(
	ctx context.Context,
	db *gorm.DB,
	c *config.Config,
	messages <-chan string,
) <-chan chat.ChatMessage {

	UsersWeDoNotLike := []string{"nightbot"}

	userMsgs := make(chan chat.ChatMessage, 10000)

	go func() {
		defer close(userMsgs)

		for msg := range messages {
		outside:

			select {
			case <-ctx.Done():
				return
			default:
				if parser.IsPrivmsg(msg) {
					user, m := parser.ParsePrivmsg(msg)

					p := player.Find(db, user)
					chatMessage, _ := chat_parser.Parse(db, p, m)

					for _, u := range UsersWeDoNotLike {
						if u == user {
							break outside
						}
					}
					userMsgs <- chatMessage
				} else if parser.IsPing(msg) {
					irc.Pong(c)
				}

			}
		}
	}()

	return userMsgs
}
