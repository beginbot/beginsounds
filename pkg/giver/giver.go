package giver

import (
	"errors"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/permissions"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

func Clone(db *gorm.DB, msg chat.ChatMessage) (string, error) {
	playerID := msg.PlayerID
	playerName := msg.PlayerName
	commandID := msg.ParsedCmd.TargetCommandID
	commandName := msg.ParsedCmd.TargetCommand
	targetUserName := msg.ParsedCmd.TargetUser
	targetUserID := msg.ParsedCmd.TargetUserID

	p := player.FindByID(db, playerID)

	if !permissions.IsAllowed(db, playerID, commandID) {
		return "", fmt.Errorf(
			"@%s can't clone cuz they don't own !%s",
			playerName,
			commandName,
		)
	}

	if permissions.IsAllowed(db, targetUserID, commandID) {
		return "", fmt.Errorf(
			"@%s already owns !%s",
			targetUserName,
			commandName,
		)
	}

	sc := stream_command.FindByID(db, commandID)

	if sc.Cost > p.CoolPoints {
		return "", fmt.Errorf(
			"@%s doesn't have enough cool points to clone !%s",
			playerName,
			sc.Name,
		)
	}

	err := permissions.Allow(db, targetUserID, commandID)
	if err != nil {
		return "", err
	}

	err = sc.ClonePriceIncrease(db)

	if err != nil {
		return "", nil
	}

	return fmt.Sprintf(
		"@%s cloned !%s and gave it to: @%s",
		playerName,
		sc.Name,
		targetUserName,
	), nil
}

func Giver(db *gorm.DB, msg chat.ChatMessage) (string, error) {

	fmt.Printf("Is Allowed: PlayerID: %d CommandID: %d\n", msg.PlayerID, msg.ParsedCmd.TargetCommandID)

	playerOwnsGift := permissions.IsAllowed(db, msg.PlayerID, msg.ParsedCmd.TargetCommandID)
	if !playerOwnsGift {
		return "", errors.New(
			fmt.Sprintf("@%s does NOT own !%s", msg.PlayerName, msg.ParsedCmd.TargetCommand),
		)
	}

	err := permissions.Allow(db, msg.ParsedCmd.TargetUserID, msg.ParsedCmd.TargetCommandID)
	if err != nil {
		return fmt.Sprintf(
			"Erroring @%s givin %s !%s",
			msg.PlayerName,
			msg.ParsedCmd.TargetUser,
			msg.ParsedCmd.TargetCommand,
		), errors.New(fmt.Sprintf("@%s Already owns !%s", msg.ParsedCmd.TargetUser, msg.ParsedCmd.TargetCommand))
	}

	err = permissions.Unallow(db, msg.PlayerID, msg.ParsedCmd.TargetCommandID)

	return fmt.Sprintf(
		"@%s gave %s !%s",
		msg.PlayerName,
		msg.ParsedCmd.TargetUser,
		msg.ParsedCmd.TargetCommand,
	), err
}
