package giver

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/permissions"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestCloner(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	// p1 := player.Player{
	// 	Name:       "young.thug",
	// 	CoolPoints: 1,
	// }
	// db.Create(&p1)
	p1 := player.CreatePlayerFromName(db, "young.thug")
	p2 := player.CreatePlayerFromName(db, "gucci.mane")

	damn := stream_command.CreateFromName(db, "damn")
	noice := stream_command.CreateFromName(db, "noice")
	permissions.Allow(db, p1.ID, noice.ID)
	permissions.Allow(db, p2.ID, noice.ID)

	cm1 := chat.ChatMessage{
		PlayerName: p1.Name,
		PlayerID:   p1.ID,
		Message:    "!clone damn gucci.mane",
		ParsedCmd: chat.ParsedCommand{
			TargetUser:      p2.Name,
			TargetUserID:    p2.ID,
			TargetCommand:   damn.Name,
			TargetCommandID: damn.ID,
		},
		Parts: []string{"!clone", "@gucci.mane", "damn"},
	}
	res, err := Clone(db, cm1)

	fmt.Printf("res = %+v\n", res)

	if err == nil {
		t.Error("We should have gotten an error")
	}
	gucciAllowed := permissions.IsAllowed(db, p2.ID, damn.ID)
	if gucciAllowed {
		t.Error("Gucci should not be allowed damn!")
	}
	thuggaAllowed := permissions.IsAllowed(db, p1.ID, damn.ID)
	if thuggaAllowed {
		t.Error("Thugga should not be allowed damn!")
	}
	sc := stream_command.Find(db, "damn")
	if sc.Cost != 1 {
		t.Errorf("!damn price should not have increased: %d", sc.Cost)
	}

	cm2 := chat.ChatMessage{
		PlayerName: p1.Name,
		PlayerID:   p1.ID,
		Message:    "!clone noice @gucci.mane",
		ParsedCmd: chat.ParsedCommand{
			TargetUser:      p2.Name,
			TargetUserID:    p2.ID,
			TargetCommand:   noice.Name,
			TargetCommandID: noice.ID,
		},
		Parts: []string{"!clone", "@gucci.mane", "noice"},
	}
	_, err = Clone(db, cm2)
	if err == nil {
		t.Errorf("You should not clone sounds already owned by both parties")
	}

	// We Allow Young Thug access to damn to clone it
	permissions.Allow(db, p1.ID, damn.ID)

	res, err = Clone(db, cm1)
	gucciAllowed = permissions.IsAllowed(db, p2.ID, damn.ID)
	if gucciAllowed {
		t.Error("Gucci should not have gotten !damn")
	}
	sc = stream_command.Find(db, "damn")
	if sc.Cost != 1 {
		t.Errorf("!damn price SHOULD not increase: %d", sc.Cost)
	}

	db.Model(&p1).Update("cool_points", 1)
	res, err = Clone(db, cm1)
	gucciAllowed = permissions.IsAllowed(db, p2.ID, damn.ID)
	if !gucciAllowed {
		t.Error("Gucci didn't get !damn from the cloning")
	}
	thuggaAllowed = permissions.IsAllowed(db, p1.ID, damn.ID)
	if !thuggaAllowed {
		t.Error("Thugga did not maintain ownership of !damn from cloning")
	}
	sc = stream_command.Find(db, "damn")
	if sc.Cost != 2 {
		t.Errorf("!damn price did not increase properly to 2: %d", sc.Cost)
	}

	_, err = Clone(db, cm1)
	if err == nil {
		t.Errorf("We should have got an error for giving an already owned sound")
	}
}

func TestGiver(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	yt := player.CreatePlayerFromName(db, "young.thug")
	g := player.CreatePlayerFromName(db, "gunna")
	damn := stream_command.CreateFromName(db, "damn")
	permissions.Allow(db, yt.ID, damn.ID)

	gifterBalancer := g.CoolPoints
	cm := chat.ChatMessage{
		PlayerName: "young.thug",
		PlayerID:   yt.ID,
		Message:    "!give @gunna damn",
		ParsedCmd: chat.ParsedCommand{
			TargetUser:      g.Name,
			TargetUserID:    g.ID,
			TargetCommand:   damn.Name,
			TargetCommandID: damn.ID,
		},
		Parts: []string{"!give", "@gunna", "damn"},
	}

	res, err := Giver(db, cm)
	isAllowed := permissions.IsAllowed(db, g.ID, damn.ID)
	if !isAllowed {
		t.Error(
			fmt.Sprintf(
				"We did not give !damn to @gunna: Response: %s | Error: %+v",
				res,
				err,
			),
		)
	}

	newGifterBalance := player.Find(db, "gunna").CoolPoints
	if gifterBalancer != newGifterBalance {
		t.Errorf("We are changing the Gifters Balance: %d", gifterBalancer-newGifterBalance)
	}

	isAllowed = permissions.IsAllowed(db, yt.ID, damn.ID)
	if isAllowed {
		t.Errorf("%s SHOULD NOT LONGER BE ALLOWED TO PLAY: %s", yt.Name, damn.Name)
	}
	// expectedOutput := "@young.thug gave @gunna !damn",
}

func TestYouCannotGiveWhatYouDoNotOwn(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	yt := player.CreatePlayerFromName(db, "young.thug")
	g := player.CreatePlayerFromName(db, "gunna")
	damn := stream_command.CreateFromName(db, "damn")
	permissions.AllowUserAccessToCommand(db, yt.Name, damn.Name)

	gifterBalancer := g.CoolPoints
	cm := chat.ChatMessage{
		PlayerName: "young.thug",
		Message:    "!give @gunna damn",
		ParsedCmd: chat.ParsedCommand{
			TargetUser:      g.Name,
			TargetUserID:    g.ID,
			TargetCommand:   damn.Name,
			TargetCommandID: damn.ID,
		},
		Parts: []string{"!give", "@gunna", "damn"},
	}

	res, err := Giver(db, cm)
	isAllowed := permissions.IsAllowed(db, g.ID, damn.ID)
	if isAllowed {
		t.Error(
			fmt.Sprintf(
				"We did give !damn to @gunna: Response: %s | Error: %+v",
				res,
				err,
			),
		)
	}

	newGifterBalance := player.Find(db, "gunna").CoolPoints
	if gifterBalancer != newGifterBalance {
		t.Errorf("We are changing the Gifters Balance: %d", gifterBalancer-newGifterBalance)
	}
	// expectedOutput := "@young.thug gave @gunna !damn",
}
