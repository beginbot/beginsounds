package irc

import (
	"bufio"
	"context"
	"io"
)

func ReadIrc(ctx context.Context, reader io.Reader) <-chan string {
	// TODO: figure out if this should be buffered
	messages := make(chan string, 10000)
	newreader := bufio.NewReader(reader)

	go func() {
		defer close(messages)

		// This is a wasteful loop
		// As it is not blocking
		// We have the for {}
		// and we have the default
		for {
			select {
			case <-ctx.Done():
				return
			default:
				res, _ := newreader.ReadString('\n')
				// is this <-res cal blocking
				messages <- res
			}
		}
	}()

	return messages
}
