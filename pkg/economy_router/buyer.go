package economy_router

import (
	"fmt"
	"math/rand"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/sound_store"
	"gorm.io/gorm"
)

func Buyer(
	db *gorm.DB,
	msg chat.ChatMessage,
	targetCommand string,
	targetAmount int,
) (string, error) {

	// TODO: Maybe this number Dynamic
	//       ....or don't because it's not worth it
	commandTotal := 3500
	if targetAmount > commandTotal {
		return "",
			fmt.Errorf("@%s You can't buy more commands than exist: %d",
				msg.PlayerName,
				commandTotal,
			)
	}

	choices := RandomCommand(db, msg.CoolPoints, msg.PlayerID)

	if len(choices) < 1 {
		return "", fmt.Errorf(
			"No sounds found for purchase: @%s. Cool Points: %d",
			msg.PlayerName,
			msg.CoolPoints,
		)
	}

	if targetAmount < 2 {
		if targetCommand != "" {
			result := sound_store.Buy(db, msg.PlayerName, targetCommand)
			if result != "" {
				return result, nil
			}
			return "", fmt.Errorf("Now Result From buying: %s", targetCommand)
		}

		randomIndex := rand.Intn(len(choices))
		c := choices[randomIndex]

		result := sound_store.Buy(db, msg.PlayerName, c.Name)
		if result != "" {
			return result, nil
		}
	}

	// We could make a better structure
	var boughtCmds []string
	var total int

	// Is this is above an amount we should be it in a more intelligent way
	// We are not checking the amount for the user
	// on each command
	initialCoolPoints := msg.CoolPoints

BuyLoop:
	for i := 0; i < targetAmount; i++ {

		// Out of Choices
		if len(choices) < 1 {
			// TODO: return message about running out Choices
			break BuyLoop
		}

		randomIndex := rand.Intn(len(choices))
		c := choices[randomIndex]
		choices = RemoveCommand(choices, randomIndex)

		initialCoolPoints = initialCoolPoints - c.Cost

		// Out of Money
		if initialCoolPoints < 1 || total+c.Cost >= msg.CoolPoints {
			// TODO: return message about running out Cool Points
			// TODO: return message about running out Cool Points
			break BuyLoop
		}

		result := sound_store.Buy(db, msg.PlayerName, c.Name)
		if result != "" {
			total += c.Cost
			boughtCmds = append(boughtCmds, c.Name)
		}
	}

	// Build Result Message
	cmds := strings.Join(boughtCmds, ", ")
	if len(boughtCmds) > 20 {
		cmds = fmt.Sprintf("%d Commands", len(boughtCmds))
	}
	res := fmt.Sprintf("@%s bought: %v for %d Cool Points",
		msg.PlayerName, cmds, total)

	return res, nil
}

func RemoveCommand(s []RandoComand, i int) []RandoComand {
	s[i] = s[len(s)-1]
	// We do not need to put s[i] at the end, as it will be discarded anyway
	return s[:len(s)-1]
}
