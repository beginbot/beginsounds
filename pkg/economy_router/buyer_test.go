package economy_router

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestBuyer(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	playerName := "zanuss"
	targetAmount := 1
	targetCommand := ""

	p := player.FindOrCreate(db, playerName)
	parsedCmd := chat.ParsedCommand{
		Name:         "buy",
		Username:     playerName,
		TargetAmount: targetAmount,
	}
	msg := chat.ChatMessage{
		PlayerName: p.Name,
		PlayerID:   p.ID,
		Message:    fmt.Sprintf("!buy %d", targetAmount),
		CoolPoints: 1000000000,
		ParsedCmd:  parsedCmd,
		Parts:      []string{"buy", fmt.Sprint(targetAmount)},
	}
	res, _ := Buyer(db, msg, targetCommand, targetAmount)
	if res != "" {
		t.Errorf(res)
	}

}
