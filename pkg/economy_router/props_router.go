package economy_router

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/prop_department"
	"gorm.io/gorm"
)

func PropsRouter(
	ctx context.Context,
	db *gorm.DB,
	commands <-chan chat.ChatMessage,
) <-chan string {

	results := make(chan string)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range commands {
			switch msg.ParsedCmd.Name {
			case "props":
				// We could refactor the use of this method
				// This handles street cred as well which is an issue
				pe, parsedCmd := parser.ParseChatMessageForProps(db, &msg)

				if pe != nil {
					results <- fmt.Sprintf("Error Giving Props !props %v", pe)
					continue MsgLoop
				}

				// This is very silly
				p := player.FindByID(db, msg.PlayerID)
				res, err := prop_department.GiveProps(db, p, parsedCmd)
				if err != nil {
					m := fmt.Sprintf("%v", err)
					results <- m
					continue MsgLoop
				}

				results <- res
			}

		}
	}()

	return results
}
