package economy_router

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

// PackRouter routes commands related to the purchasing
//  and distribution of packs
func PackRouter(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
) (<-chan string, <-chan string) {

	botbotResponses := make(chan string, 10000)
	chatResponses := make(chan string, 10000)

	go func() {

		defer close(botbotResponses)
		defer close(chatResponses)

		for msg := range messages {
			parsedCmd := msg.ParsedCmd
			parts := msg.Parts

			switch parsedCmd.Name {
			case "peakpack", "peekpack":
				if parsedCmd.TargetPackName == "" {
					chatResponses <- fmt.Sprintf(
						"@%s you tried to peak at Pack that isn't present: %s",
						msg.PlayerName,
						parts[1],
					)
					continue
				}

				// fmt.Printf("\tparsedCmd.TargetPack = %+v\n", parsedCmd.TargetPack)
				// cmds := parsedCmd.TargetPack.Commands(db)

				p, _ := pack.Find(db, parsedCmd.TargetPackName)
				cmds := p.Commands(db)

				chatResponses <- fmt.Sprintf(
					"@%s Pack: %s | %s",
					msg.PlayerName,
					parsedCmd.TargetPackName,
					strings.Join(cmds, " | "),
				)

			case "listpacks", "packs":
				names, err := pack.AllNames(db)
				if err == nil {
					site := "http://www.beginworld.exchange/packs.html"
					m := fmt.Sprintf("Packs: %s | %s", strings.Join(names, ", "), site)
					chatResponses <- m
					pack.GeneratePacksPage(db)
					website_generator.SyncRootPage("packs")
				}
			case "createpack":
				p, err := pack.CreatePackFromParts(db, parts[1:])
				// Should we pass this to the pack builder
				// botbotResponses <- "New Pack party"
				if err == nil {
					botbotResponses <- fmt.Sprintf("New Pack: %s", p.Name)
				}
			case "addtopack", "add_to_pack":
				packName := parts[1]
				cps, _ := pack.AddSoundsToPacks(db, packName, parts[2:])
				for _, command := range cps {
					botbotResponses <- fmt.Sprintf(
						"New Command: %s Added to Pack: %s",
						command,
						packName,
					)
				}
				// s = "New Sound in \"party\": !damn"
			case "removefrompack":
				// case "approvepack":
				// case "denypack":

			case "droppack":
				potentialPack := parts[1]
				p, _ := pack.FindByName(db, potentialPack)
				// playa := player.FindByName(db, parsedCmd.TargetUser)
				p.GiveToPlayer(db, parsedCmd.TargetUser)
				chatResponses <- fmt.Sprintf(
					"@%s got pack: %s",
					parsedCmd.TargetUser,
					potentialPack,
				)

			case "buypack":
				if parsedCmd.TargetPackName != "" {
					res, err := pack.BuyPack(
						db,
						parsedCmd.TargetPack.Name,
						// parsedCmd.TargetPackName,
						&player.Player{
							ID:         msg.PlayerID,
							Name:       msg.PlayerName,
							CoolPoints: msg.CoolPoints,
						},
					)

					if err != nil {
						fmt.Printf("Error Buying Pack: %+v\n", err)
					} else {
						chatResponses <- res
					}
				}
			}
		}
	}()

	return botbotResponses, chatResponses
}
