package player

import (
	"errors"
	"fmt"
	"time"

	"gorm.io/gorm"
)

type Player struct {
	ID         int `gorm:"primaryKey"`
	CreatedAt  time.Time
	Name       string
	Streamlord bool
	Streamgod  bool
	InJail     bool
	StreetCred int `json:"street_cred"`
	CoolPoints int `json:"cool_points"`
	Mana       int
}

// name or ID?
func CommandsForPlayers(db *gorm.DB, ID int) []string {
	results := []string{}

	res := db.Raw(`
			SELECT
				c.name
			FROM
				commands_players cp
			INNER JOIN
				players p
			ON
				p.id = cp.player_id
			INNER JOIN
				stream_commands c
			ON
				cp.stream_command_id = c.id
			WHERE
				p.id = ?
	`, ID).Scan(&results)

	if res.Error != nil {
		fmt.Printf("Error Find Commands for Player = %+v\n", res.Error)
	}
	return results
}

func FindTimerByID(db *gorm.DB, ID int) *time.Timer {
	var p Player
	db.Table("players").Where("ID = ?", ID).First(&p)
	if p.Streamlord {
		return time.NewTimer(1 * time.Hour)
	}
	return time.NewTimer(6 * time.Second)
}

func (u *Player) String() string {
	return fmt.Sprintf("<Player: ID: %d Name: %s StreedCred: %d CoolPoints: %d Mana: %d>", u.ID,
		u.Name, u.StreetCred, u.CoolPoints, u.Mana)
}

type CommandResult struct {
	Name string
}

type CommandPlayer struct {
	ID              int
	PlayerID        int
	StreamCommandID int
}

func (p *Player) UpdateMana(db *gorm.DB, mana int) error {
	if mana < 1 {
		return errors.New("Mana cannot be negative")
	}
	res := db.Exec("UPDATE players SET mana = mana - ? WHERE ID = ?",
		mana, p.ID)
	if res.Error != nil {
		fmt.Printf("Error Updating Mana= %+v\n", res.Error)
	}
	return nil
}

// This should be on the player struct

// UpdateMana updates the mana for a player id and mana amount
func UpdateMana(db *gorm.DB, playerID int, mana int) {
	res := db.Exec("UPDATE players SET mana = mana - 1 WHERE ID = ?",
		playerID)
	if res.Error != nil {
		fmt.Printf("Error Updating Mana= %+v\n", res.Error)
	}
}

func FindCommandCount(db *gorm.DB, playerName string) int64 {
	var count int64
	res := db.Raw(`
SELECT count(*) FROM commands_players c
			INNER JOIN players p ON p.id = c.player_id
			WHERE p.name = ?
	`, playerName).Count(&count)

	if res.Error != nil {
		fmt.Printf("res.Error = %+v\n", res.Error)
	}
	return count
}

func FindCommandsForUser(db *gorm.DB, playerName string) []CommandResult {
	var commands []CommandResult

	db.Table("stream_commands").Exec(`SELECT sc.name FROM stream_commands sc
INNER JOIN commands_players c ON sc.ID = c.stream_command_id
			INNER JOIN players p ON p.ID = c.player_id
			WHERE p.name = 'beginbot'`).Scan(&commands)

	return commands
}

// ========= //
// Find User //
// ========= //

func FindByID(db *gorm.DB, ID int) *Player {
	var p Player
	_ = db.Table("players").Where("id = ?", ID).First(&p)
	// if res.Error != nil {
	// 	fmt.Printf("Error Player#Find: %+v\n", res.Error)
	// }
	return &p
}

func Find(db *gorm.DB, name string) *Player {
	var p Player
	res := db.Table("players").Where("name = ?", name).First(&p)
	if res.Error != nil {
		// fmt.Printf("Error Player#Find: %+v\n", res.Error)
	}
	return &p
}

// ============ //
// Create Users //
// ============ //

func CreatePlayerFromName(db *gorm.DB, name string) *Player {
	p := Player{Name: name}
	db.Create(&p)
	return &p
}

func FindOrWelcome(db *gorm.DB, name string) (*Player, bool) {
	var p Player
	res := db.First(&p, "name= ?", name)

	var newPlayer bool
	if res.Error != nil {
		p = *CreatePlayerFromName(db, name)
		newPlayer = true
	}
	return &p, newPlayer
}

func FindOrCreate(db *gorm.DB, name string) *Player {
	var p Player
	res := db.First(&p, "name= ?", name)

	if res.Error != nil {
		// This is where need need to give the pack
		// We don't want to give the pack in here though
		p = *CreatePlayerFromName(db, name)
	}
	return &p
}

// =========== //
// All Players //
// =========== //

func Count(db *gorm.DB) int64 {
	var count int64
	db = db.Model(&Player{}).Count(&count)
	if db.Error != nil {
		fmt.Printf("Error Checking Method %v", db.Error)
	}
	return count
}

func AllNames(db *gorm.DB) []string {
	var result []string
	db = db.Model(&Player{}).Raw("SELECT name FROM players").Scan(&result)
	if db.Error != nil {
		fmt.Printf("Error Checking Method %v", db.Error)
	}
	return result
}

// ============================== //
// Lower Level Methods taking IDs //
// ============================== //

func TransferCommand(db *gorm.DB, streamCommandID int, giverID int, receiverId int) {
	AllowAccess(db, receiverId, streamCommandID)
	RemoveAccess(db, giverID, streamCommandID)
}

func AllowedToPlay(db *gorm.DB, pid int, cid int) bool {
	var count int64
	db.Table("commands_players").
		Where("player_id = ? AND stream_command_id = ?", pid, cid).
		Count(&count)
	return count == 1
}

func AllowAccess(db *gorm.DB, pid int, cid int) error {
	res := db.Exec("INSERT INTO commands_players (player_id,stream_command_id) VALUES (?, ?)", pid, cid)
	if res.Error != nil {
		fmt.Printf("AllowAccess Error: %+v\n", res.Error)
	}
	return res.Error
}

func RemoveAccess(db *gorm.DB, pid int, cid int) {
	res := db.Exec("DELETE FROM commands_players WHERE player_id = ? AND stream_command_id = ?", pid, cid)
	if res.Error != nil {
		fmt.Printf("RemoveAccess Error: %+v\n", res.Error)
	}
}

func (p *Player) MakeLord(db *gorm.DB) error {
	return db.Model(p).Update("streamlord", true).Error
}

func (p *Player) RemoveLord(db *gorm.DB) error {
	return db.Model(p).Update("streamlord", false).Error
}

func (p *Player) MakeGod(db *gorm.DB) error {
	return db.Model(p).Update("streamgod", true).Error
}

func (p *Player) RemoveGod(db *gorm.DB) error {
	return db.Model(p).Update("streamgod", false).Error
}

func (p *Player) SendToJail(db *gorm.DB) error {
	return db.Model(p).Update("in_jail", true).Error
}

func (p *Player) ReleaseFromJail(db *gorm.DB) error {
	return db.Model(p).Update("in_jail", false).Error
}

// ======================== //
// This doesn't belong here //
// ======================== //

func TotalCommandsPlayers(db *gorm.DB) int64 {
	var count int64
	res := db.Table("commands_players").Exec("SELECT count(*)").Count(&count)
	if res.Error != nil {
		panic("Can't Find commands_players")
	}
	return count
}
