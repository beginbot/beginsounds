package proposal

import (
	"fmt"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gorm.io/gorm"
)

// Should we store
// we could be goofy, and save user names in an array

type Vote struct {
	Yay           bool
	ProposalID    int
	PlayerID      int
	ChatMessageID int
}

type Proposal struct {
	ID            int
	ApprovedAt    *time.Time
	RejectedAt    *time.Time
	TriggeredAt   *time.Time
	ChatMessageID int
	Command       string
}

type VoteCount struct {
	Yay int
	Nay int
}

func (prop *Proposal) Votes(db *gorm.DB) (int, int) {
	var res VoteCount

	db.Model(Vote{}).Raw(`
		SELECT
			count(yay) as yay, count(*) - count(yay) as nay
		FROM
			votes
		WHERE
			proposal_id = ?;
	`, prop.ID).Scan(&res)

	return res.Yay, res.Nay
}

func (prop *Proposal) Vote(
	db *gorm.DB,
	msg chat.ChatMessage,
	yay bool,
) (*Vote, error) {
	v := Vote{
		Yay:           yay,
		ProposalID:    prop.ID,
		PlayerID:      msg.PlayerID,
		ChatMessageID: msg.ID,
	}
	tx := db.Create(&v)
	return &v, tx.Error
}

func PendingToBeTriggered(db *gorm.DB) ([]*Proposal, error) {
	var res []*Proposal
	db.Where("rejected_at IS NULL AND approved_at IS NOT NULL AND triggered_at IS NULL").Find(&res)
	return res, nil
}

func PendingProposals(db *gorm.DB) ([]*Proposal, error) {
	var res []*Proposal
	tx := db.Table("proposals").Where(
		`rejected_at IS NULL
			AND approved_at IS NULL
			AND triggered_at IS NULL
			AND created_at > (NOW() - interval '10 minute')
	`).Find(&res)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}
	return res, nil
}

// Rejected
// Approved But not Triggered
func Create(db *gorm.DB, msg chat.ChatMessage) (*Proposal, error) {
	m := msg.Message[7:]
	p := Proposal{
		ChatMessageID: msg.ID,
		Command:       m,
	}
	tx := db.Create(&p)
	return &p, tx.Error
}
