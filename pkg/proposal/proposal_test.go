package proposal

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestPendingProposals(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	p := player.CreatePlayerFromName(db, "young.thug")

	parsedCmd := chat.ParsedCommand{
		Name:     "sunny",
		Username: p.Name,
	}

	cm := chat.ChatMessage{
		PlayerName: p.Name,
		PlayerID:   p.ID,
		Message:    "!sunny The Gang Uses Sunny command correctly",
		ParsedCmd:  parsedCmd,
		Parts:      []string{"!sunny", "The", "Gang", "Uses", "Sunny", "command", "correctly"},
	}
	tx := db.Create(&cm)
	if tx.Error != nil {
		t.Error(tx.Error)
	}

	prop, err := Create(db, cm)
	if err != nil {
		t.Error(err)
	}
	if prop.ChatMessageID != cm.ID {
		t.Errorf("Chat Message IDs didn't match: %d - %d", prop.ChatMessageID, cm.ID)
	}
	ps, err := PendingProposals(db)
	if err != nil {
		t.Error(err)
	}
	if len(ps) != 1 {
		t.Errorf("Wrong number of proposals: %d", len(ps))
	}
}
