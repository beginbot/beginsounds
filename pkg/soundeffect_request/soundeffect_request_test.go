package soundeffect_request

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

var testData = []struct {
	input  string
	output string
}{
	{
		"https://youtu.be/gfWQ1r6IQrY",
		"https://youtu.be/gfWQ1r6IQrY?t=71",
	},
	{
		"https://www.youtube.com/watch?v=Q-ejz2RmueE",
		"https://www.youtube.com/watch?v=Q-ejz2RmueE&t=71",
	},
	{
		"https://www.twitch.tv/theprimeagen/clip/SteamyGoldenPuppyLeeroyJenkins",
		"https://www.twitch.tv/theprimeagen/clip/SteamyGoldenPuppyLeeroyJenkins",
	},
}

func TestFormattedURL(t *testing.T) {
	for _, tt := range testData {
		sc := SoundeffectRequest{
			URL:       tt.input,
			StartTime: "01:11",
		}
		res := sc.FormattedURL()
		if res != tt.output {
			t.Errorf("Incorrectly URL: %s", res)
		}
	}
}

// Test With Start Time and End Time
// Make sure the Stream Command is Saved
// Check if the Stream command Already exists
func TestProcessStreamCommandRequests(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	// This needs an Approved ID to be nil
	sq := SoundeffectRequest{
		Name:     "puppy2",
		URL:      "https://www.youtube.com/watch?v=wfzadSG4NH0",
		Approved: true,
	}

	Save(db, &sq)

	results, _ := Approved(db)
	if len(results) != 1 {
		t.Errorf("Error Fetching Approved: %v", results)
	}

	// TODO: figure a way to only run with with a test flag like -slow
	//
	// for _, sr := range results {
	// 	sr, err := sr.ProcessRequest(db)
	// 	if err != nil {
	// 		t.Errorf("Error processing SoundeffectRequest")
	// 	}
	// 	if sr.DeletedAt == nil {
	// 		t.Errorf("Error marking SoundeffectRequest as Deleted")
	// 	}
	// }
}

// func TestUnapprovedRequests(t *testing.T) {
// 	db := database.CreateDBConn("test_beginsounds3")
// 	test_support.ClearDb(db)

// 	sq := SoundeffectRequest{
// 		Name: "puppy2",
// 		Url:  "https://www.youtube.com/watch?v=wfzadSG4NH0",
// 	}

// 	Save(db, &sq)

// 	results, _ := Unapproved(db)
// 	if len(results) != 1 {
// 		t.Errorf("Error Fetching Unapproved: %v", results)
// 	}
// }

// func TestDenyRequests(t *testing.T) {
// 	// we need some Requests to Deny
// 	db := database.CreateDBConn("test_beginsounds3")
// 	test_support.ClearDb(db)
// 	p := player.CreatePlayerFromName(db, "young.thug")
// 	sr := SoundeffectRequest{Name: "cool_sound", Url: "real_url", RequesterID: p.ID}
// 	db.Create(&sr)
// 	err := UnapproveForPlayerName(db, "young.thug")
// 	if err != nil {
// 		t.Errorf("we Should Not have gotten an error %+v\n", err)
// 	}

// 	// Approve by ID
// 	// Approve by multiple IDS
// 	// Approve by username
// 	// Approve by soundname
// }
