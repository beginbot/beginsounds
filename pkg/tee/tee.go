package tee

import (
	"context"
	"sync"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
)

type ChatChan chan chat.ChatMessage

func FanOut2BackInTheHabit(
	ctx context.Context,
	commands <-chan chat.ChatMessage,
	chanAmount int,
) []ChatChan {

	var chatChan []ChatChan
	for i := 0; i < chanAmount; i++ {
		ch := make(chan chat.ChatMessage, 9001)
		chatChan = append(chatChan, ch)
	}

	go func() {
		for _, ch := range chatChan {
			defer close(ch)
		}

		for c := range commands {
			for _, ch := range chatChan {
				select {
				case ch <- c:
				default:
				}
			}
		}
	}()

	return chatChan
}

// TODO: do these need wait groups
func ChatFanIn(chatChannels ...<-chan chat.ChatMessage) <-chan chat.ChatMessage {
	results := make(chan chat.ChatMessage)

	go func() {
		defer close(results)

		var wg sync.WaitGroup
		wg.Add(len(chatChannels))

		for _, channel := range chatChannels {
			go func(c <-chan chat.ChatMessage) {
				for m := range c {
					results <- m
				}
				wg.Done()
			}(channel)
		}

		wg.Wait()
	}()

	return results
}

func FanOutString(messages <-chan string) (<-chan string, <-chan string) {
	res1 := make(chan string)
	res2 := make(chan string)

	go func() {
		defer close(res1)
		defer close(res2)

		for msg := range messages {
			res1 <- msg
			res2 <- msg
		}

	}()

	return res1, res2
}

func FanOut(
	ctx context.Context,
	commands <-chan chat.ChatMessage) (
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage) {

	c1 := make(chan chat.ChatMessage, 1000)
	c2 := make(chan chat.ChatMessage, 1000)
	c3 := make(chan chat.ChatMessage, 1000)
	c4 := make(chan chat.ChatMessage, 1000)
	c5 := make(chan chat.ChatMessage, 1000)
	c6 := make(chan chat.ChatMessage, 1000)
	c7 := make(chan chat.ChatMessage, 1000)
	c8 := make(chan chat.ChatMessage, 1000)
	c9 := make(chan chat.ChatMessage, 1000)
	c10 := make(chan chat.ChatMessage, 1000)
	c11 := make(chan chat.ChatMessage, 1000)
	c12 := make(chan chat.ChatMessage, 1000)
	c13 := make(chan chat.ChatMessage, 1000)
	c14 := make(chan chat.ChatMessage, 1000)
	c15 := make(chan chat.ChatMessage, 1000)
	c16 := make(chan chat.ChatMessage, 1000)
	c17 := make(chan chat.ChatMessage, 1000)

	go func() {
		defer close(c1)
		defer close(c2)
		defer close(c3)
		defer close(c4)
		defer close(c5)
		defer close(c6)
		defer close(c7)
		defer close(c8)
		defer close(c9)
		defer close(c10)
		defer close(c11)
		defer close(c12)
		defer close(c13)
		defer close(c14)
		defer close(c15)
		defer close(c16)
		defer close(c17)

		for c := range commands {
			select {
			case <-ctx.Done():
				return
			default:
				c1 <- c
				c2 <- c
				c3 <- c
				c4 <- c
				c5 <- c
				c6 <- c
				c7 <- c
				c8 <- c
				c9 <- c
				c10 <- c
				c11 <- c
				c12 <- c
				c13 <- c
				c14 <- c
				c15 <- c
				c16 <- c
				c17 <- c
			}
		}
	}()

	return c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c16, c17
}

func DuplicateChatMessages(
	ctx context.Context,
	commands <-chan chat.ChatMessage) (<-chan chat.ChatMessage, <-chan chat.ChatMessage) {

	// These have to have buffers on them
	c1 := make(chan chat.ChatMessage, 1000)
	sfxs := make(chan chat.ChatMessage, 1000)

	go func() {
		defer close(c1)
		defer close(sfxs)

		for c := range commands {
			select {
			case <-ctx.Done():
				return
			default:
				c1 <- c
				sfxs <- c
			}
		}
	}()

	return c1, sfxs
}
