package pokemon

import (
	"fmt"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/utils"
	"gorm.io/gorm"
)

func CheckGuess(
	db *gorm.DB,
	chatMsg chat.ChatMessage,
	enableOBS bool,
) (string, bool, bool, int64) {
	answer := CurrentAnswer(db)
	guessCount := CurrentValidGuessCount(db)
	guess, valid := extractGuess(chatMsg.Message)
	correct := guess == answer.Pokemon

	if enableOBS {
		movePokemonForGuess(db, guess, guessCount, correct)
	}

	p := player.FindOrCreate(db, chatMsg.PlayerName)
	userGuess := CreatePokemonGuess(db, guess, valid, p.ID, answer.ID)
	fmt.Printf("userGuess = %+v\n", userGuess)

	var winnerCount int64
	winnerCount = CurrentGuessCount(db)

	if correct {

		wonAt := time.Now()
		db = db.Table("pokemon_answers").
			Where("pokemon ILIKE ?", answer.Pokemon).Update("won_at", &wonAt)

		if db.Error != nil {
			fmt.Printf("Error Updating WonAt err = %+v\n", db.Error)
		}

	}

	return guess, valid, correct, winnerCount
}

func movePokemonForGuess(
	db *gorm.DB,
	guess string,
	guessCount int64,
	correct bool,
) {

	if correct {
		moveMsg := fmt.Sprintf("!hideinscene %s", "pokemon")
		utils.CreateOBSRequest(db, moveMsg)

		moveMsg = fmt.Sprintf("!move %s %d %d ", guess, 500, 250)
		utils.CreateOBSRequest(db, moveMsg)
	}

	msg := fmt.Sprintf("!reset %s", guess)
	utils.CreateOBSRequest(db, msg)

	normMsg := fmt.Sprintf("!norm %s", guess)
	utils.CreateOBSRequest(db, normMsg)

	if correct {
		scaleMsg := fmt.Sprintf("!winning_pokemon %s", guess)
		utils.CreateOBSRequest(db, scaleMsg)
		return
	}

	xPos := 0 + (guessCount * 50)
	yPos := 600
	moveMsg := fmt.Sprintf("!move %s %d %d", guess, xPos, yPos)
	utils.CreateOBSRequest(db, moveMsg)
}
