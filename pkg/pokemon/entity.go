package pokemon

import (
	"fmt"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

type PokemonAnswer struct {
	ID        int
	Pokemon   string
	CreatedAt *time.Time
	WonAt     *time.Time
}

type PokemonGuess struct {
	ID              int `gorm:"primaryKey"`
	Pokemon         string
	Valid           bool // Is it a real pokemon
	CreatedAt       time.Time
	PokemonAnswer   *PokemonAnswer `gorm:"-"`
	PokemonAnswerID int
	Player          *player.Player `gorm:"foreignKey:PlayerID"`
	PlayerID        int
}

func CreatePokemonGuess(
	db *gorm.DB,
	guess string,
	valid bool,
	playerID int,
	answerID int,
) *PokemonGuess {

	g := PokemonGuess{
		Pokemon:         guess,
		Valid:           valid,
		PlayerID:        int(playerID),
		PokemonAnswerID: int(answerID),
	}
	db = db.Create(&g)
	if db.Error != nil {
		fmt.Printf("Error Saving Pokemon Guess: %+v", db.Error)
	}
	return &g
}

func CreatePokemonAnswer(db *gorm.DB, name string) *PokemonAnswer {
	answer := PokemonAnswer{Pokemon: name}
	db = db.Create(&answer)
	if db.Error != nil {
		fmt.Printf("NewNew: Err Saving Pokemon Answer: %+v", db.Error)
	}
	return &answer
}

func CurrentAnswer(db *gorm.DB) PokemonAnswer {
	var answer PokemonAnswer
	winnerDB := db.Model(&answer).Where("won_at IS NULL").First(&answer)

	if winnerDB.Error != nil {
		pokemonName := randomGuess()
		answer := PokemonAnswer{Pokemon: pokemonName}
		aDB := db.Create(&answer)

		if aDB.Error != nil {
			fmt.Printf("NewCurrentAnswer: Err Saving Pokemon Answer: %+v", aDB.Error)
		}
		return answer
	}

	return answer
}

func CurrentGuessCount(db *gorm.DB) int64 {
	answer := CurrentAnswer(db)

	var count int64

	db.
		Table("pokemon_guesses").
		Where("pokemon_answer_id = ?", answer.ID).
		Count(&count)

	return count
}

func CurrentValidGuessCount(db *gorm.DB) int64 {
	answer := CurrentAnswer(db)

	var count int64

	db.
		Table("pokemon_guesses").
		Where("valid = true AND pokemon_answer_id = ?", answer.ID).
		Count(&count)

	return count
}
