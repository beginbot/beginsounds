package media_router

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_parser"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

// Route the commands related to uploading
//  and approving Media in BeginWorld™
func Route(
	ctx context.Context,
	db *gorm.DB,
	msgs <-chan chat.ChatMessage,
) (<-chan string, <-chan string) {

	chatResults := make(chan string, 10000)
	botbotResults := make(chan string, 10000)

	go func() {
		defer close(chatResults)
		defer close(botbotResults)

	MsgLoop:
		for msg := range msgs {
			select {
			case <-ctx.Done():
				return

			default:
				parsedCmd := msg.ParsedCmd

				switch parsedCmd.Name {
				case "approve_meme", "approvememe", "approve_image", "approveimage":

					if parsedCmd.TargetMedia != "" {
						fmt.Printf("Attempting to Approve Media: %s", parsedCmd.TargetMedia)
						m, _ := media_request.FindUnapprovedByName(db, parsedCmd.TargetMedia)

						pID := sql.NullInt32{
							Int32: int32(msg.PlayerID),
							Valid: true,
						}
						tx := db.Model(&m).Updates(
							&media_request.MediaRequest{
								Approved:   true,
								ApproverID: pID,
							},
						)

						if tx.Error != nil {
							fmt.Printf("tx.Error = %+v\n", tx.Error)
							continue MsgLoop
						}

						chatResults <- fmt.Sprintf("New Media! !%s", parsedCmd.TargetMedia)
					}

				case "deny_meme", "denymeme":
					if parsedCmd.TargetMedia != "" {
						m, _ := media_request.FindByName(db, parsedCmd.TargetMedia)

						pID := sql.NullInt32{
							Int32: int32(msg.PlayerID),
							Valid: true,
						}
						tx := db.Model(&m).Updates(
							&media_request.MediaRequest{
								Approved:   true,
								ApproverID: pID,
							},
						)
						if tx.Error != nil {
							fmt.Printf("tx.Error = %+v\n", tx.Error)
							continue MsgLoop
						}
						chatResults <- fmt.Sprintf("Media Denied: !%s", parsedCmd.TargetMedia)
					}

				case "image", "img":

					// maybe we take an optional scene here?
					media, err := media_parser.Parse("image", msg)
					if err != nil {
						fmt.Printf("Error Parsing = %+v\n", err)
						continue MsgLoop
					}

					tx := db.Create(&media)
					if tx.Error != nil {
						fmt.Printf("tx.Error = %+v\n", tx.Error)
						continue MsgLoop
					}

					// At this point we need to upload the
					// image to Linode in a folder
					// called dangerous memes
					// We need a 2nd Meme page
					// or unapproved/dangerous memes
					// We need to sync said page
					website_generator.CreateMemePage(db)
					website_generator.SyncMemesPage()
				case "gif", "video":
					fmt.Println("\t\tTRYING TO DOWNLOAD VIDEO")

					media, err := media_parser.Parse("video", msg)
					if err != nil {
						fmt.Printf("Error Parsing = %+v\n", err)
						continue MsgLoop
					}
					fmt.Printf("media = %+v\n", media)

					tx := db.Create(&media)
					if tx.Error != nil {
						fmt.Printf("tx.Error = %+v\n", tx.Error)
						continue MsgLoop
					}
				}
			}
		}
	}()

	return botbotResults, chatResults
}
