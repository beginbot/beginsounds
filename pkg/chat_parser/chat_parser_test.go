package chat_parser

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

var testData = []struct {
	input  string
	output chat.ChatMessage
}{
	{
		"!slide begin", chat.ChatMessage{
			Message: "!slide begin",
			ParsedCmd: chat.ParsedCommand{
				Name: "slide",
			},
		},
	},
	{
		"!b sf 0 -2", chat.ChatMessage{
			Message: "!b sf 0 -2",
			ParsedCmd: chat.ParsedCommand{
				Name: "b",
			},
		},
	},
}

func TestChatParser(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	p := player.CreatePlayerFromName(db, "young.thug")

	for _, tt := range testData {
		cm, _ := Parse(db, p, tt.input)
		if cm.ParsedCmd.Name != tt.output.ParsedCmd.Name {
			t.Errorf("Wrong Parsed Name: %s", cm.ParsedCmd.Name)
		}
		// if cm.Message != tt.output.Message {
		// 	t.Errorf("Wrong Message: %s", cm.Message)
		// }

		// if len(cm.Parts) != 2 {
		// 	t.Errorf("Wrong Parts: %d", len(cm.Parts))
		// 	return
		// }

		// if cm.Parts[0] != "!slide" {
		// 	t.Errorf("Wrong Parts: %v", cm.Parts)
		// }
	}

}
