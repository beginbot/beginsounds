package streamgod_router

import (
	"errors"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/economy_router"
	"gitlab.com/beginbot/beginsounds/pkg/permissions"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

// Should we pass in the 3 channels:
// 	- Chat responses
//  - secret chat response
//  - obs Commands
func dropeffect(
	db *gorm.DB,
	parsedCmd *chat.ParsedCommand,
	targetAmount int,
	chatResults chan<- string,
	botbotResults chan<- string,
	websocketResults chan<- string,
) {
	// ================ //
	// We Know The User //
	// ================ //
	if parsedCmd.TargetUser != "" {
		p := player.Find(db, parsedCmd.TargetUser)

		if parsedCmd.TargetCommand != "" {

			// This needs to check if theres a TargetUser
			// This removes cool points Fix that
			permissions.DropeffectForPlayer(
				db,
				parsedCmd.TargetUser,
				parsedCmd.TargetCommand,
			)
			chatResults <- fmt.Sprintf(
				"@%s now has !%s",
				parsedCmd.TargetUser,
				parsedCmd.TargetCommand,
			)
			return
		}

		// IS this choice not working right???
		choices := economy_router.RandomCommandNoCost(db, p.ID)
		if len(choices) < 1 {
			// This should return an error
			return
		}

		var cmdsDropped []string

		// This I want to encapsulate into a new method
		for i := 0; i < targetAmount; i++ {
			randomIndex := rand.Intn(len(choices))
			targetCommand := choices[randomIndex]
			choices = economy_router.RemoveCommand(choices, randomIndex)
			permissions.DropeffectForPlayer(
				db,
				parsedCmd.TargetUser,
				targetCommand.Name,
			)
			msg := fmt.Sprintf(
				"@%s got !%s",
				parsedCmd.TargetUser,
				targetCommand.Name,
			)
			botbotResults <- msg
			cmdsDropped = append(cmdsDropped, "!"+targetCommand.Name)
		}

		returnMsg := strings.Join(cmdsDropped, ", ")

		if returnMsg != "" && targetAmount < 15 {
			chatResults <- fmt.Sprintf(
				"@%s got SFXs: %s",
				parsedCmd.TargetUser,
				returnMsg,
			)
		}
		return
	}

	// ============================ //
	// Unknown User + Known Command //
	// ============================ //
	if parsedCmd.TargetCommand != "" {
		targetCommand := stream_command.Find(db, parsedCmd.TargetCommand)

		players := chat.RecentChatters(db)

		players = players[0:targetAmount]
		rand.Shuffle(len(players), func(i, j int) {
			players[i], players[j] = players[j], players[i]
		})

		for _, p := range players {
			player.AllowAccess(db, p.ID, targetCommand.ID)
			websocketResults <- fmt.Sprintf("@%s got !%s", p.Name, targetCommand.Name)
		}
	}

	// ========================= //
	// Unknown User and Command //
	// ========================= //
	if parsedCmd.TargetCommand == "" {
		// For every like 10 we need to send a message
		// Need to chunk in groups of 10
		// This is the no mans land
		players := chat.RecentChatters(db)

		players = players[0:targetAmount]

		rand.Shuffle(len(players), func(i, j int) {
			players[i], players[j] = players[j], players[i]
		})

	Loop:
		for _, p := range players {
			targetCommand, err := randomChoice(db, p.ID)
			if err != nil {
				fmt.Printf("Error Finding Random Choice:  %+v\n", err)
				continue Loop
			}
			player.AllowAccess(db, p.ID, targetCommand.ID)
			websocketResults <- fmt.Sprintf("@%s got !%s", p.Name, targetCommand.Name)
		}
		return
	}

}

// This takes a user and gives a random command
// for that user
func randomChoice(db *gorm.DB, ID int) (*economy_router.RandoComand, error) {
	rand.Seed(time.Now().UnixNano())
	choices := economy_router.RandomCommandNoCost(db, ID)
	if len(choices) == 0 {
		return &economy_router.RandoComand{}, errors.New("No Choices!")
	}
	randomIndex := rand.Intn(len(choices))
	return &choices[randomIndex], nil
}

func RandomPlayer(db *gorm.DB, players []chat.ChatResult) (
	*player.Player,
	[]chat.ChatResult,
	error,
) {

	fmt.Printf("Looking for Random Player From Options: %v", players)

	if len(players) < 1 {
		return &player.Player{}, players, errors.New("No Chatters Found")
	}

	// I am reshuffling the Array here
	rand.Shuffle(len(players), func(i, j int) {
		players[i], players[j] = players[j], players[i]
	})

	res := players[len(players)-1]

	return &player.Player{
		ID:   res.ID,
		Name: res.Name,
	}, players[0 : len(players)-2], nil
}
