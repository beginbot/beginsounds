package streamgod_router

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

func TestChaosMode(t *testing.T) {
	// db := database.CreateDBConn("test_beginsounds3")

	// jesterName := player.FindByID(db, jester.PlayerID).Name

	// jester, _ := stream_jester.CurrentJester(db)
	// fmt.Printf("jester.ID %+v\n", jester.ID)
	// fmt.Printf("jester.Secret %+v\n", jester.Secret)
	// fmt.Printf("jester.ChaosMode = %+v\n", jester.ChaosMode)
	// res := chaosMode(db, fakeSync)

	// if res != "!zoombegin CHAOS MODE" {
	// 	t.Errorf("Wrong Message From Chaos Mode: %v", res)
	// }

}

// This is to be passed in, to prevent actually Syncing
// during tests
func fakeSync(db *gorm.DB, sj *stream_jester.StreamJester) {
}
