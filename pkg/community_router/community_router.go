package community_router

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/proposal"
	"gorm.io/gorm"
)

// !sunny
// !proposals
// !support !yay
// !oppose !nay
func Route(
	ctx context.Context,
	db *gorm.DB,
	msgs <-chan chat.ChatMessage,
) (<-chan string, <-chan string, <-chan chat.ChatMessage) {

	beginbotChatMsgs := make(chan string, 10000)
	beginbotbotChatMsgs := make(chan string, 10000)
	obsCmds := make(chan chat.ChatMessage, 10000)

	go func() {
		defer close(beginbotChatMsgs)
		defer close(beginbotbotChatMsgs)
		defer close(obsCmds)

		select {
		case <-ctx.Done():
			return
		default:
			for msg := range msgs {
				parsedCmd := msg.ParsedCmd
				parts := msg.Parts

				switch parsedCmd.Name {
				case "sunny", "iasip":
					fmt.Println("Attempting to save a proposal")
					p, err := proposal.Create(db, msg)

					if err != nil {
						fmt.Printf("Error Creating proposal: %+v\n", err)
						beginbotbotChatMsgs <- fmt.Sprintf(
							"@%s your proposal didn't work!",
							msg.PlayerName,
						)
						continue
					}

					beginbotChatMsgs <- fmt.Sprintf("@%s Thank you for your proposal: %d", msg.PlayerName, p.ID)
				case "proposals":
					// this could get spammy
					proposals, err := proposal.PendingProposals(db)
					if err != nil {
						beginbotbotChatMsgs <- fmt.Sprintf(
							"@%s Error fetching proposals",
							msg.PlayerName,
						)
						continue
					}

					for _, p := range proposals {
						beginbotChatMsgs <- fmt.Sprintf(
							"%d - %s", p.ID, p.Command,
						)
					}
				case "support", "yay":
					// We need to know how close we are to
					prop := findValidProposal(db, parts)
					if prop.ID == 0 {
						beginbotChatMsgs <- fmt.Sprintf(
							"@%s We did not find the proposal to support",
							msg.PlayerName,
						)
						continue
					}

					yayCount, nayCount := prop.Votes(db)

					// This is on 4 votes
					if yayCount-nayCount > 2 {

						// We need to update to save
						approvedTime := time.Now()
						prop.ApprovedAt = &approvedTime
						db.Save(&prop)

						// This doesn't work
						// prop.ApprovedAt = &time.Now()
						// prop.ApprovedAt = &(time.Now())

						obsCmds <- chat.ChatMessage{
							Message:    fmt.Sprintf("!communitysunny %s", prop.Command),
							PlayerName: "beginbotbot",
							Streamgod:  true,
							Streamlord: true,
						}
					}
					// What is this returned whether
					// the proposal should be triggered
					_, err := prop.Vote(db, msg, true)
					if err != nil {
						fmt.Printf("Error Voting:  %+v\n", err)
						continue
					}
					beginbotChatMsgs <- fmt.Sprintf("%s thank you for your support", msg.PlayerName)

				case "oppose", "nay":
					prop := findValidProposal(db, parts)

					if prop.ID == 0 {
						beginbotChatMsgs <- fmt.Sprintf(
							"@%s We did not find the proposal to oppose",
							msg.PlayerName,
						)
						continue
					}

					_, err := prop.Vote(db, msg, false)
					if err != nil {
						fmt.Printf("Error Voting:  %+v\n", err)
						continue
					}
					beginbotChatMsgs <- fmt.Sprintf("%s thank you for your anti-support", msg.PlayerName)
				}

				// fmt.Printf("msg = %+v\n", msg)

			}
		}
	}()

	return beginbotChatMsgs, beginbotbotChatMsgs, obsCmds
}

func findValidProposal(db *gorm.DB, parts []string) *proposal.Proposal {
	var p proposal.Proposal
	for _, part := range parts {
		id, err := strconv.Atoi(part)
		if err == nil {
			tx := db.Where(
				"id = ? AND rejected_at IS NULL AND triggered_at IS NULL AND approved_at IS NULL",
				id,
			).Find(&p)
			if tx.Error != nil && p.ID != 0 {
				return &p
			}
		}
	}
	return &p
}
