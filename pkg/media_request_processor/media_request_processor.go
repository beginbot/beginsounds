package media_request_processor

import (
	"context"
	"fmt"
	"os/exec"
	"sync"
	"time"

	"gitlab.com/beginbot/beginsounds/obs/obs"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/utils"
	"gorm.io/gorm"
)

// Process - looks for approved Media Requests
// 	attempts to download them and make them available on stream
func Process(
	ctx context.Context,
	db *gorm.DB,
) (<-chan string, <-chan string, <-chan chat.ChatMessage) {

	timer := time.NewTicker(time.Second * 25)

	chatResults := make(chan string, 10000)
	botbotResults := make(chan string, 10000)
	obsResults := make(chan chat.ChatMessage, 1000)

	go func() {
		defer close(chatResults)
		defer close(botbotResults)
		defer close(obsResults)

		fmt.Println("Launching The Media Processor")

		// MsgLoop:
		for {
			select {
			case <-ctx.Done():
				return
			default:
				var wg sync.WaitGroup

				videos, err := media_request.ApprovedVideos(db)
				if err != nil {
					fmt.Printf("Err Fetching Approved Videos = %+v\n", err)
				}

				images, err := media_request.ApprovedImages(db)
				if err != nil {
					fmt.Printf("Err Fetching Approved images %+v\n", err)
				}

				unprocessedMedia := len(videos) + len(images)
				wg.Add(unprocessedMedia)

				beginbotbot := player.Find(db, "beginbotbot")
				go ProcessVideos(db, &wg, beginbotbot, videos, botbotResults, obsResults)
				go ProcessImage(db, &wg, beginbotbot, images, botbotResults, obsResults)

				wg.Wait()

				<-timer.C
			}
		}
	}()

	return botbotResults, chatResults, obsResults
}
func ProcessImage(
	db *gorm.DB,
	wg *sync.WaitGroup,
	beginbotbot *player.Player,
	images []media_request.MediaRequest,
	botbotResults chan<- string,
	obsResults chan<- chat.ChatMessage,
) {

	for _, image := range images {

		fmt.Printf("\timage.Name = %+v\n", image.Name)
		if image.Filename == "" {
			fmt.Printf("Missing Filename: %+v\n", image.Name)
		}

		filename := fmt.Sprintf(
			"/home/begin/stream/Stream/ViewerImages/%s",
			image.Filename,
		)

		fmt.Print("\nStarting download\n", filename)
		err := utils.DownloadFile(image.URL, filename)
		fmt.Print("\nEnding download\n")

		if err != nil {
			msg := fmt.Sprintf(
				"Error Downloading: %s | %+v",
				image.Name,
				err,
			)
			fmt.Print(msg)
			botbotResults <- msg
			wg.Done()
		}

		m := fmt.Sprintf("!create_image_source %s %s", image.Name, image.Scene)
		createOBSRequest(db, beginbotbot, m)
		wg.Done()
	}

}

func ProcessVideos(
	db *gorm.DB,
	wg *sync.WaitGroup,
	beginbotbot *player.Player,
	videos []media_request.MediaRequest,
	botbotResults chan<- string,
	obsResults chan<- chat.ChatMessage,
) {

VideoLoop:
	for _, video := range videos {

		filename := fmt.Sprintf(
			"/home/begin/stream/Stream/ViewerVideos/%s", video.Filename)

		err := utils.DownloadFile(video.URL, filename)

		if err != nil {
			msg := fmt.Sprintf(
				"err Downloading: %s | %s %+v\n",
				video.Name,
				video.URL,
				err,
			)
			fmt.Println(msg)
			botbotResults <- msg
			wg.Done()
			continue VideoLoop
		}

		args := []string{
			"--no-mime-magic",
			"--acl-public",
			"put",
			filename,
			"s3://beginworld/memes/",
		}
		cmd := exec.Command("s3cmd", args...)
		err = cmd.Run()
		if err != nil {
			fmt.Printf("err = %+v\n", err)
		}

		m := fmt.Sprintf("!create_video_source %s %s", video.Name, video.Scene)

		createOBSRequest(db, beginbotbot, m)

		wg.Done()
	}
}

func createOBSRequest(db *gorm.DB, beginbotbot *player.Player, m string) error {
	cm := chat.ChatMessage{
		Message:    m,
		PlayerName: beginbotbot.Name,
		PlayerID:   beginbotbot.ID,
		Streamgod:  true,
		Streamlord: true,
	}
	tx := db.Save(&cm)

	if tx.Error != nil {
		fmt.Printf("Error Saving chat Message For OBS Request: %+v\n", tx.Error)
		return tx.Error
	}

	if cm.ID == 0 {
		fmt.Printf("Why is CM ID 0 %+v\n", cm)
		return tx.Error
	}

	req := obs.OBSRequest{
		ChatMessageID: cm.ID,
	}
	tx = db.Save(&req)
	if tx.Error != nil {
		fmt.Printf("Error Saving OBS Request: %+v\n", tx.Error)
		return tx.Error
	}

	return nil
}
