package factorio_router

import (
	"context"
	"fmt"
	"log"
	"strings"

	"github.com/nxadm/tail"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/forpeople"
	"gitlab.com/beginbot/beginsounds/pkg/rcon"
	"gorm.io/gorm"
)

// How can we have our Go App ask the Lua Mod
// what commands are available???
//
// How often should the go app ask?
// Before everys single commmand (eww)
// on sTaRtUp (annoying, have to restart go bot, everytime we update mod)
//
// 2 different modes
//
// We could have the Lua Mod just write out to a file,
// the go bot reads said file
var allowedCommands = []string{
	"trade",
	"club",
	"wb",
	"nb",
	"eb",
	"sb",
	"dir",
	"n",
	"north",
	"s",
	"south",
	"e",
	"east",
	"w",
	"west",
	"ne",
	"northeast",
	"nw",
	"northwest",
	"se",
	"southeast",
	"sw",
	"southwest",
	"stop",
	"backwards",
	"bw",
	"~",
	"b",
	"build",
	"c",
	"craft",
	"d",
	"destroy",
	"mine",
	"m",
	"stopmine",
	"take",
	"rotate",
	"rot",
	"rrot",
	"flip",
	"p",
	"put",
	"lay",
	"l",
	"recipe",
	"move",
	"research",
	"create",
	"friend",
	"killall",
	"killmode",
	"biters",
	"firemode",
	"oilmode",
	"nuclear",
	"nuke",
	"squad",
	"destroysquad",
	"launch",
	"reset",
	"teleport",
	"tele",
	"tel",
	"htr",
	"htr2",
	"reach",
	"recipes",
	"tech",
	"subjects",
	"banner",
	"ingred",
	"markers",
	"marker",
	"mark",
	"map",
	"iron",
	"coal2",
	"show_menus",
	"hide_menus",
	"toggle_gui",
	"toggle_entity",
	"toggle_research",
	"level",

	"forces",
	"force_count",
	"create_force",
	"possess",
	"new_character",
	"current_force",
}

func Route(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
) (<-chan string, <-chan string) {

	botbotResponses := make(chan string, 10000)
	chatResponses := make(chan string, 10000)

	go func() {

		// How do we know, whne messages failed
		port := "27015"
		host := "127.0.0.1"
		password := "password"

		address := fmt.Sprintf("%s:%s", host, port)

		remoteConsole, _ := rcon.Dial(address, password)
		cmd := "/promote beginbot"
		_, err := remoteConsole.Write(cmd)

		defer close(botbotResponses)
		defer close(chatResponses)

		for msg := range messages {
			parsedCmd := msg.ParsedCmd
			parts := msg.Parts
			fmt.Printf("\n\tFactorio parsedCmd: %+v | Parts: %+v\n", parsedCmd, parts)
			isCommand := isFactorioCommand(parsedCmd.Name)

			// Nothing to do lets move on
			if !isCommand {
				fmt.Printf("Not Command: Command = %+v\n", parsedCmd.Name)
				continue
			}

			fp, _ := forpeople.GetCurrentForperson(db)
			if fp != nil {
				fmt.Printf("fp.PlayerName = %+v\n", fp.PlayerName)

				if fp.PlayerID != msg.PlayerID {
					chatResponses <- fmt.Sprintf(
						"%s is NOT THE FORPERSON. %s IS",
						msg.PlayerName,
						fp.PlayerName,
					)
					continue
				}

			}

			allArgs := append(parts[1:], msg.PlayerName)
			args := strings.Join(allArgs, " ")
			factorioCmd := fmt.Sprintf("!%s %s", parsedCmd.Name, args)

			fmt.Printf("\n%s Attempting Command: %s", msg.PlayerName, factorioCmd)

			_, err = remoteConsole.Write(factorioCmd)

			if err != nil {
				log.Printf("\tError Writing to RCON: %+v", err)

				remoteConsole, _ = rcon.Dial(host, password)
				// If this write doesn't work
				// that's a big bummer
				_, _ = remoteConsole.Write(factorioCmd)
			}
		}
	}()

	// Will this Go routine continue to run???
	go tailNotifications("notifications.txt", botbotResponses)
	go tailNotifications("chat_notifications.txt", chatResponses)
	// chat_notifications.txt

	return botbotResponses, chatResponses
}

func tailNotifications(filename string, responseChan chan<- string) {
	factorioNotificationsPath := fmt.Sprintf("/home/begin/.factorio/script-output/%s", filename)

	t, _ := tail.TailFile(
		factorioNotificationsPath,
		tail.Config{
			Follow: true,
		},
	)

	for line := range t.Lines {
		fmt.Println(line.Text)
		rl := line.Text
		if rl != "" {
			l := fmt.Sprintf("Factorio: %s", rl)
			responseChan <- l
		}
	}

}

func isFactorioCommand(cmd string) bool {
	var isCommand bool

	for _, c := range allowedCommands {
		if cmd == strings.TrimSpace(c) {
			return true
		}
	}
	return isCommand
}
