package permissions

import (
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

// We could end our predicate methods with QuestionMark
// have Neovim replace the word QuestionMark
// with a QuestionMark
// Then we can fake Predicates in Go
// teej_dv: Hahahaha just use conceal?
// teej_dv: yes, you can do it
func IsAllowed(db *gorm.DB, pid int, cid int) bool {
	var count int64
	db.Table("commands_players").
		Where("player_id = ? AND stream_command_id = ?", pid, cid).
		Count(&count)
	return count == 1
}

func Unallow(db *gorm.DB, pid int, cid int) error {
	res := db.Exec("DELETE FROM commands_players WHERE player_id = ? AND stream_command_id = ?", pid, cid)

	if res.Error != nil {
		fmt.Printf("Error Removing Command Permission: %+v\n", res.Error)
	}

	return res.Error
}

func Allow(db *gorm.DB, pid int, cid int) error {
	res := db.Exec(
		"INSERT INTO commands_players (player_id,stream_command_id) VALUES (?, ?)",
		pid,
		cid,
	)

	if res.Error != nil {
		fmt.Printf("AllowUserAccessToCommand Error: %+v\n", res.Error)
	}

	return res.Error
}

func UserAllowedToPlay(db *gorm.DB, username string, command string) bool {
	// This will need to check Mana
	// This will need to check Stream Lord Access
	p := player.Find(db, username)
	sc := stream_command.Find(db, command)
	return player.AllowedToPlay(db, p.ID, sc.ID)
}

func DropeffectForPlayer(db *gorm.DB, username string, command string) error {
	p := player.Find(db, username)
	sc := stream_command.Find(db, command)
	return player.AllowAccess(db, p.ID, sc.ID)
}

func AllowUserAccessToCommand(db *gorm.DB, username string, command string) error {
	p := player.Find(db, username)
	sc := stream_command.Find(db, command)
	pres := db.Model(&p).Update("cool_points", p.CoolPoints-sc.Cost)
	if pres.Error != nil {
		fmt.Printf("Error Charging Players Cool Points: %+v\n", pres.Error)
		return pres.Error
	}
	res := db.Model(&sc).Update("cost", sc.Cost+1)
	if res.Error != nil {
		fmt.Printf("Error Charging Players Cool Points: %+v\n", res.Error)
		return res.Error
	}
	return player.AllowAccess(db, p.ID, sc.ID)
}
