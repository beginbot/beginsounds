package forpeople

import (
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

type Forperson struct {
	ID         int
	Motto      string
	PlayerID   int
	PlayerName string
	CreatedAt  *time.Time
	ExpiresAt  *time.Time
	DeletedAt  *time.Time
}

func (Forperson) TableName() string {
	return "forpeople"
}

func GetCurrentForperson(db *gorm.DB) (*Forperson, error) {
	var forperson Forperson
	tx := db.Where("deleted_at IS NULL").Order("created_at desc").First(&forperson)

	if tx.Error != nil || forperson.ID == 0 {
		return nil, nil
	}

	// Check if their time expired
	now := time.Now()
	diff := now.Sub(*forperson.ExpiresAt)
	if diff > 0 {
		tx = db.Table("forpeople").Where("ID = ?", forperson.ID).Update("deleted_at", &now)
		return nil, tx.Error
	}

	return &forperson, tx.Error
}

func SetCurrentForperson(db *gorm.DB, p *player.Player, lengthInMin int) (*Forperson, error) {
	expiresAt := time.Now().Add(time.Minute * time.Duration(lengthInMin))
	fp := Forperson{
		ExpiresAt:  &expiresAt,
		PlayerName: p.Name,
		PlayerID:   p.ID,
	}
	tx := db.Create(&fp)
	return &fp, tx.Error
}
