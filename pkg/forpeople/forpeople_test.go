package forpeople

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
)

func TestSetForperson(t *testing.T) {
	// If I wait 5 minutes this should delete
	db := database.CreateDBConn("test_beginsounds3")
	// test_support.ClearDb(db)
	p := player.FindOrCreate(db, "begin")
	res, _ := GetCurrentForperson(db)
	fmt.Printf("res: %+v\n", res)

	fp, err := SetCurrentForperson(db, p, 5)
	if err != nil {
		t.Errorf("Error Setting Current Foreman: %s", err.Error())
	}

	fmt.Printf("fp = %+v\n", fp)

}

// func GetCurrentForperson(db *gorm.DB) {
// }
