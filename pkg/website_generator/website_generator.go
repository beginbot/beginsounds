package website_generator

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"text/template"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gitlab.com/beginbot/beginsounds/pkg/party"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

// var Domain = "http://www.beginworld.exchange"

var Domain = "https://beginworld.website-us-east-1.linodeobjects.com"

type PlayerPage struct {
	Name         string
	Domain       string
	Extension    string
	CoolPoints   int
	StreetCred   int
	Lovers       []LoverResult
	CommandCount int64
	Commands     []CommandResult
}
type CommandPage struct {
	Domain   string
	Name     string
	Filename string
	Cost     int
	Owners   []OwnerResult
}

// We need a custom page for this
// How can we handle calling this for tests
func CreateCommandPage(db *gorm.DB, command *stream_command.StreamCommand) {
	tmpl, err := template.ParseFiles("../../templates/command.html")
	// tmpl, err := template.ParseFiles("templates/command.html")
	if err != nil {
		fmt.Println("\tError Parsing Template File: ", err)
		return
	}
	// buildFile := fmt.Sprintf("build/commands/%s.html", command.Name)
	buildFile := fmt.Sprintf("../../build/commands/%s.html", command.Name)
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}

	owners := CommandOwners(db, command.ID)
	page := CommandPage{
		Domain:   "https://beginworld.us-east-1.linodeobjects.com",
		Name:     command.Name,
		Filename: command.Filename,
		Cost:     int(command.Cost), Owners: owners,
	}

	err = tmpl.Execute(f, page)
	if err != nil {
		fmt.Printf("Error Executing Template File: %s", err)
		return
	}
	fmt.Println("We generated a Page: ", command.Name)
}

// Make a commands.json file
func CommandsJson(db *gorm.DB) {
	var results []CommandResult

	db.Table("stream_commands").Raw(`
		SELECT * from stream_commands
	`, results)

	JsonPage("commands.json", results)
}

// Useful to build a json page
// JsonPage("memes.json", map[string]string{yo: "yo"})
func JsonPage(name string, data interface{}) {
	// Use current build folder
	// TODO configure build folder
	file := fmt.Sprintf("build/%s", name)
	// file := fmt.Sprintf("../../build/%s", name)
	f, err := os.Create(file)
	if err != nil {
		fmt.Printf("Error Creating JSON: %s\n", err)
		return
	}
	encoder := json.NewEncoder(f)
	jsonErr := encoder.Encode(data)

	if jsonErr != nil {
		fmt.Printf("Error saving to file: %s %s\n", file, jsonErr)
		return
	}

	fmt.Printf("JSON Generated: %s\n", name)
}

type PartiesPage struct {
	Domain    string
	Extension string
	Parties   []party.PartiesAndMembers
}

func CreatePartiesPage(db *gorm.DB) {
	tmpl, _ := template.ParseFiles("templates/parties.html", "templates/sub_templates.tmpl")

	buildFile := fmt.Sprintf("build/parties.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}

	// parties := party.AllParties(db)
	parties := party.AllPartiesAndMembers(db)
	page := PartiesPage{
		Parties:   parties,
		Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension: ".html",
	}

	err = tmpl.Execute(f, page)
	if err != nil {
		fmt.Printf("Error Executing Template File: %s", err)
		return
	}

	JsonPage("parties.json", parties)
}

func CreateUserPage(db *gorm.DB, p *chat.ChatMessage) {
	tmpl, err := template.ParseFiles("templates/user.html", "templates/sub_templates.tmpl")
	// tmpl, err := template.ParseFiles("../../templates/user.html")
	if err != nil {
		fmt.Println("\nErrror Parsing Template File: ", err)
		return
	}

	// buildFile := fmt.Sprintf("../../build/%s.html", p.Name)
	buildFile := fmt.Sprintf("build/users/%s.html", p.PlayerName)
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}

	lovers := LoverNames(db, p.PlayerID)
	cmds := CommandsAndCosts(db, p.PlayerID)

	var cmdCount int64
	res := db.Raw(`
			SELECT count(*) FROM commands_players c
			WHERE c.player_id = ?
	`, p.PlayerID).Count(&cmdCount)
	if res.Error != nil {
		fmt.Printf("res.Error = %+v\n", res.Error)
	}

	page := PlayerPage{
		Name:         p.PlayerName,
		Domain:       "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension:    ".html",
		CoolPoints:   p.CoolPoints,
		StreetCred:   p.StreetCred,
		Lovers:       lovers,
		CommandCount: cmdCount,
		Commands:     cmds,
	}
	err = tmpl.Execute(f, page)
	if err != nil {
		fmt.Printf("Error Executing Template File: %s", err)
		return
	}
}

// ========= //
// Help Page //
// ========= //

type HelpPage struct {
	Domain    string
	Extension string
}

func CreateHelpPage() {
	var Domain = "http://www.beginworld.exchange"
	subTemplate := "templates/sub_templates.tmpl"
	helpTmpl := template.Must(template.ParseFiles("templates/help.html", subTemplate))
	remotePage := HelpPage{
		Domain:    Domain,
		Extension: ".html",
	}
	buildFile := fmt.Sprintf("build/help.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = helpTmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}
}

// ========= //
// meme Page //
// ========= //

type ArtMemesPage struct {
	Domain      string
	Extension   string
	Videos      []string
	Images      []string
	Stylesheets []string
}

type MemePage struct {
	Domain    string
	Extension string
	// Videos    []string
	// Images    []string
	Memes []*memes.Meme
	// Videos []*media_request.MediaRequest
	// Images []*media_request.MediaRequest
	// Images []*memes.Meme
}

func CreateMemePage(db *gorm.DB) {
	var Domain = "http://www.beginworld.exchange"
	subTemplate := "templates/sub_templates.tmpl"
	memeTmpl := template.Must(template.ParseFiles("templates/memes.html", subTemplate))

	memes, _ := memes.AllDefaults(db)
	remotePage := MemePage{
		Memes:     memes,
		Domain:    Domain,
		Extension: ".html",
	}
	buildFile := fmt.Sprintf("build/memes.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("Error Creating Build File: %s\n", err)
		return
	}
	err = memeTmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	json := map[string]interface{}{"media": memes}
	JsonPage("memes.json", json)
}

func CreateArtMemePage(db *gorm.DB) {
	var Domain = "http://www.beginworld.exchange"
	subTemplate := "templates/sub_templates.tmpl"
	memeTmpl := template.Must(template.ParseFiles("templates/artmemes.html", subTemplate))

	images := media_request.Videos(db)
	videos := media_request.Images(db)
	files, err := ioutil.ReadDir("assets/static/.")
	if err != nil {
		log.Fatal("Error reading in the ")
	}
	var stylesheets []string
	for _, f := range files {
		stylesheets = append(stylesheets, f.Name())
	}

	remotePage := ArtMemesPage{
		Images:      images,
		Stylesheets: stylesheets,
		Videos:      videos,
		Domain:      Domain,
		Extension:   ".html",
	}
	buildFile := fmt.Sprintf("build/artmemes.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = memeTmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}
}

// ==========================================
// Queries that don't belong
// ==========================================

type OwnerResult struct {
	Name string
}

func CommandOwners(db *gorm.DB, commandID int) []OwnerResult {
	var c []OwnerResult

	db.Table("players").Raw(`
			SELECT
				p.name
			FROM
				players p
		  INNER JOIN
				commands_players c
			ON
				p.ID = c.player_id
		  WHERE
				c.stream_command_id = ?
			ORDER BY
				p.name`,
		commandID).Scan(&c)

	return c
}

type CommandResult struct {
	Name     string
	Filename string
	Cost     int
}

// TODO: move this queries somewhere better
func CommandsAndCosts(db *gorm.DB, playerID int) []CommandResult {
	var c []CommandResult

	db.Table("stream_commands").Raw(`
			SELECT
				sc.name, sc.filename, sc.cost
			FROM
				stream_commands sc
		  INNER JOIN
				commands_players c
			ON
				sc.ID = c.stream_command_id
		  WHERE
				c.player_id = ?
			ORDER BY
				sc.name`,
		playerID).Scan(&c)

	return c
}

type LoverResult struct {
	Name string
}

func LoverNames(db *gorm.DB, playerId int) []LoverResult {
	var results []LoverResult

	tx := db.Table("players").Raw(`
		SELECT l.name FROM players p
		INNER JOIN players_lovers pl ON p.ID = pl.player_id
		INNER JOIN players l ON pl.lover_id = l.id
		WHERE p.id = ?`, playerId).Scan(&results)

	if tx.Error != nil {
		fmt.Printf("Error LoverResult: %+v\n", tx.Error)
	}

	return results
}

func CreateSunnyPage(db *gorm.DB, title string) {
	tmpl, err := template.ParseFiles("templates/sunny.html")
	if err != nil {
		fmt.Println("\nErrror Parsing Template File: ", err)
		return
	}

	buildFile := "build/sunny.html"
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}

	type SunnyPage struct {
		Title string
	}

	page := SunnyPage{Title: title}
	err = tmpl.Execute(f, page)
	if err != nil {
		fmt.Printf("Error Executing Template File: %s", err)
		return
	}
}
