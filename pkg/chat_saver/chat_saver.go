package chat_saver

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gorm.io/gorm"
)

func SaveChatAndTheme(
	db *gorm.DB,
	msg *chat.ChatMessage,
) (*audio_request.AudioRequest, chat.ChatMessage) {

	// This is checks if the user has chatted recently
	// plays their theme song if they haven't chatted recently
	chattedToday := chat.HasChattedToday(db, msg.PlayerName)

	var obsRequest chat.ChatMessage
	var audioRequest *audio_request.AudioRequest
	if !chattedToday {
		// avatarCommands := map[string][]string{
		// 	"obieru":       {"!obieru"},
		// 	"theprimeagen": {"!primeagen"},
		// 	"roxkstar74":   {"!roxkstar1"},
		// }
		// obsCommands, ok := avatarCommands[msg.PlayerName]

		// if ok {
		// 	i := rand.Intn(len(obsCommands))
		// 	obsCommand := obsCommands[i]
		// 	obsRequest = chat.ChatMessage{
		// 		Message:    obsCommand,
		// 		PlayerName: "beginbotbot",
		// 		Streamgod:  true,
		// 		Streamlord: true,
		// 	}
		// }

		audioRequest, _ = soundboard.CreateAudioRequest(db, msg.PlayerName, msg.PlayerName)
	}

	// This needs to be checked before chattedToday
	chat.Save(db, msg)

	return audioRequest, obsRequest
}

// SaveChat doesn't imply its go func channel consuming + creating nature
func SaveChat(
	ctx context.Context,
	db *gorm.DB,
	dataStream <-chan chat.ChatMessage,
) (<-chan audio_request.AudioRequest, <-chan chat.ChatMessage) {
	obsRequests := make(chan chat.ChatMessage)

	// Why the magic limit of 1000
	themeSongs := make(chan audio_request.AudioRequest, 1000)

	go func() {
		defer close(themeSongs)
		defer close(obsRequests)

		for msg := range dataStream {
			select {

			case <-ctx.Done():
				return
			default:

				// This is checks if the user has chatted recently
				// plays their theme song if they haven't chatted recently
				chattedToday := chat.HasChattedToday(db, msg.PlayerName)
				// chattedToday := false
				if !chattedToday {
					// avatarCommands := map[string][]string{
					// 	"obieru":       {"!obieru"},
					// 	"theprimeagen": {"!primeagen"},
					// 	"roxkstar74":   {"!roxkstar1"},
					// }
					// obsCommands, ok := avatarCommands[msg.PlayerName]
					// if ok {
					// 	i := rand.Intn(len(obsCommands))
					// 	obsCommand := obsCommands[i]

					// 	obsRequests <- chat.ChatMessage{
					// 		Message:    obsCommand,
					// 		PlayerName: "beginbotbot",
					// 		Streamgod:  true,
					// 		Streamlord: true,
					// 	}
					// }

					// We have username twice because are playing the User's Theme Song
					audioRequest, err := soundboard.CreateAudioRequest(db, msg.PlayerName, msg.PlayerName)

					if err != nil {
						fmt.Printf("Error Creating Audio Request: %+v\n", err)
					}

					// As long as we find a valid sample
					if audioRequest.Name != "" {
						themeSongs <- *audioRequest
					}

					// We also could return an obs action here
				}

				// We save after since we do a check for
				// the first message above
				chat.Save(db, &msg)
			}
		}
	}()

	return themeSongs, obsRequests
}
