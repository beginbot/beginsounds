package cube_bet

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestBetting(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	p := player.CreatePlayerFromName(db, "young.thug")
	cb := NewBet(db, 40, p.ID)

	if cb.ID == 0 {
		t.Skipf("We did not save the Cube Bet")
	}
}

func TestProcessBet(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	p := player.CreatePlayerFromName(db, "young.thug")
	sc := stream_command.CreateFromName(db, "damn")
	player.AllowAccess(db, p.ID, sc.ID)

	pb := parser.ParsedCubeBet{
		Name:     "bet",
		PlayerID: p.ID,
		Duration: 10,
		Commands: []string{"damn"},
	}

	cb, cs, _ := ProcessBet(db, &pb)
	if cb.ID == 0 {
		t.Skipf("We did not create a CubeBet")
	}

	if len(cs) != 1 {
		t.Skipf("We didn't create the right number of CubeBetStreamCommand: %d", len(cs))
	}
}

func TestAllBets(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	p := player.CreatePlayerFromName(db, "young.thug")
	sc := stream_command.CreateFromName(db, "damn")
	player.AllowAccess(db, p.ID, sc.ID)

	pb := parser.ParsedCubeBet{
		Name:     "bet",
		PlayerID: p.ID,
		Duration: 10,
		Commands: []string{"damn"},
	}

	_, _, _ = ProcessBet(db, &pb)

	results := AllBets(db)

	fmt.Printf("All Bets: %+v\n", results)

	if len(results) != 1 {
		t.Skipf("Did not get any results")
		return
	}

	if len(results[0].Bets) != 1 {
		t.Skipf("We did not get the bets back!")
		return
	}

	// if results[0].Bets[0] != "damn" {
	// 	t.Skipf("We did not get the bets back!")
	// }
}

func TestCurrentGame(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	g := CurrentGame(db)
	fmt.Printf("g = %+v\n", g)
}
