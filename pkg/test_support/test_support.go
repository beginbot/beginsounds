package test_support

import (
	"fmt"

	"gorm.io/gorm"
)

// ClearDb - clears the tables out using DELETE
// 	it does not use truncate because you would
//  have to drop constraints and recreate them
//  after truncating
//
// DELETING in a list is annoying
// as you can't DELETE from a table
// who references primary IDs referenced
// in other tables as Foreign Keys
func ClearDb(db *gorm.DB) {
	tables := []string{
		"forpeople",
		"proposals",
		"commands_packs",
		"packs_players",
		"packs",
		"chat_messages",
		"parties_players",
		"parties",
		"soundeffect_requests",
		"cube_bet_stream_commands",
		"cube_bets",
		"cube_games",
		"players_lovers",
		"audio_requests",
		"commands_players",
		"pokemon_guesses",
		"pokemon_answers",
		"stream_jesters",
		"players",
		"stream_commands",
	}

	for _, table := range tables {
		stmt := fmt.Sprintf("DELETE FROM %s", table)
		db = db.Exec(stmt)
		if db.Error != nil {
			fmt.Printf("Error clearing DB: %+v", db.Error)
		}
	}
}
