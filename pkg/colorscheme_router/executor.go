package colorscheme_router

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme"
	"gorm.io/gorm"

	"math/rand"
	"os/exec"
	"strings"
	"time"
)

// This could the protype
// for figuring out a router interface
//
func Route(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
	themes []string,
) <-chan string {

	fmt.Println("Lauching Colorscheme Router")

	results := make(chan string)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range messages {
			select {
			case <-ctx.Done():
				return
			default:
			}

			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0][1:]))

			if cmd == "colors" {
				results <- "https://gist.github.com/dylanaraps/3c688fe1db75838e52275264182b1ead"
				continue MsgLoop
			}

			if cmd == "color" {
				color := colorFinder(msg.Message)

				if color == "" {
					color, res := voteRandom(msg, themes)
					results <- res
					newExecColor(ctx, db, results, msg, color)
					continue MsgLoop
				}

				subMatch := []string{}

				// We iterate through and look for exact matches
				// and if we find one, we continue to the MsgLoop
				// We then look and add submatches
				// and add them to to a Submatch lists
				for _, c := range themes {
					if c == color {
						fmt.Println("Match Found!", c)
						newExecColor(ctx, db, results, msg, color)
						continue MsgLoop
					}

					if strings.Contains(c, color) {
						fmt.Println("SubMatch Found color: ", color, " C: ", c)
						subMatch = append(subMatch, c)
					}
				}

				if len(subMatch) > 0 {
					if len(subMatch) == 1 {
						c := subMatch[0]
						newExecColor(ctx, db, results, msg, c)
						continue MsgLoop
					}

					results <- fmt.Sprintf("Choose one of the following: %s", subMatch)
					continue MsgLoop
				} else {
					res := fmt.Sprintf("@%s Not Color Found: %s", msg.PlayerName, color)
					results <- res
				}
			}

		}
	}()

	return results
}

func colorFinder(msg string) string {
	return strings.TrimSpace(strings.Join(strings.Split(msg, " ")[1:], " "))
}

// Terrible Name!
func newExecColor(
	ctx context.Context,
	db *gorm.DB,
	results chan<- string,
	msg chat.ChatMessage,
	color string,
) {

	vote := colorscheme.ColorschemeVote{Theme: color, Username: msg.PlayerName}
	colorscheme.NewCreate(db, vote)

	themeCount := colorscheme.NewCountByTheme(db, color)
	fmt.Println("ThemeCount: ", themeCount)
	winningColor := colorscheme.NewWinningColor(db)
	tc := winningColor.Theme
	tcc := winningColor.Total

	if themeCount >= tcc {
		fmt.Printf("Changing Color: %s | Username: %s", color, msg.PlayerName)
		fmt.Printf("Top Color: %s | Count: %d\n", tc, tcc)
		results <- fmt.Sprintf("User @%s Changed Color to: %s", msg.PlayerName, color)
		changeColorScheme(color)
	} else {
		res := fmt.Sprintf("THX4UR V0T3 @%s: %s - %d | Current Top Color: %s - %d ",
			msg.PlayerName, color, themeCount, tc, tcc)
		results <- res
	}
}

func changeColorScheme(color string) {
	if color == "" {
		color = "random_dark"
	}

	fmt.Println("Color: ", color)
	exec.Command("scripts/colorscheme.sh", color).Output()
}

func voteRandom(msg chat.ChatMessage, themes []string) (string, string) {
	rand.Seed(time.Now().UTC().UnixNano())
	i := rand.Intn(len(themes))

	color := themes[i]
	res := fmt.Sprintf("User @%s Voted for Random Color: %s\n", msg.PlayerName, color)
	return color, res
}
