#!/bin/sh

source /home/begin/.twitch

curl http://localhost:1992/memes/invisible.json > /tmp/invisible.json

meme="$(cat /tmp/invisible.json | jq -r ".[].Name" | sort | dmenu -l 30)"

python /home/begin/code/chat_thief/beginbot.py "!show ${meme}" &
