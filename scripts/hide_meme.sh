#!/bin/sh

source /home/begin/.twitch

curl http://localhost:1992/memes/visible.json > /tmp/visible.json

meme="$(cat /tmp/visible.json | jq -r ".[].Name" | sort | dmenu -l 30)"

python /home/begin/code/chat_thief/beginbot.py "!hide ${meme}" &
