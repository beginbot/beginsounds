package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme_router"
	"gitlab.com/beginbot/beginsounds/pkg/community_router"
	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/criminal_activities"
	"gitlab.com/beginbot/beginsounds/pkg/cube_bet_router"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/economy_router"
	"gitlab.com/beginbot/beginsounds/pkg/factorio_router"
	"gitlab.com/beginbot/beginsounds/pkg/filter"
	"gitlab.com/beginbot/beginsounds/pkg/hand_of_the_market"
	"gitlab.com/beginbot/beginsounds/pkg/info_router"
	"gitlab.com/beginbot/beginsounds/pkg/irc"
	"gitlab.com/beginbot/beginsounds/pkg/media_request_processor"
	"gitlab.com/beginbot/beginsounds/pkg/media_router"
	"gitlab.com/beginbot/beginsounds/pkg/relationship_manager"
	"gitlab.com/beginbot/beginsounds/pkg/reporter"
	"gitlab.com/beginbot/beginsounds/pkg/router"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request_processor"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request_router"
	"gitlab.com/beginbot/beginsounds/pkg/streamgod_router"
	"gitlab.com/beginbot/beginsounds/pkg/tee"
	"gitlab.com/beginbot/beginsounds/pkg/utils"
)

type ResponseChannels struct {
	beginChat       chan string
	beginbotbotChat chan string
}

// TODO: All of these router functions should share an type interface
func main() {
	db := database.CreateDBConn("beginsounds4")

	twitchChannel := flag.String("channel", "beginbot", "The main IRC Channel to connect to")
	isMarketOpen := flag.Bool("market", false, "If the market is open")
	factorioTime := flag.Bool("factorio", false, "Whether we are playing Factorio")
	allowChangingColorscheme := flag.Bool("colors", true, "Whether chat can control colorscheme")

	flag.Parse()

	ctx := context.Background()

	fmt.Printf("Main Bot Running on Twitch Channel: %#v\n", *twitchChannel)
	beginchatConfig := config.NewIrcConfig(*twitchChannel)

	var botResponses = []<-chan string{}
	var botbotResponses = []<-chan string{}
	var audioRequests = []<-chan audio_request.AudioRequest{}

	// We will need to launch another Go Routine to read
	// off from beginbotbot channel
	// and only respond to messages from Streamlords and Streamgods
	botChannelConfig := config.NewIrcConfig("beginbotbot")

	fmt.Println("Launching Back Alley message collector")
	backAlleyMsgs := irc.ReadIrc(ctx, botChannelConfig.Conn)
	backAlleyCmds, _, _ := router.FilterChatMsgs(ctx, db, &botChannelConfig, backAlleyMsgs)

	// Launch a Go Routine to collect messages from IRC
	messages := irc.ReadIrc(ctx, beginchatConfig.Conn)

	// Filter out only the User Chat messages from IRC
	beginbotMsgs, themeRequests, _ := router.FilterChatMsgs(ctx, db, &beginchatConfig, messages)
	audioRequests = append(audioRequests, themeRequests)

	chatMsgs := tee.ChatFanIn(beginbotMsgs, backAlleyCmds)
	unfilteredMsg := tee.FanOut2BackInTheHabit(
		ctx,
		chatMsgs,
		2,
	)

	// This filters out just attempted user commands
	userCommands, botResponses1 := filter.RouteUserCommands(ctx, db, unfilteredMsg[0])
	botResponses = append(botResponses, botResponses1)

	// This is after it's filtered to commands that exist in our DB
	ircChans := tee.FanOut2BackInTheHabit(
		ctx,
		userCommands,
		25,
	)

	uc, ircChans := ircChans[0], ircChans[1:]

	// ------------------------------------------------------------

	// ------------------------------------------------------------
	// Everything after this is chat functionality
	// ------------------------------------------------------------

	// All of the "routers" should return an object
	// that has messages for beginchat and beginbotbot chat

	if *factorioTime {
		uc, ircChans = ircChans[0], ircChans[1:]
		backAlleyResults, chatResults := factorio_router.Route(ctx, db, unfilteredMsg[1])
		botResponses = append(botResponses, chatResults)
		botbotResponses = append(botbotResponses, backAlleyResults)
	}

	// !listpacks
	// !peakpack
	// !createpack
	// !addtopack
	// !removefrompack
	// !droppack
	// !buypack
	adminResults2, botResponses18 := economy_router.PackRouter(ctx, db, uc)
	botResponses = append(botResponses, botResponses18)
	botbotResponses = append(botbotResponses, adminResults2)

	uc, ircChans = ircChans[0], ircChans[1:]
	adminResults1, chatResults, _ := cube_bet_router.Route(ctx, db, uc)
	botResponses = append(botResponses, chatResults)
	botbotResponses = append(botbotResponses, adminResults1)

	uc, ircChans = ircChans[0], ircChans[1:]
	bb1, bbb1, _ := community_router.Route(ctx, db, uc)
	botResponses = append(botResponses, bb1)
	botbotResponses = append(botbotResponses, bbb1)

	// NoMemes Channels
	//
	// !passthejester
	// !unapproveparty
	// !approveparty
	// !unapprovedparties
	// !nomeme
	// !artleaker
	// !chaos
	// !dropeffect
	// !cage
	// !release
	// !enable
	// !disable
	uc, ircChans = ircChans[0], ircChans[1:]
	botbotResponses1, _, botResponses17, audioRequests5, _, _ := streamgod_router.Route(
		ctx,
		db,
		chat.DBChan{DB: db, Messages: uc},
	)
	botResponses = append(botResponses, botResponses17)
	botbotResponses = append(botbotResponses, botbotResponses1)
	audioRequests = append(audioRequests, audioRequests5)

	// !pokemon
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses11 := filter.RoutePokemonCommands(ctx, db, uc)
	botResponses = append(botResponses, botResponses11)

	// Process audio requests and saves them in the DB to Played later
	uc, ircChans = ircChans[0], ircChans[1:]
	audioRequests3, botResponses10, _ := filter.AudioRequests(ctx, db, uc)
	botResponses = append(botResponses, botResponses10)
	audioRequests = append(audioRequests, audioRequests3)

	// !help
	// !wtf
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses21 := info_router.Route(ctx, db, uc)
	botResponses = append(botResponses, botResponses21)

	// !color
	// !colors
	// !setcolor
	if *allowChangingColorscheme {
		themes := colorscheme.GatherColors()
		uc, ircChans = ircChans[0], ircChans[1:]
		botResponses3 := colorscheme_router.Route(ctx, db, uc, themes)
		botResponses = append(botResponses, botResponses3)
	}

	// !props
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses4 := economy_router.PropsRouter(ctx, db, uc)
	botResponses = append(botResponses, botResponses4)

	// !perms
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses14 := economy_router.PermsRouter(ctx, db, uc)
	botResponses = append(botResponses, botResponses14)

	// !buy
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses13 := economy_router.BuyRoute(ctx, db, uc)
	botResponses = append(botResponses, botResponses13)

	// !give
	// !donate
	// !clone -> the word clone is also used by OBS code
	// !duplicate
	uc, ircChans = ircChans[0], ircChans[1:]
	botbotResponses5, botResponses22 := economy_router.GivingRouter(ctx, db, uc)
	botResponses = append(botResponses, botResponses22)
	botbotResponses = append(botbotResponses, botbotResponses5)

	// !me
	// !jester
	// !formparty
	// !parties
	// !join
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses9 := economy_router.MeRoute(ctx, db, uc)
	botResponses = append(botResponses, botResponses9)

	// !image
	// !video
	uc, ircChans = ircChans[0], ircChans[1:]
	botbotResponses2, botResponses19 := media_router.Route(ctx, db, uc)
	botResponses = append(botResponses, botResponses19)

	botbotResponses3, botResponses20, _ := media_request_processor.Process(ctx, db)
	botbotResponses = append(botbotResponses, botbotResponses3)
	botResponses = append(botResponses, botResponses20)

	// !soundeffect
	// !requests
	// !approve
	// !deny
	uc, ircChans = ircChans[0], ircChans[1:]
	botbotResponses2, botResponses5 := soundeffect_request_router.Route(ctx, db, uc)
	botResponses = append(botResponses, botResponses5)
	botbotResponses = append(botbotResponses, botbotResponses2)

	// !love
	// !hate
	// !props
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses8 := relationship_manager.Manage(ctx, db, uc)
	botResponses = append(botResponses, botResponses8)

	// This runs every second to process any new soundeffect requests
	botResponses6, audioRequests2 := soundeffect_request_processor.Process(ctx, db)
	botResponses = append(botResponses, botResponses6)
	audioRequests = append(audioRequests, audioRequests2)

	// Drops Mana and Street Cred
	// Also Leaks credentials ssshhh
	if *isMarketOpen {
		botResponses7 := hand_of_the_market.Serve(ctx, db)
		botResponses = append(botResponses, botResponses7)
	}

	// !steal
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses12 := criminal_activities.Serve(ctx, chat.DBChan{DB: db, Messages: uc})
	botResponses = append(botResponses, botResponses12)

	// Fan in all the Bot Responses before Reporting
	allResults := utils.StringFanIn(ctx, botResponses...)

	// Fan
	allBotBotResponses := utils.StringFanIn(ctx, botbotResponses...)
	reporter.Report(ctx, allBotBotResponses, &botChannelConfig)
	reporter.Report(ctx, allResults, &beginchatConfig)

	for {
		select {
		case <-ctx.Done():
		default:
		}
	}
}
